#include <vector>
#include <algorithm>
#include <random>
#include <chrono>
#include "TileStack.h"
#include "KnyExceptions.h"
/*
Data Structure Representing a stack of King of New York building/unit Tiles.
@author Hau Gilles Che
@version 1.0 10/10/2018
*/

TileStack::TileStack()
{
	this->tiles = std::vector<Tile>();
}

TileStack::TileStack(std::vector<Tile> tiles)
{
	this->tiles = tiles;
}

/*
@return vector of Tile Objects that make up the Stack of tiles.
*/
std::vector<Tile> TileStack::getTiles() { return this->tiles; }

/*
@return Tile at the top of the stack (i.e: Tile that would be visible on the stack in game).
*/
Tile TileStack::getTopTile()
{
	if (tiles.empty())
		throw KingOfNewYorkExceptions::EmptyTileStackException();

	return this->tiles.back();}

/*
@param tiles vector of Tile objects
*/
void TileStack::setTiles(std::vector<Tile> tiles) { this->tiles = tiles; }

/*
Removes The Tile at the top of the stack
@return The Tile that was at the top of the stack.
*/
Tile TileStack::removeTopTile()
{
	Tile topTile = this->tiles.back();
	this->tiles.pop_back();
	return topTile;
}

/**
Shuffles all the Tiles in a stack in a pseudo-random way. (As per the Game Rules, only used for game set up).
Special thanks to user703016's help at https://stackoverflow.com/a/6926473/5648172
*/
void TileStack::shuffle()
{
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine rng(seed);

	std::shuffle(begin(tiles), end(tiles), rng);
}