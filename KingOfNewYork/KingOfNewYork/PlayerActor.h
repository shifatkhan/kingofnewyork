#ifndef PLAYERACTOR_H
#define PLAYERACTOR_H

#include "PlayerStrategy.h"
#include "MainGameLoop.h"

/*
*	Class acting as the main interface for a Player's actions
*/

class PlayerActor {
private:
	PlayerStrategy* strategy;
public:
	//Mutator for PlayerStrategy
	void setPlayerStrategy(PlayerStrategy* stategy);
	
	void rollDice(Player* player, MainGameLoop::MainGameLoop * loop);
	void resolveDice(Player* player, Map* map, int num_of_player, MainGameLoop::MainGameLoop * loop);
	void move(Player* player, Map* map, int num_of_player, MainGameLoop::MainGameLoop* loop);
	void buyCards(Player* player, Deck& deck, MainGameLoop::MainGameLoop & loop);
	
};

#endif // !PLAYERACTOR_H
