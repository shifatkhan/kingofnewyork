#ifndef DICEROLLINGFACILITY_H
#define DICEROLLINGFACILITY_H
#include <array>
#include <vector>
#include "BlackDie.h"
#include "Subject.h"
using namespace std;

class DiceRollingFacility : public Subject{
public:
	//Constructors
	DiceRollingFacility();
	DiceRollingFacility(int rollCtr, array<BlackDie, 6> dice);
	~DiceRollingFacility();

	//Accessors
	array<BlackDie, 6> getDice();
	int getRollCtr();
	std::array<Symbol,6> getSymbols();
	vector<Symbol> getSymbolsToResolve();
	vector<Symbol> getUniqueSymbols();
	
	//Mutators
	void setDice(array<BlackDie, 6> dice);
	void setRollCtr(int rollCtr);
	void setSymbolsToResolve(vector<Symbol> symbolsToResolve);
	
	//Actions
	array <BlackDie, 6> rollAllDice();
	array<BlackDie,6> rollDice();
	void chooseDiceToKeep(array<bool, 6> keep);

	friend std::ostream& operator <<(std::ostream& outs, const DiceRollingFacility& diceFacility);

private:
	int rollCtr;
	array<BlackDie,6> dice;
	vector<Symbol> symbolsToResolve;

	void sortDice();
	void sortSymbols();
	
};
#endif 