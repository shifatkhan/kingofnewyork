/*
*	@author Thai-Vu Nguyen
*	@version 1.0 11/18/2018
*/

#include "AiUtil.h"

using namespace std;

/*
*	Random number generator
*/
int AiUtil::random(int min, int max)
{
	if (max == 0)
		return 0;
	int seed = ((int)time(0))*rand();
	srand(seed);
	int value = (rand() % max + min);
	return value;
}

/*
*	Print effect
*/
void AiUtil::loadingEffect()
{
	cout << "Loading: ";
	for (int i = 0; i < 3; i++)
	{
		Sleep(500);
		std::cout << ".";
	}
	cout << endl;
}

