/** This is the header file Player.h an interface for the Player class.

	@author Shifat Khan
	@version 1.0 10/07/2018
*/
#ifndef CARD_H
#define CARD_H

#include <iostream>
#include <fstream>
#include <iostream>
#include <string>

// Implemented cards.
enum CardEffect
{
	EGO_TRIP,
	NEXT_STAGE,
	POWER_SUBSTATION,
	BROOKLYN_BRIDGE,
	SUBTERRANEAN_CABLE
};

class Card {
private:
	int cost;
	std::string name;
	std::string howToPlay;
	std::string effect;

public:
	// Conctructors
	Card();
	Card(int cost, std::string name, std::string howToPlay, std::string effect);

	// Accessors
	int getCost();
	std::string getName() const;
	std::string getHowtoPlay() const;
	std::string getEffect() const;

	// Mutators
	void setCost(int cost);
	void setName(std::string name);
	void setHowtoPlay(std::string howToPlay);
	void setEffect(std::string effect);

	CardEffect nameToCardNamesEnum(std::string cardName);

	friend std::istream& operator >>(std::istream& ins, Card& card);
	friend std::ostream& operator <<(std::ostream& outs, const Card& card);
};
#endif// PLAYER_H