#include "IOHelper.h"
#include <iostream>
#include <string>

using namespace std;

/**
*	Helper functions for handling IO input
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 11/4/2018
*/


int IOHelper::getValidIntOutput(string message, string error, int min, int max) {
	int valid = false;
	int value;
	string input;
	do {
		cout << message;
		cin >> input;
		cout << endl;
		value = atol(input.c_str());
		if (value >= min && value <= max)
			valid = true;
		else
			cout << error;
	} while (valid == false);
	return value;
}