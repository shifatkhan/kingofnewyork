#pragma once
#include<list>
#include "Observer.h"
#ifndef SUBJECT_H
#define SUBJECT_H
class Observer;
class Subject {

public:
	Subject();
	~Subject();

	void Attach(Observer *);
	void Detach(Observer *);

	void Notify(int gameState);
private:
	std::list<Observer*>* _observers;
};
#endif // SUBJECT_H