#ifndef SYMBOL_H
#define SYMBOL_H
#include <iostream>
enum Symbol {
	ENERGY = 1,
	ATTACK = 2,
	DESTRUCTION = 3,
	HEAL = 4,
	CELEBRITY = 5,
	OUCH = 6
};

inline std::ostream& operator<<(std::ostream& os,const Symbol s)
{
	switch (s)
	{
	case 1:
		os << "Energy";
		break;
	case 2:
		os << "Attack";
		break;
	case 3:
		os << "Destruction";
		break;
	case 4:
		os << "Heal";
		break;
	case 5:
		os << "Celebrity";
		break;
	case 6:
		os << "Ouch!";
		break;
	}
	return os;
}
#endif // !SYMBOL_H
