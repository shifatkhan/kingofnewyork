#ifndef MAP_DRIVER_H
#define MAP_DRIVER_H


namespace MapDriverSpace {
	
	void testWholeDFS();
	void testRegionalDFS();
	void testNotConnectedDFS();
	void testExceptions();
	void runTests();
}

#endif // !MAP_DRIVER_H
