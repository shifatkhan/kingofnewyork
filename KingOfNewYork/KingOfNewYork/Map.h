#ifndef MAP_H
#define MAP_H

#include "Region.h"
#include "Zone.h"
#include "MapExceptions.h"
#include <vector>
/*
*	Header file for Map class. The map acts as the data structure mapping Zones and Regions
*	Zones can be adjacent to other zones and a Zone belong to a Region.
*
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 10/14/2018
*/


struct duo {
	int id;
	bool visited;

public:
	duo();
	duo(int id, bool visited);
};

/*
*	After adding the vertexes. Use the connectTheEdges method to connect the edges
*/
class Map {
	friend class MapDriver;
private:
	//Members

	std::string map_name;
	std::vector<Region*>* regions;
	std::vector<Zone*>* zones;
	std::vector<std::vector<int>> adjacencyZoneList;
	std::vector<std::vector<int>> regionZoneList;
	int zone_capacity;
	int region_capacity;
	int zone_size;
	int region_size;
public:
	//Constructors
	Map();
	Map(std::string map_name);
	Map(std::string map_name, int ZONE_CAPACITY, int REGION_CAPACITY);
	//Destructor
	~Map();
	//Accessors
	std::string getMapName();
	std::vector<Zone*>* getZones();
	vector<Region*>* getRegions();
	int getZoneSize();
	int getRegionSize();
	
	Region* getRegionById(int id);
	Zone* getZoneById(int id);

	//Mutators
	void setMapName(string map_name);

	//Graph Operations
	void addRegion(Region* region);
	void addZone(Zone* zone);
	void addEdge(int zone_id1, int zone_id2, bool ordered);

	Region* getRegionByZone(Zone* zone);
	vector<int> getAdjZoneIDs(int zone_id);
	vector<Zone*> getAdjZones(int zone_id);
	vector<Zone*> getAdjZones(Zone zone);
	vector<int> getZoneIdsByRegion(Region* region);
	vector<int> getZoneIdsByRegion(int region_id);
	vector<Zone*> getZonesByRegion(Region* region);
	vector<Zone*> getZonesByRegion(int region_id);

	void connectTheEdges();

	bool isZonesConnected(int src_id, int dest_id);

	bool areAllZonesConnected(int region_id);
	bool areAllZonesConnected();

	void printMap();


private:
	vector<bool> DFS();
	vector<bool> regionalDFS(int region_id);
	void DFSHelper(int currIndex, vector<duo> &visited, bool visit);
	void RegionalDFSHelper(int region_id, int currIndex, vector<duo> & visited, bool visit);
	void resetLists();

	bool zoneExist(int id);
	bool regionExist(int id);
	bool isZonePartOfRegion(int zone_id, int region_id);
};

#endif // !MAP_H


