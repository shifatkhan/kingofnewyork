#include "CardEffectsView.h"

CardEffectsView::CardEffectsView(Player* player) {
	this->player = player;
}

/** Takes care of displaying information about the card's effect
	being played by a said player. It checks which card had been played
	to display information accordingly.
*/
void CardEffectsView::Update(int gameState) {
	// Check if this view is allowed to display or not.
	if (gameState == MainGameLoop::BUYING) {
		switch (player->getCardEffectState())
		{
		case EGO_TRIP:
			egoTripView();
			break;
		case NEXT_STAGE:
			nextStageView();
			break;
		case POWER_SUBSTATION:
			PowerSubstationView();
			break;
		case BROOKLYN_BRIDGE:
			brooklynBridgeView();
			break;
		case SUBTERRANEAN_CABLE:
			subterraneanCableView();
			break;
		default:
			std::cout << "\nInvalid card effect state: " << player->getCardEffectState() << std::endl;
			break;
		}
	}
}

inline void CardEffectsView::egoTripView() {
	std::cout << "\nCard Effect! \"Ego Trip\":\n" << 
		"\t" << player->getName() << " gained +1 Energy with Superstar's power!\n";
}

inline void CardEffectsView::nextStageView() {
	std::cout << "\nCard Effect! \"Next Stage\":\n" <<
		"\tLose all your VP. Gain 1 Energy or heal 1 damage for each VP you lost this way.\n";

	bool exit = false;
	string input;
	int choice;

	// Loop until user chose valid choice.
	while (!exit)
	{
		cout << "Trade Energy or Heal for each VP?" << endl;
		cout << "[1] Energy." << endl;
		cout << "[2] Heal." << endl;
		cout << "Choose your option: ";
		cin >> input;
		choice = atol(input.c_str());

		switch (choice)
		{
		case 1:
			std::cout << "\t"<< player->getName() <<" traded all VP for Energies\n";
			std::cout << "\t Gained +" << player->getVP() << " Energy!\n";
			player->setEnergy(player->getVP() + player->getEnergy());
			player->setVP(0);
			exit = true;
			break;

		case 2:
			std::cout << "\t" << player->getName() << " traded all VP for Heals!\n";
			std::cout << "\t Gained +" << player->getVP() << " Heals!\n";
			player->setLP(player->getVP() + player->getLP());
			player->setVP(0);
			exit = true;
			break;

		default:
			cout << "Invalid input. Please choose from the selection." << endl;
			break;
		}
	}
}

inline void CardEffectsView::PowerSubstationView() {
	std::cout << "\nCard Effect! \"Power Substation\":\n" <<
		"\t" << player->getName() << " gained +1 VP, +8 Energy, and took 3 Damage!\n";
	player->setVP(player->getVP() + 1);
	player->setEnergy(player->getEnergy() + 8);
	player->setLP(player->getLP() - 3);
}

inline void CardEffectsView::brooklynBridgeView() {
	std::cout << "\nCard Effect! \"Brooklyn Bridge:\n" <<
		"\t" << player->getName() << " gained +4 VP!\n";
	player->setVP(player->getVP() + 4);
}

inline void CardEffectsView::subterraneanCableView() {
	std::cout << "\nCard Effect! \"Subterranean Cable\":\n" <<
		"\t" << player->getName() << " gained +4 Energy, and took 4 Damage!\n";
	player->setVP(player->getVP() + 4);
	player->setLP(player->getLP() - 4);
}