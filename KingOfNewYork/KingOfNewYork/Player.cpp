/**
	This is an implementation file of the class Player.

	@author Shifat Khan
	@version 1.0 10/07/2018
*/
//#include <iostream>
#include <algorithm>
#include "Player.h"

#include "Zone.h"
#include "KnyExceptions.h"
#include "MapUtil.h"
#include "MoveDriver.h"
#include "PlayerStrategy.h"
#include "KnyExceptions.h"

using namespace std;

/**
	Default constructor. Initializes the life points to 10,
	victory points to 0, and name to an empty string.
*/
Player::Player() {
	this->lp = 10;
	this->vp = 0;
	this->name = "";
	this->energy = 0;
	this->keepCards.reserve(10);
	this->specialCards.reserve(2);
	this->zone = nullptr;
}

/**
	3 param constructor. Takes in the life points, 
	victory points, name, and a monster card.

	@param lp The life points of the player
	@param vp The victory points of the player
	@param name The name of the player
	@param monsterCard The monster of the player
	@param diceRollingFacility The player's dice rolling facility
*/
Player::Player(int lp, int vp, std::string name, Card monsterCard, DiceRollingFacility* diceRollingFacility) {
	this->lp = lp;
	this->vp = vp;
	this->name = name;
	this->energy = 0;
	this->keepCards.reserve(10);
	this->monsterCard = monsterCard;
	this->specialCards.reserve(2);
	this->zone = nullptr;
	this->diceRollingFacility = diceRollingFacility;
}

Player::~Player()
{
	delete strategy;
	strategy = NULL;
	//this->strategy = NULL;
}

// Accessors
int Player::getLP() { return this->lp; }
int Player::getVP() { return this->vp; }
std::string Player::getName() { return this->name; }
int Player::getEnergy() { return this->energy; }
std::vector<Card> Player:: getKeepCards() { return this->keepCards; };
Card Player::getMonsterCard() { return this->monsterCard; };
std::vector<Card> Player::getSpecialCards() { return this->specialCards; };
Zone * Player::getZone()
{
	return this->zone;
}
DiceRollingFacility* Player::getDiceRollingFacility() { return this->diceRollingFacility; }

PlayerStrategy* Player::getPlayerStrategy()
{
	return this->strategy;
}

// Mutators
void Player::setLP(int lp) { this->lp = lp; }
void Player::setVP(int vp) { this->vp = vp; }
void Player::setName(std::string name) { this->name = name; }
void Player::setEnergy(int energy) { this->energy = energy; }
void Player::setKeepCards(std::vector<Card> keepCards) { this->keepCards = keepCards; }
void Player::setMonsterCard(Card monsterCard) { this->monsterCard = monsterCard; }
void Player::setSpecialCards(std::vector<Card> specialCards) { this->specialCards = specialCards; }
void Player::setDiceRollingFacility(DiceRollingFacility* diceRollingFacility) {
	this->diceRollingFacility = diceRollingFacility; 
	//Notify();
}

void Player::setPlayerStrategy(PlayerStrategy* strategy)
{
	this->strategy = strategy;
}

/**
	TODO: might not need this function
*/
void Player::addKeepCard(Card keepCard) {
	this->keepCards.push_back(keepCard);
}

/**
	Remove keep card and put it in the discard pile.

	Used "erase-remove idiom" method: https://en.wikipedia.org/wiki/Erase%E2%80%93remove_idiom
*/
void Player::removeKeepCard(std::string cardName) {
	// Card to be moved to the discard pile.
	Card discardCard;
	std::string tempCardName;
	for (int i = 0; i < this->keepCards.size(); i++)
	{
		if (cardName == this->keepCards[i].getName()) {
			discardCard = this->keepCards[i];
			this->keepCards.erase(this->keepCards.begin() + i);
			
			// TODO: Change this design to avoid breaks.
			break;
		}
	}
}

/**
	Remove a special card
*/
void Player::removeSpecialCard(std::string cardName) {
	std::string tempCardName;
	for (int i = 0; i < this->specialCards.size(); i++)
	{
		if (cardName == this->specialCards[i].getName()) {
			this->specialCards.erase(this->specialCards.begin() + i);

			// TODO: Change this design to avoid breaks.
			break;
		}
	}
}

/**
	Decreases the player's LP by 1.
*/
void Player::takeDamage() {
	this->lp--;
}

/**
	Decreases the player's LP by the amount specified.

	@param damage The amount of LP to deduct from the player's LP
*/
void Player::takeDamage(int damage) {
	this->lp -= damage;
}

/**
Rolls the player's black dice.
*/
void Player::rollDice() {
	/*
	bool goodInput;
	bool reroll = true;
	do {
		goodInput = false;
		diceRollingFacility->rollDice();

		if (diceRollingFacility->getRollCtr() < 3) {
			displayRollOptions();
		}
	} while (reroll && (diceRollingFacility->getRollCtr() < 3));
	*/
	
	try
	{
		//while((diceRollingFacility->getRollCtr() < 3))
		//this->diceRollingFacility.rollDice();
	}
	catch (KingOfNewYorkExceptions::OutOfRollException ore)
	{
		std::cout << "Player "<< this->name << " is out of roll." << endl;
	}
}

/**
Completes the Resolve Dice step of a Player's turn. (i.e.: process the Player's Dice symbols and their effects)
*/
void Player::resolveDice(Map* map, int number_of_player, MainGameLoop::MainGameLoop* loop) {
	int nextIndex = 0;
	int symbolCtr = 0;
	int numCelebrity = 0;
	int numOuch = 0;
	int damageTaken = 0;

	//Flags used for resolving particular symbols that require to be resolved all at once (and not sequentially)
	bool ouchHasBeenResolved = false;
	bool celebrityHasBeenResolved = false;
	bool destructionHasBeenResolved = false;

	bool isInManhattan = MapUtil::isPartOfManhattan(this->getZone(), map);
	vector<Symbol> symbolsToResolve = diceRollingFacility->getSymbolsToResolve();
	
	Symbol currSymbol = chooseSymbolToResolve(symbolsToResolve, loop);

	int symbolToResolveCtr = symbolsToResolve.size();
	int currSymbolIndex = firstOccurenceOfSymbol(currSymbol, symbolsToResolve);
	
	//grabs a hold on the borough where the player's monster is located.
	Region* playerRegion = MapUtil::zoneToRegion(this->getZone(), map);
	string borough = playerRegion->getRegionName();

	vector<Player*> playersToAttack;
	//Determines players to attack if any Attack has been rolled.
	if (isInManhattan)
	{
		playersToAttack = MapUtil::getAllPlayersOutsideMahattan(map);
	}
	else 
	{
		playersToAttack = MapUtil::getAllPlayersInManhattan(map);
	}
	
	
	while (!symbolsToResolve.empty())
	{
		//Spacing purposes for better readability.
		cout << endl;
		switch (currSymbol)
		{
		case ENERGY:
			cout << "\nResolving Energy Dice!" << endl;
			cout << this->getName() <<" is acquiring 1 Energy cube." << endl;
			this->energy++;
			break;
		case ATTACK:
			cout << "Resolving Attack Dice!" << endl;
			cout << this->getName() << " is in " << borough << " and inflicts damage to all Monsters ";
			if (isInManhattan)
				cout << "outside";
			else
				cout << "inside";
			cout << " Manhattan!" << endl;
			if (playersToAttack.empty())
			{
				cout << "\nThere is no Monster to attack!" << endl;
			}
			else
			{
				for (Player* attackedPlayer : playersToAttack)
				{
					if (!attackedPlayer->getName()._Equal(this->getName()))
					{
						attackedPlayer->takeDamage();
						cout << attackedPlayer->getName() << " loses 1 Life Point." << endl;
						if (MapUtil::isPartOfManhattan(attackedPlayer->getZone(), map)) {
							attackedPlayer->getPlayerStrategy()->moveOutManhattan(attackedPlayer, map, number_of_player);
						}
					}
				}
			}
			break;
		case DESTRUCTION:
			if (!destructionHasBeenResolved)
			{
				cout << "\nResolving Destruction Dice(s)!" << endl;
				symbolCtr = 1;
				nextIndex = currSymbolIndex + 1;
				while ((nextIndex < symbolToResolveCtr) && (symbolsToResolve[nextIndex] == currSymbol))
				{
					symbolCtr++;
					nextIndex++;
				}

				destroyTile(symbolCtr, playerRegion, loop);
				destructionHasBeenResolved = true;
			}
			break;
		case HEAL:
			cout << "\nResolving Heal Dice!" << endl;
			if (this->lp < 10)
			{
				if (!isInManhattan)
				{
					cout << this->getName() <<" heals by 1 Life Point." << endl;
					this->lp++;
				}
				else
				{
					cout << this->getName() << " is in Manhattan. You cannot heal." << endl;
				}
			}
			else
			{
				cout << "Your Monster is at full health. You cannot heal." << endl;
			}
			break;
		case CELEBRITY:
			if (!celebrityHasBeenResolved)
			{
				symbolCtr = 1;
				nextIndex = currSymbolIndex + 1;
				while ((nextIndex < symbolToResolveCtr) && (symbolsToResolve[nextIndex] == currSymbol))
				{
					symbolCtr++;
					nextIndex++;
				}
				if (symbolCtr >= 3)
				{
					//might need decisional logic for the message to display and stuff
					cout << this->getName() << " acquires the Superstar card!" << endl;
					cout << this->getName() << " earns " << symbolCtr - 2 << " Victory Point(s)" << endl;
				}
				else
				{
					cout << "\nYou do not have enough Celebrity dice!" << endl;
				}
				celebrityHasBeenResolved = true;
			}
			break;
		case OUCH:
			if (!ouchHasBeenResolved)
			{
				cout << "\nResolving Ouch! Dice!" << endl;
				symbolCtr = 1;
				nextIndex = currSymbolIndex + 1;

				//Calculates the number of ouch symbol rolled since the amount will cause different outcomes.
				while ((nextIndex < symbolToResolveCtr) && (symbolsToResolve[nextIndex] == currSymbol))
				{
					symbolCtr++;
					nextIndex++;
				}

				resolveOuch(symbolCtr, playerRegion,map);
				ouchHasBeenResolved = true;
			}		
			break;
		default:
			throw new KingOfNewYorkExceptions::InvalidDieSymbolException();
			break;
		}

		if ((symbolToResolveCtr < 2) || currSymbolIndex == (symbolToResolveCtr - 1))
		{
			symbolsToResolve.pop_back();
			symbolToResolveCtr--;
		}
		else
		{
			symbolsToResolve.erase(symbolsToResolve.begin() + (currSymbolIndex));
			symbolToResolveCtr--;
		}
		if (!symbolsToResolve.empty())
		{
			if (currSymbolIndex < symbolToResolveCtr)
			{
				if (symbolsToResolve[currSymbolIndex] != currSymbol)
				{
					currSymbol = chooseSymbolToResolve(symbolsToResolve,loop);
					currSymbolIndex = firstOccurenceOfSymbol(currSymbol, symbolsToResolve);
				}
			}
			else
			{
				currSymbol = chooseSymbolToResolve(symbolsToResolve,loop);
				currSymbolIndex = firstOccurenceOfSymbol(currSymbol, symbolsToResolve);
			}
		}
	}
}

/**
*Choose the Die symbol to be resolved.
*@param symbolsToResolve A list of the available symbols to be resolved.
*@return The symbol to resolve.
*/
Symbol Player::chooseSymbolToResolve(vector<Symbol> symbolsToResolve, MainGameLoop::MainGameLoop* loop)
{
	string input;
	int numInput;
	bool goodInput = false;
	while (!goodInput)
	{
		cout << "\nPlease select the Symbol to resolve by inputting its option number" << endl;
		Symbol s = symbolsToResolve[0];
		cout << "[" << static_cast<int>(s) << "]" << s << endl;
		for (int i = 1; i < symbolsToResolve.size(); i++)
		{
			if (symbolsToResolve[i] != s)
			{
				s = symbolsToResolve[i];
				cout << "[" << static_cast<int>(s) << "] " << s << endl;
			}
		}
		cout << "[-1] See Game statistics." << endl;

		cout << "Option to resolve: ";
		numInput = this->strategy->chooseSymbolToResolve(symbolsToResolve);
		if (numInput == -1)
		{
			loop->setGameState(MainGameLoop::STATS_MENU);
			//goodInput = true;
		}
		else
		{
			for (Symbol s : symbolsToResolve)
			{
				if (s == numInput)
				{
					goodInput = true;
				}
			}
		}
	}
	
	Symbol symbol = static_cast<Symbol>(numInput);
	return symbol;

}

/*
Retrieves the index of the first occurence of a symbol in a list of symbols
*/
int Player::firstOccurenceOfSymbol(Symbol s,vector<Symbol> symbols)
{
	int index= -1;
	int i = 0;
	while ((index < 0) && (i < symbols.size()))
	{
		if (symbols[i] == s)
		{
			index = i;
		}
		i++;
	}
	return index;
}

/*
*	Function to move Player from one zone to another
*/
void Player::move(Zone* zone) {
	if (this->zone != NULL && this->zone != nullptr)
		this->zone->setPlayer(NULL);

	if(zone != NULL && zone != nullptr)
		zone->setPlayer(this);
	
	this->zone = zone;
	
}

/*
*	Removes Player from the zone
*/
void Player::die() {
	if (this->zone != NULL && this->zone != nullptr) {
		this->zone->setPlayer(NULL);
		this->zone = NULL;
	}
}

void Player::buyCard(Card cardBought) {
	this->energy = this->energy - cardBought.getCost();

	if (cardBought.getHowtoPlay() == "Keep") {
		this->keepCards.push_back(cardBought);
	}
	else {
		// Card bought is a Discard card
		applyCardEffect(&cardBought);
	}
}

/*
	Applies the given card's effect on the player.
*/
void Player::applyCardEffect(Card* card) {
	try
	{
		switch (card->nameToCardNamesEnum(card->getName())) {
		case EGO_TRIP:
			setCardEffectState(EGO_TRIP);
			break;
		case NEXT_STAGE:
			setCardEffectState(NEXT_STAGE);
			break;
		case POWER_SUBSTATION:
			setCardEffectState(POWER_SUBSTATION);
			break;
		case BROOKLYN_BRIDGE:
			setCardEffectState(BROOKLYN_BRIDGE);
			break;
		case SUBTERRANEAN_CABLE:
			setCardEffectState(SUBTERRANEAN_CABLE);
			break;
		default:
			// The card was not implemented yet.
			std::cout << card->getName() << "'s effect has not yet been implemented." << endl;
			break;
		}
	}
	catch (const KingOfNewYorkExceptions::CardNotImplementedException&)
	{
		// The card was not implemented yet.
		std::cout << "\n" << card->getName() << "'s effect has not yet been implemented." << endl;
	}
}

CardEffect Player::getCardEffectState() {
	return cardEffectState;
}

void Player::setCardEffectState(CardEffect cardEffect) {
	this->cardEffectState = cardEffect;
	Notify(MainGameLoop::BUYING);
}

void Player::displayPlayerStatus() {
	std::cout << "Player " << this->name << "'s status:" << std::endl;
	std::cout << "\tMonster: " << this->monsterCard.getName() << std::endl;
	std::cout << "\tLife Points: " << this->lp << std::endl;
	std::cout << "\tVictory Points: " << this->vp << std::endl;
	std::cout << "\tEnergy: " << this->energy << std::endl;

	std::cout << "\tKeep Cards: " << this->keepCards.size() << std::endl;
	for (int i = 0; i < this->keepCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << this->keepCards[i] << std::endl;
	}

	std::cout << "\tSpecial Cards: " << this->specialCards.size() << std::endl;
	for (int i = 0; i < this->specialCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << this->specialCards[i] << std::endl;
	}
	std::cout << "\n";
}

/**
*Handles the resolution and effects of resolving the Ouch! symbols
*@param ouchCtr The amount of Ouch! that has been rolled
*@param playerRegion The location of the player
*@param map The game map
*/
void Player::resolveOuch(int ouchCtr, Region* playerRegion,Map* map)
{
	int damageTaken;
	vector<Player*> playersToAttack;
	string borough = playerRegion->getRegionName();
	switch (ouchCtr)
	{
	case 1:
		damageTaken = playerRegion->getNumOfUnits();
		this->takeDamage(damageTaken);
		cout << "1 Ouch have been rolled." << endl;
		cout << this->name << " suffers " << damageTaken << " damage from the unit(s) in " << borough << "." << endl;
		break;
	case 2:
		damageTaken = playerRegion->getNumOfUnits();
		playersToAttack = MapUtil::getAllPlayersInsideRegion(playerRegion);
		cout << "2 Ouch have been rolled." << endl;
		for (Player* p : playersToAttack)
		{
			cout << p->getName() << " suffers " << damageTaken << "damage from the unit(s) in " << borough << "." << endl;
			p->takeDamage(damageTaken);
		}
		break;
	default:
		//Army attack, each player gets attack by all units in their respective borough
		playersToAttack = MapUtil::getAllPlayersInManhattan(map);
		if (!playersToAttack.empty())
		{
			playerRegion = (MapUtil::zoneToRegion(playersToAttack[0]->getZone(), map));
		}
		
		damageTaken = playerRegion->getNumOfUnits();
		cout << ouchCtr <<" Ouch have been rolled. You have triggered a counterattack from the United States of America's Army!" << endl;
		for (Player* p : playersToAttack)
		{
			p->takeDamage(damageTaken);
			cout << p->getName() << " suffers " << damageTaken << "damage from the unit(s) in " << borough << "." << endl;
		}

		playersToAttack = MapUtil::getAllPlayersOutsideMahattan(map);
		for (Player* p : playersToAttack)
		{
			Region* pRegion = (MapUtil::zoneToRegion(p->getZone(), map));
			damageTaken = pRegion->getNumOfUnits();
			p->takeDamage(damageTaken);
			cout << p->getName() << " suffers " << damageTaken << "damage from the unit(s) in " << pRegion->getRegionName() << "." << endl;
		}
		break;
	}
}

/**
*Action to destroy tile.
*@param destructCtr The amount of Destruction to be resolved.
*@param playerRegion The borough where the player is located.
*/
void Player::destroyTile(int destructCtr, Region* playerRegion, MainGameLoop::MainGameLoop * loop)
{
	vector<TileStack> buildings = playerRegion->getTileStacks();
	vector<Tile> units = playerRegion->getUnits();
	vector<Tile> newUnits;
	bool goodInput;
	bool hasBuildings = !buildings.empty();
	bool hasUnits = !units.empty();
	bool canDestroyBuildings = this->canDestroyBuildingsTile(buildings, destructCtr);
	bool canDestroyUnits = this->canDestroyUnitsTile(units, destructCtr);
	string destB = "[1] Destroy a Building.";
	string destU = "[2] Destroy a Unit.";
	string input;
	int numInput;
	int index = 0;

	if (hasBuildings || hasUnits)
	{
		playerRegion->displayTiles();
		Tile building;
		Tile unit;
		while ((destructCtr > 0) && (canDestroyBuildings || canDestroyUnits))
		{
			goodInput = false;
			while (!goodInput)
			{			
				cout << "\nYou have " << destructCtr << " Destruction Die. Select one of the options below:" << endl;
				//Serie of if to display only the option that a Player could do (e.g.: will not display destroy unit option if there is no unit)
				if (canDestroyBuildings && canDestroyUnits)
				{
					cout << destB << endl;
					cout << destU << endl;
				}
				else if (canDestroyBuildings)
				{
					cout << destB << endl;
				}
				else
				{
					cout << destU << endl;
				}
				cout << "[-1] See Game statistics." << endl;
				cout << "Option selected: ";

				numInput = this->strategy->chooseTileToDestroy(canDestroyBuildings, canDestroyUnits);

				switch (numInput)
				{
				case -1:
					loop->setGameState(MainGameLoop::STATS_MENU);
					break;
				case 1:
					if (canDestroyBuildings)
					{
						index = this->buildingToDestroy(destructCtr, buildings);
						building = buildings[index].getTopTile();

						if (building.getBuildingDurability() <= destructCtr)
						{
							buildings[index].removeTopTile();
							building.setIsUnit(true);
							newUnits.push_back(building);
							destructCtr = destructCtr - building.getBuildingDurability();
							Reward reward = building.getBuildingReward();

							cout << "\nBuilding destroyed!" << endl;
							this->processTileRewards(reward);
							try {
								Tile aTile = buildings[index].getTopTile();
								cout << "\nA new Building is reavealed:" << endl;
								cout << aTile << endl;
							}
							catch (KingOfNewYorkExceptions::EmptyTileStackException) {
								cout << "\nAll Buildings in this pile have been destroyed." << endl;
							}
							//cout << "\nA new Building is reavealed:" << endl;
							//cout << buildings[index].getTopTile() << endl;
							cout << "\nA new Unit has been set in your borough:" << endl;
							cout << building << endl;
							goodInput = true;
						}
						else
						{
							cout << "\nYou do not have the necessary Destruction Dice to destroy this!" << endl;
						}
					}	
					else
					{
						cout << "\nThere is no Building destructable in your borough!" << endl;
					}
					break;
				case 2:
					if (canDestroyUnits)
					{	
						index = this->unitToDestroy(destructCtr, units);

						unit = units[index];

						if (destructCtr >= unit.getUnitDurability())
						{
							destructCtr = destructCtr - unit.getUnitDurability();
							units.erase(units.begin() + index);
							Reward reward = unit.getUnitReward();
							this->processTileRewards(reward);
							goodInput = true;
						}
						else
						{
							cout << "\nYou do not have the necessary Destruction Dice to destroy this!" << endl;
						}

					}
					else
					{
						cout << "\nThere is no Unit destructable in your borough!" << endl;
					}
					break;
				default:
					cout << "\nInvalid input. Please select from the given options." << endl;
					break;
				}
				canDestroyBuildings = this->canDestroyBuildingsTile(buildings, destructCtr);
				canDestroyUnits = this->canDestroyUnitsTile(units, destructCtr);
			}
		}
		cout << "\nYou cannot destroy anymore!" << endl;
		playerRegion->setTileStack(buildings);
		//Assigns the newly created units, if any, to the units vector once the resolve desctruction is completed
		//this prevents the Player of being able to destroy units that appeared on his own turn (as per the game rules)
		for (Tile unit : newUnits)
		{
			units.push_back(unit);
		}
		playerRegion->setUnits(units);
	}
	else
	{
		cout << "\nThere is no Building nor Unit in your borough to destroy!" << endl;
	}
}

/**
*Evaluates if there is a Building that the Player can destroy.
*@param buildings The buildings present in the player's borough.
*@param destructCtr The amount of Destruction to be resolved.
*@return True if the player can destroy a building tile, false otherwise.
*/
bool Player::canDestroyBuildingsTile(vector<TileStack> buildings, int destructCtr)
{
	bool canDestroy = false;
	for (TileStack building : buildings)
	{
		if (destructCtr >= building.getTopTile().getBuildingDurability())
		{
			canDestroy = true;
		}
	}
	return canDestroy;
}

/**
*Evaluates if there is a Unit that the Player can destroy.
*@param units The units present in the player's borough.
*@param destructCtr The amount of Destruction to be resolved.
*@return True if the player can destroy at least one unit tile, false otherwise.
*/
bool Player::canDestroyUnitsTile(vector<Tile> units, int destructCtr)
{
	bool canDestroy = false;
	for (Tile unit : units)
	{
		if (destructCtr >= unit.getUnitDurability())
		{
			canDestroy = true;
		}
	}
	return canDestroy;
}

/**
* Gives the player the rewards from a Tile destruction.
*@param reward The reward from the destroyed tile.
*/
void Player::processTileRewards(Reward reward)
{
	int numReward = reward.getRewardCount();
	cout << "Destruction Reward: " << numReward;
	switch (reward.getRewardSymbol())
	{
	case 1:
		cout << " Energy Cubes." << endl;
		this->energy = this->energy + numReward;
		break;
	case 4:
		cout << " Life Points." << endl;
		this->lp = this->lp + numReward;
		break;
	case 5:
		cout << " Victory Points." << endl;
		this->vp = this->vp + numReward;
		break;
	}
}

/**
* Selects the Unit to be destroyed.
*@param units The units in the player's borough
*@return the index of the unit to be destroyed.
*/
int Player::unitToDestroy(int destructCtr, vector<Tile> units)
{
	string input;
	int numInput;
	bool goodInput = false;
	while (!goodInput)
	{
		cout << "\nPlease select the Unit to destroy:" << endl;
		for (int i = 0; i < units.size(); i++)
		{
			cout << "\t[" << i << "] " << units[i] << endl;
		}
		cout << "Option selected: ";

		numInput = this->strategy->chooseUnitToDestroy(destructCtr, units);

		if ((numInput >= 0) && (numInput < units.size()))
		{
			goodInput = true;
		}
		else
		{
			cout << "Invalid input.  Please select from the given options. " << endl;
		}
	}
	return numInput;
}

/**
*Selects the building to be destroyed.
*@param buildings The buildings present in the player's borough.
*@return the index of the building to be destroyed.
*/
int Player::buildingToDestroy(int destructCtr, vector<TileStack> buildings)
{
	string input;
	int numInput;
	bool goodInput = false;

	while (!goodInput)
	{
		cout << "\nPlease select the Building to destroy:" << endl;
		for (int i = 0; i < buildings.size(); i++)
		{
			cout << "\t[" << i << "] " << buildings[i].getTopTile() << endl;
		}
		cout << "Option selected: ";

		numInput = this->strategy->chooseBuildingToDestroy(destructCtr, buildings);
		if ((numInput >= 0) && (numInput < buildings.size()))
		{
			goodInput = true;
		}
		else
		{
			cout << "Invalid input.  Please select from the given options. " << endl;
		}
	}
	return numInput;
}

int Player::setUpInput(string message, string error, int min, int max) {
	int valid = false;
	int value;
	string input;
	do {
		cout << message;
		value = strategy->setUpInput(min, max);
		cout << endl;
		if (value >= min && value <= max)
			valid = true;
		else
			cout << error;
	} while (valid == false);
	return value;
}