#include <vector>
#include <iostream>
#include "TileSetUpDriver.h"
#include "Reward.h"
/*
Driver for tests and set up of Building/Unit Tiles
@author Hau Gilles Che
@version 1.0 10/10/2018
*/
namespace TileSetUpDriver
{
	/*
	Triggers all the tests functions for the implementation of the Building/Unit tiles.
	*/
	void testAll()
	{
		std::vector<Tile> tiles = generateTiles();
		TileStack shuffledTileStack = shuffleTiles(tiles);
		setUpTilesForGame(shuffledTileStack, 15, 3);
	}

	/*
	Shuffle a stack of tiles for game set up.
	@param tiles vector of Tiles to be shuffled.
	@return a TileStack containing the randomly shuffled tiles.
	*/
	TileStack shuffleTiles(std::vector<Tile> tiles)
	{
		TileStack tileStack(tiles);
		TileStack shuffledTileStack(tiles);
		int stackSize = (int)tiles.size();

		//Shuffles the tiles
		shuffledTileStack.shuffle();
		//for display/test purposes, the original shuffled stack will be emptied so generate an additional object with shuffled tiles to be returned.
		TileStack shuffledTileStackCopy(shuffledTileStack.getTiles());

		
		//std::cout << "***********Tiles before shuffling VS After Shuffling**********" << std::endl;
		for(int i = 0; i < stackSize; i++)
		{
			Tile preShuffle = tileStack.removeTopTile();
			Tile postShuffle = shuffledTileStack.removeTopTile();
			/*
			std::cout << "Pre Shuffle=";
			std::cout << "Building Durability:" << preShuffle.getBuildingDurability();
			std::cout << ";Post Shuffle=";
			std::cout << "Building Durability:" << postShuffle.getBuildingDurability() << std::endl;
			*/
		}
		//std::cout << "*******************End of Shuffle Test**********************" << std::endl;

		return shuffledTileStackCopy;
	}

	/*
	Creates all the stacks of tiles for game board set up. (Game Rules Says to create stacks of 3 building tiles).
	@param tiles Stack of all building/unit tiles in the game. As per King Of New York Game Rules there is 45 tiles.
	@param numOfStacks amount of stacks to be generated for the game. As per King Of New York Game Rules there is 15 stacks (3 per borough).
	@param numOfTilesPerStack amount of Tiles in each stack to be created. As per King Of New York Game Rules each stack contains 3 tiles.
	*/
	void setUpTilesForGame(TileStack tiles, int numOfStacks, int numOfTilesPerStack)
	{
		std::vector<TileStack> tileStacks;
		tileStacks.reserve(numOfStacks);
		
		//Temporary vars used to populate the tileStacks vector
		TileStack aTempTileStack;
		std::vector<Tile> tempTile;
		tempTile.reserve(numOfTilesPerStack);

		for (int i = 0; i < numOfStacks; i++)
		{
			for (int i = 0; i < numOfTilesPerStack; i++)
			{
				tempTile.push_back(tiles.removeTopTile());
			}
			aTempTileStack.setTiles(tempTile);
			tileStacks.push_back(aTempTileStack);
			tempTile.clear();
		}
		/*
		std::cout << "****************Testing the start of game set up for Tiles*****************************" << std::endl;
		displayTileStacks(tileStacks);
		std::cout << "************************End of Tiles Game Set Up***************************************" << std::endl;
		*/
	}

	/*
	Creates all the stacks of tiles for game board set up. (Game Rules Says to create stacks of 3 building tiles).
	@param tiles Stack of all building/unit tiles in the game. As per King Of New York Game Rules there is 45 tiles.
	@param numOfStacks amount of stacks to be generated for the game. As per King Of New York Game Rules there is 15 stacks (3 per borough).
	@param numOfTilesPerStack amount of Tiles in each stack to be created. As per King Of New York Game Rules each stack contains 3 tiles.
	*/
	std::vector<TileStack> createTileStacksForGame(TileStack tiles, int numOfStacks, int numOfTilesPerStack)
	{
		std::vector<TileStack> tileStacks;
		tileStacks.reserve(numOfStacks);

		//Temporary vars used to populate the tileStacks vector
		TileStack aTempTileStack;
		std::vector<Tile> tempTile;
		tempTile.reserve(numOfTilesPerStack);

		for (int i = 0; i < numOfStacks; i++)
		{
			for (int i = 0; i < numOfTilesPerStack; i++)
			{
				tempTile.push_back(tiles.removeTopTile());
			}
			aTempTileStack.setTiles(tempTile);
			tileStacks.push_back(aTempTileStack);
			tempTile.clear();
		}
		return tileStacks;

	}

	/*
	Displays the content of each TileStack
	@param tilestack vector of Tile Stacks containing Tiles to display.
	*/
	void displayTileStacks(std::vector<TileStack> tileStack)
	{
		int numOfStacks = (int)tileStack.size();

		for (int i = 0; i < numOfStacks; i++)
		{
			std::cout << "STACK #" << i << "= " << std::endl;
			for (Tile t : tileStack.back().getTiles() )
			{
				std::cout << "Building Durability: " << t.getBuildingDurability() << "; Unit Durability: " << t.getUnitDurability() << ", ";
			}
			std::cout << std::endl;
			tileStack.pop_back();
		}
	}

	/*
	Generates all the building/unit tiles in the game.
	@return vector containing all the game tiles.
	*/
	std::vector<Tile> generateTiles()
	{
		std::vector<Tile> tiles;
		//Values set to the actual amount of each tiles in the physical game.
		int numBuilding1ToCreate = 25;

		int numBuildingTallD2ToCreate = 8;
		int numHospitalD2ToCreate = 3;
		int numPowerPlantD2ToCreate = 3;

		int numBuildingTallD3ToCreate = 3;
		int numHospitalD3ToCreate = 3;
		int numPowerPlantD3ToCreate = 3;
		int totalTiles = numBuilding1ToCreate + numBuildingTallD2ToCreate + numHospitalD2ToCreate + numPowerPlantD2ToCreate + numBuildingTallD3ToCreate + numHospitalD3ToCreate + numPowerPlantD3ToCreate;

		tiles.reserve(totalTiles);

		//Generate All types of building/unit tiles
		std::vector<Tile> buildingsD1 = generateDurability1Building(numBuilding1ToCreate);
		std::vector<Tile> buildingsD2 = generateDurability2Building(numBuildingTallD2ToCreate, numHospitalD2ToCreate, numPowerPlantD2ToCreate);
		std::vector<Tile> buildingsD3 = generateDurability3Building(numBuildingTallD3ToCreate, numHospitalD3ToCreate, numPowerPlantD3ToCreate);

		//Creating a vector of all the tiles
		tiles.insert(tiles.end(), buildingsD1.begin(), buildingsD1.end());
		tiles.insert(tiles.end(), buildingsD2.begin(), buildingsD2.end());
		tiles.insert(tiles.end(), buildingsD3.begin(), buildingsD3.end());

		return tiles;
	}

	/*
	Creates a requested amount of Building tiles with durability 1.
	Durability 1 Building (As per King of New York Game Rules):
	-Each durability 1 building has a 1 Celebrity reward and an Infantry as a unit.
	-an Infantry has durability 2 and 1 Heal reward.

	@param numToCreate amount of Building of Durability 1 to create.
	@return vector containing the created Buildings of Durability 1.
	*/
	std::vector<Tile> generateDurability1Building(int numToCreate)
	{
		std::vector<Tile> buildings;

		//Instantiation of rewards
		Reward buildingReward(CELEBRITY,1);
		Reward unitReward(HEAL, 1);

		buildings.reserve(numToCreate);

		Tile buildingD1(1, buildingReward, 2, unitReward, false);
		buildings.assign(numToCreate, buildingD1);
		
		return buildings;
	}

	/*
	Creates a requested amount of Building tiles with durability 2.
	Durability 2 Building (As per King of New York Game Rules):
	-Each durability 2 building has a Jet as a unit.
	-a Jet has durability 3 and 2 Energy reward.

	@param numTallToCreate amount of Tall Building of Durability 2 to create.
	@param numHospitalToCreate amount of Hospital Building of Durability 2 to create.
	@param numPowerPlantToCreate amount of Power Plant Building of Durability 2 to create.
	@return vector containing the created Buildings of Durability 2.
	*/
	std::vector<Tile> generateDurability2Building(int numTallToCreate, int numHospitalToCreate, int numPowerPlantToCreate)
	{
		std::vector<Tile> buildings;
		int numBuildings = numTallToCreate + numHospitalToCreate + numPowerPlantToCreate;
		buildings.reserve(numBuildings);

		//Instantiation of rewards
		Reward tallBuildingReward(CELEBRITY, 2);
		Reward powerPlantBuildingReward(ENERGY, 2);
		Reward hospitalBuildingReward(HEAL, 2);

		Reward jetReward(ENERGY, 2);

		//Instantiation of Tile objects to be added to the vector
		Tile tallBuilding(2, tallBuildingReward, 3, jetReward, false);
		Tile powerPlantBuilding(2, powerPlantBuildingReward, 3, jetReward, false);
		Tile hospitalBuilding(2, hospitalBuildingReward, 3, jetReward, false);

		buildings = insertTiles(buildings, numTallToCreate, tallBuilding);
		buildings = insertTiles(buildings, numHospitalToCreate, hospitalBuilding);
		buildings = insertTiles(buildings, numPowerPlantToCreate, powerPlantBuilding);

		return buildings;
	}

	/*
	Creates a requested amount of Building tiles with durability 3.
	Durability 3 Building (As per King of New York Game Rules):
	-Each durability 3 building has a Tank as a unit.
	-a Tank has durability 4 and 3 Celebrity reward.

	@param numTallToCreate amount of Tall Building of Durability 3 to create.
	@param numHospitalToCreate amount of Hospital Building of Durability 3 to create.
	@param numPowerPlantToCreate amount of Power Plant Building of Durability 3 to create.
	@return vector containing the created Buildings of Durability 3.
	*/
	std::vector<Tile> generateDurability3Building(int numTallToCreate, int numHospitalToCreate, int numPowerPlantToCreate)
	{
		std::vector<Tile> buildings;
		int numBuildings = numTallToCreate + numHospitalToCreate + numPowerPlantToCreate;
		buildings.reserve(numBuildings);

		//Instantiation of rewards
		Reward tallBuildingReward(CELEBRITY, 3);
		Reward powerPlantBuildingReward(ENERGY, 3);
		Reward hospitalBuildingReward(HEAL, 3);

		Reward tankReward(CELEBRITY, 3);

		//Instantiation of Tile objects to be added to the vector
		Tile tallBuilding(3, tallBuildingReward, 4, tankReward, false);
		Tile powerPlantBuilding(3, powerPlantBuildingReward, 4, tankReward, false);
		Tile hospitalBuilding(3, hospitalBuildingReward, 4, tankReward, false);

		buildings = insertTiles(buildings, numTallToCreate, tallBuilding);
		buildings = insertTiles(buildings, numHospitalToCreate, hospitalBuilding);
		buildings = insertTiles(buildings, numPowerPlantToCreate, powerPlantBuilding);
		
		return buildings;
	}

	/*
	Inserts Tile objects into a tile vector.
	@param tiles vector to insert the Tiles into.
	@param numToInsert amount of Tiles to insert in the vector.
	@param tileToInsert Tile to be inserted.
	@return vector containing the inserted Tile objects.
	*/
	std::vector<Tile> insertTiles(std::vector<Tile> tiles,int numToInsert, Tile tileToInsert)
	{
		for (int i = 0; i < numToInsert; i++)
		{
			tiles.push_back(tileToInsert);
		}
		return tiles;
	}
}