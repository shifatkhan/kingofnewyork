/**
	This is an implementation file of the class Card.

	@author Shifat Khan
	@version 1.0 10/08/2018
*/
#include "Card.h"
#include "KnyExceptions.h"

/**
	Default empty constructor.
*/
Card::Card() {
	this->cost = 99;
	this->name = "";
	this->howToPlay = "";
	this->effect = "";
}

/**
	4 param constructor. Takes in the card's description.

	@param cost The cost of the card
	@param name The name of the card
	@param howToPlay How to use the card
	@param effect The outcome of the card
*/
Card::Card(int cost, std::string name, std::string howToPlay, std::string effect) {
	this->cost = cost;
	this->name = name;
	this->howToPlay = howToPlay;
	this->effect = effect;
}

// Accessors
int Card::getCost() { return this->cost; }
std::string Card::getName() const { return this->name; }
std::string Card::getHowtoPlay() const { return this->howToPlay; }
std::string Card::getEffect() const{ return this->effect; }

// Mutators
void Card::setCost(int cost) { this->cost = cost;  }
void Card::setName(std::string name) { this->name = name; }
void Card::setHowtoPlay(std::string howToPlay) { this->howToPlay = howToPlay; }
void Card::setEffect(std::string effect) { this->effect = effect; }

CardEffect Card::nameToCardNamesEnum(std::string cardName) {
	if (cardName == "Ego Trip") {
		return EGO_TRIP;
	}
	else if (cardName == "Next Stage") {
		return NEXT_STAGE;
	}
	else if (cardName == "Power Substation") {
		return POWER_SUBSTATION;
	}
	else if (cardName == "Brooklyn Bridge") {
		return BROOKLYN_BRIDGE;
	}
	else if (cardName == "Subterranean Cable") {
		return SUBTERRANEAN_CABLE;
	}
	else {
		// No such card.
		throw KingOfNewYorkExceptions::CardNotImplementedException();
	}
}

std::istream& operator >>(std::istream& ins, Card& card) {
	std::string costStr;

	// Separate line by ; and get the variables.
	if (getline(ins, costStr, ';') && getline(ins, card.name, ';') && getline(ins, card.howToPlay, ';') &&
		getline(ins, card.effect)) {
		// Convert the string "Cost" from input to int.
		card.cost = stoi(costStr);
	}

	return ins;
}

std::ostream& operator <<(std::ostream& outs, const Card& card){
	outs << card.name;
	return outs;
}