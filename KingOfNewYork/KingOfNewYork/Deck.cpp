/**
	This is an implementation file of the class Deck.
	When a card's cost value is "99", it means that the card does not have any cost
	(not even 0).

	@author Shifat Khan
	@version 1.0 10/08/2018
*/
#include <iostream>
#include <fstream>
#include <algorithm>
#include <random>
#include <chrono>  
#include "Deck.h"
#include "KnyExceptions.h"

/**
	Default empty constructor.
*/
Deck::Deck() {}

Deck::Deck(const Deck& d2) {
	monsterCards = d2.monsterCards;
	specialCards = d2.specialCards;
	powerCards = d2.powerCards;
	discardedCards = d2.discardedCards;
	flippedThreeCards = d2.flippedThreeCards;
}

Deck& Deck::operator= (const Deck& d2) {
	monsterCards = d2.monsterCards;
	specialCards = d2.specialCards;
	powerCards = d2.powerCards;
	discardedCards = d2.discardedCards;
	flippedThreeCards = d2.flippedThreeCards;

	return *this;
}

// Accessors
std::vector<Card> Deck::getMonsterCards() { return this->monsterCards; }
std::vector<Card> Deck::getSpecialCards() { return this->specialCards; }
std::vector<Card> Deck::getPowerCards() { return this->powerCards; }
std::vector<Card> Deck::getDiscardedCards() { return this->discardedCards; }
std::vector<Card> Deck::getFlippedThreeCards() { return this->flippedThreeCards; }

/**
	Reveal 3 new cards. It does so by discarding the currently 3 flipped cards, then
	getting 3 new cards from the power cards deck.
*/
void Deck::flipTopThreeCards() {
	if (this->powerCards.empty()) {
		throw KingOfNewYorkExceptions::EmptyDeckException();
	}

	if (!flippedThreeCards.empty()) {
		// Add currently flipped 3 cards to discard pile.
		for (int i = flippedThreeCards.size() - 1; i >= 0; i--)
		{
			addToDiscardedCards(flippedThreeCards[i]);
			flippedThreeCards.pop_back();
		}
	}
	// Reveal 3 new cards.
	for (int i = 0; i < 3; i++)
	{
		if (!powerCards.empty()) {
			flippedThreeCards.push_back(powerCards.back());
			powerCards.pop_back();
		}
		else {
			// Don't get anymore cards.
			break;
		}
	}

}

/**
	Peek at one of the 3 flipped cards and returns it. This doesn't remove the card from
	the flippedCards

	@return Selected card from flipped cards.
*/
Card Deck::getFlippedCardAtIndex(int i) {
	if (flippedThreeCards.empty()) {
		throw KingOfNewYorkExceptions::EmptyDeckException();
	}
	if (i > flippedThreeCards.size() || i < 0) {
		throw KingOfNewYorkExceptions::DeckIndexOutOfRangeException();
	}

	return flippedThreeCards[i];
}

/**
	Takes one of the 3 flipped cards and returns it. It then draws a new card from the
	power cards deck.

	@return Selected card from flipped cards.
*/
Card Deck::takeFlippedCardAtIndex(int i) {
	if (flippedThreeCards.empty()) {
		throw KingOfNewYorkExceptions::EmptyDeckException();
	}
	if (i > flippedThreeCards.size() || i < 0) {
		throw KingOfNewYorkExceptions::DeckIndexOutOfRangeException();
	}

	Card cardTaken = flippedThreeCards[i];
	if (cardTaken.getHowtoPlay() == "Discard") {
		discardedCards.push_back(cardTaken);
	}
	flippedThreeCards.erase(flippedThreeCards.begin() + i);

	try {
		Card drawNewCard = drawCard();
		flippedThreeCards.push_back(drawNewCard);
	}
	catch (KingOfNewYorkExceptions::EmptyDeckException ede) {
		std::cout << ede.what() << std::endl;
	}

	return cardTaken;
}

Card Deck::drawCard() {
	if (this->powerCards.empty()) {
		throw KingOfNewYorkExceptions::EmptyDeckException();
	}

	Card temp;
	temp = this->powerCards.back();
	this->powerCards.pop_back();
	return temp;
}

void Deck::removeMonsterCard(std::string cardName) {
	for (int i = 0; i < this->monsterCards.size(); i++)
	{
		if (this->monsterCards[i].getName() == cardName) {
			this->monsterCards.erase(this->monsterCards.begin() + i);

			// TODO: Change this design to avoid breaks.
			break;
		}
	}
}

void Deck::removeSpecialCard(std::string cardName) {
	for (int i = 0; i < this->specialCards.size(); i++)
	{
		if (this->specialCards[i].getName() == cardName) {
			this->specialCards.erase(this->specialCards.begin() + i);

			// TODO: Change this design to avoid breaks.
			break;
		}
	}
}

/**
	Populates the deck with monster cards, special cards, and power cards.
*/
void Deck::setupDeck() {
	monsterCards.reserve(6);
	specialCards.reserve(2);
	powerCards.reserve(10);
	discardedCards.reserve(10);
	flippedThreeCards.reserve(3);

	Card tempCard;

	// Create Monster cards
	for (int i = 0; i < 6; i++)
	{
		tempCard.setName(monsterNames[i]);
		monsterCards.push_back(tempCard);
	}

	// Create Special cards
	for (int i = 0; i < 2; i++)
	{
		tempCard.setName(specialNames[i]);
		tempCard.setHowtoPlay(specialHowToPlay[i]);
		tempCard.setEffect(specialEffects[i]);
		specialCards.push_back(tempCard);
	}

	// Create Power cards
	std::fstream fileStream;
	fileStream.open("cards_10.txt", std::ios::in);

	// Get the cards' information line by line
	while (fileStream >> tempCard)
	{
		powerCards.push_back(tempCard);
	}

	fileStream.close();
	shuffle();
	// Flip the top three cards
	flipTopThreeCards();
}

/**
	Shuffles the cards randomly. Makes a seed based around the system clock to emulate randomness.

	Special thanks to user703016's help at https://stackoverflow.com/a/6926473/5648172
*/
void Deck::shuffle() {
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::default_random_engine rng(seed);

	// Shuffle the powerCards
	std::shuffle(begin(powerCards), end(powerCards), rng);

	std::cout << "*Cards shuffled*\n" << std::endl;
}

/**
	Puts a card into the discard pile.
*/
void Deck::addToDiscardedCards(Card card) {
	this->discardedCards.push_back(card);
}

/**
	This function simply outputs all the cards onto the cmd.
	Usually not needed for gameplay since players shouldn't see all the cards.
	Used before and after to show what cards might come up.
*/
void Deck::displayDecks() {
	std::cout << "DECK" << std::endl;

	std::cout << "\tMonster Cards: " << std::endl;
	for (int i = 0; i < monsterCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << monsterCards[i] << std::endl;
	}

	std::cout << "\tSpecial Cards: " << std::endl;
	for (int i = 0; i < specialCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << specialCards[i] << std::endl;
	}

	std::cout << "\tPower Cards: " << std::endl;
	for (int i = 0; i < powerCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << powerCards[i] << std::endl;
	}

	std::cout << "\tDiscarded Cards: " << std::endl;
	for (int i = 0; i < discardedCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << discardedCards[i] << std::endl;
	}

	std::cout << "\tTop 3 Cards" << std::endl;
	for (int i = 0; i < flippedThreeCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << flippedThreeCards[i].getName() <<
			" - Cost: " << flippedThreeCards[i].getCost() <<
			" - Type: " << flippedThreeCards[i].getHowtoPlay() << std::endl;
	}
	std::cout << "\n";
}

/**
	This function outputs the cards that are visible during the game.
	The only card piles the players are able to see are Discarded cards and the 
	Top 3 cards (that are flipped).
*/
void Deck::displayPlayDecks() {
	std::cout << "DECK" << std::endl;

	std::cout << "\tDiscarded Cards: " << std::endl;
	for (int i = 0; i < discardedCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << discardedCards[i] << std::endl;
	}

	std::cout << "\tTop 3 Cards" << std::endl;
	for (int i = 0; i < flippedThreeCards.size(); i++)
	{
		std::cout << "\t\t" << i + 1 << ": " << flippedThreeCards[i].getName() <<
			"\n\t\t\t Cost: " << flippedThreeCards[i].getCost() <<
			"\n\t\t\t Type: " << flippedThreeCards[i].getHowtoPlay() << 
			"\n\t\t\t Effect: " << flippedThreeCards[i].getEffect() << std::endl;
	}
	std::cout << "\n";
}