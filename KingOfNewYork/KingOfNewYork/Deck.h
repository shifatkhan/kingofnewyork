/** This is the header file Deck.h, an interface for the Deck class.
	There are generally 3 types of cards: Monster cards, Special cards, and Power cards.
	The monster cards are nothing but characters (no special abilities for now).

	@author Shifat Khan
	@version 1.0 10/08/2018
*/
#ifndef DECK_H
#define DECK_H

#include <iostream>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "Card.h"

class Deck {
private:
	std::vector<Card> monsterCards;
	std::vector<Card> specialCards;
	std::vector<Card> powerCards;
	std::vector<Card> discardedCards;
	std::vector<Card> flippedThreeCards;

	const std::string monsterNames[6] = {
		"Kong", 
		"Sheriff", 
		"Mantis", 
		"Cpt. Fish", 
		"Rob", 
		"Drakonis" 
	};

	const std::string specialNames[2] = {
		"Statue of Liberty", 
		"Superstar" 
	};
	const std::string specialHowToPlay[2] = { "Goal", "Goal" };
	const std::string specialEffects[2] = {
		"Take this card when you roll at least 3 Ouch! +3 VP when you take this card. -3 VP when you lose this card.",
		"Take this card when you roll at least 3 Celebrity, and gain 1 VP, +1 VP per additional Celebrity rolled. "
			"While you have this card, you gain 1 VP for each Celebrity you roll."
	};

public:
	Deck();
	Deck(const Deck& d2);
	Deck& operator= (const Deck& d2);

	// Accessors
	std::vector<Card> getMonsterCards();
	std::vector<Card> getSpecialCards();
	std::vector<Card> getPowerCards();
	std::vector<Card> getDiscardedCards();
	std::vector<Card> getFlippedThreeCards();
	Card getFlippedCardAtIndex(int i);
	Card takeFlippedCardAtIndex(int i);
	
	Card drawCard();

	void removeMonsterCard(std::string cardName);
	void removeSpecialCard(std::string cardName);

	void setupDeck();
	void shuffle();
	void addToDiscardedCards(Card card);
	void flipTopThreeCards();
	void displayDecks();
	void displayPlayDecks();
};

#endif// DECK_H