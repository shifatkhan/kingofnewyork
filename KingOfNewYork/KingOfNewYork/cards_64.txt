3;Violent Start;Keep;Deal 2 damage to the Monster from whom you take Superstar. Deal 1 damage to the Monster who takes Superstar from you.
4;Sharp Shooter;Keep;You can destroy Jets that are not in your borough. Gain 1 VP each time you destroy a jet.
6;Of Another World;Keep;You can use LP as Energy, and Energy as LP.
7;Extra Head;Keep;You get 1 extra die.
3;Ego Trip;Keep;Gain 1 Energy when you take Superstar and when you start your turn with it.
5;Hailing Cabs;Keep;You may add 2 Destruction to your result. If you do you can destroy only Units.
4;Next Stage;Discard;Lose all your VP. Gain 1 Energy or heal 1 damage for each VP you lost this way.
5;Power Substation;Discard;+1 VP, +8 Energy, and take 3 damage.
5;General Ellis;Discard;You can only buy this card if you destroyed a Tank this turn. +3 VP and all Monsters take 1 damage.
6;New York Marathon;Discard;While this card is available for purchase, Monsters must pay 2 Energy to move (including fleeing from MANHATTAN), unless the movement is obligatory. +2 VP, +2 Energy, and heal 2 damage.
6;Air Force One;Discard;You can only buy this card if you destroyed a Jet this turn. +4 VP and all Units attack all Monsters in their boroughs.
10;Subway;Discard;Heal 2 damage and take another turn. During this extra turn, you can move as often as you like, whenever you like.
0;Subterranean Cable;Discard;+4 Energy and take 4 damage.
5;Tesla Canon;Discard;+2 VP. All Monsters (including you) must discard one Keep card.
5;The Unishpere;Discard;Buy this card for 1 Energy if you are in QUEENS. +4 VP.
9;Yankee Stadium;Discard;Buy this card for 1 Energy less if you are in the BRONX. +5 VP and heal 5 damage.
3;Flatiron Building;Discard;Buy this card for 1 Energy less if you are in MANHATTAN. +2 VP.
5;Central Park;Discard;Buy this card for 1 Energy less if you are in MANHATTAN. +2 VP and heal 2 damage.
4;Coney Island;Discard;Buy this card for 1 Energy less if you are in BROOKLYN. +3 VP.
12;Drink the Hudson;Discard;Buy this card for 1 Energy less if you are in MANHATTAN, BROOKLYN, or STATEN ISLAND. +13 Energy, +1 VP, and heal 2 damage.
4;Photo Op;Discard;Choose a Monster. Gain +1 VP for each damage you dealt to that Monster this turn.
9;Holland Tunnel;Discard;Buy this card for 1 Energy less if you are in MANHATTAN. +6 VP and all Monsters gain 3 Energy.
6;Columbia University;Discard;Buy this card for 1 Energy less if you are in MANHATTAN. +2 VP and take the next Keep card revealed, for free.
6;Brooklyn Bridge;Discard;Buy this card for 1 Energy less if you are in BROOKLYN. +4 VP.
3;Climb the Empire State Building;Keep;If you roll one of each die in Manhattan, gain 2 VP and take another turn.
4;Artificial Heart;Keep;You can change some or all of your Heal or Energy dice.
5;Painbow;Keep;If the # of Destruction you roll is... 2, you get an extra Attack. 4, you get an extra 2 Attack. 6, you WIN the game.
5;Hunter;Keep;Heal 1 damage each time you destroy an Infantry.
4;Super Speed;Keep;You can have a free move before rolling your dice.
5;Personal Spotlight;Keep;Gain 1 VP each turn you rolled at least 1 Celebrity.
4;Terminal Rage;Keep;Take another turn when you buy this card. From now on you can no longer reroll Attack.
5;Leveler;Keep;Gain 1 VP each turn that you destroy at least one Building.
4;Bullet Proof;Keep;Whenever you take damage because of Ouch!, take 1 less damage.
5;King of Queens;Keep;Gain 1 Energy and heal 1 damage when you start your turn in Queens.
4;Curse;Keep;When you deal damage to Monsters, give each a Curse token. Each time a Monster wants to reroll Ouch!, he must spend 1 Energy for each Curse token he has. A Monster may use a Heal to discard a Curse token instead of using it to heal.
4;Fireball;Keep;Your attacks also deal damage to the other Monsters in your borough.
3;Long Neck;Keep;You need only 2 Destruction to destroy a Jet.
5;Trophy Hunter;Keep;Gain 1 VP each time you destroy a Unit.
6;Tourist;Keep;Place a Souvenir token in your current borough, and in each borough you enter that doesn't already have a Souvenir token. Gain 1 VP and 1 Energy each time you place a Souvenir token.
5;Stink Attack;Keep;Whenever you move, you may disperse up to 4 Units from your destination borough. Each dispersed Unit must move to a separate borough.
4;Can Opener;Keep;You only need 3 Destruction to destroy a Tank.
4;Phoenix Blood;Keep;Gain 1 Energy each time you take damage.
4;Tesla Antennae;Keep;Monsters that have more LP than you take 1 extra damage whenever you attack them.
4;Power Tap;Keep;Each time a Monster gains at least 3 Energy in a turn, he must pay you 1 Energy.
4;Stomp;Keep;Add 1 Destruction to your result.
4;Jinx Aura;Keep;Other Monsters must always use all their rerolls. (They still get to choose which dice to reroll.)
4;Seismic Ray;Keep;Spend 1 Energy to use your Destruction in any borough other than where you are.
3;Chinatown Regular;Keep;Heal 2 damage when you enter Manhattan.
3;Scavenger;Keep;You can buy Keep cards from the discard by spending 1 Energy less than their cost.
4;Antimatter Pellets;Keep;Monsters you attack must roll a die. Those that roll 1 Ouch! take double damage.
6;Webspinner;Keep;When you deal damage to Monsters, give them a Web token. A Monster has one fewer reroll on his turn for each Web token he has. A Monster can choose to use an Attack to discard a Web token instead of using it to attack.
6;Universal Soldier;Keep;You can change some or all of your Heal(s) to Attack(s), and/or vice-versa.
4;Fury;Keep;Add 1 Destruction and 1 Energy to your results if you are in a borough with at least 3 Units.
12;Shadow Double;Keep;Whenever you attack, you deal double damage.
4;Drain;Keep;Take 2 Energy from Monsters that attack you.
5;Diva;Keep;The other Monsters need 4 Celebrity to take Superstar from you.
10;Towering Titan;Keep;Add 2 Attacks to your result.
5;Broadway Star;Keep;Gain 1 VP when you take Superstar, and when you start your turn with it.
0;Overload;Keep;Gain 4 Energy when you buy this card. Gain 1 less Energy when you roll Energy.
4;Chameleon;Keep;You can discard as many Celebrity as you wish. For each Celebrity discarded, you can change the face of one of the dice you haven't resolved.
2;Natural Selection;Keep;Gain 4 Energy and heal 4 damage when you buy this card. You roll an extra die. If you end your turn with at least 1 Celebrity, you lose this card, and all you LP.
5;Regeneration;Keep;Heal 1 damage at the start of your turn.
5;Trash Thrower;Keep;Monsters you attack lose 1 VP.
3;Carapace;Keep;Your max LP is increased by 2 as long as you have this card. For each Ouch! you roll, place a Carapace token on this card. You can discard this card to heal 1 damage for each Carapace token on it.
