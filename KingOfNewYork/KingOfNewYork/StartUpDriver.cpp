#include "StartUpDriver.h"
#include "StartUpPhase.h"

namespace StartUpDriver
{
	vector<Player*> setUp(vector<Player*> players,Map* map)
	{
		displayPlayerOrder(players);
		
		players = generatePlayOrder(players);
		testSetMonsterInBorough(players, map);
		return players;
	}

	int determineFirstPlayer(vector<Player*> players)
	{
		int first = StartUpPhase::determineFirstPlayer(players);
		return first;
	}

	vector<Player*> generatePlayOrder(vector<Player*> players)
	{
		int firstPindex = determineFirstPlayer(players);
		vector<Player*> orderedPlayers = StartUpPhase::generatePlayOrder(players, firstPindex);
		std::cout << "\n\n" << std::endl;
		displayPlayerOrder(orderedPlayers);
		return orderedPlayers;
	}

	void testSetMonsterInBorough(vector<Player*> players, Map* map)
	{
		StartUpPhase::setMonstersInBorough(players, map);
	}

	void displayPlayerOrder(vector<Player*> players)
	{
		string pName;
		int paddingNum = 0;
		std::cout << "Players' Play Order:" << std::endl;
		std::cout << " ___________________________" << std::endl;
		std::cout << "|                |          |" << std::endl;
		std::cout << "|NAME            |TURN ORDER|" << std::endl;
		std::cout << "|________________|__________|" << std::endl;
		for (int i = 0; i < players.size(); i++)
		{
			pName = players[i]->getName();
			if (pName.length() > 16)
			{
				pName = pName.substr(0, 13);
				pName = pName.append(".");
				pName = pName.append(".");
				pName = pName.append(".");
			}
			else
			{
				paddingNum = 16 - (int)pName.length();
				pName.append(paddingNum, 0x20);
			}
			std::cout << "|                |          |" << std::endl;
			std::cout << "|" << pName <<  "|    "<< i+1 << "     |" << std::endl;
			std::cout << "|________________|__________|" << std::endl;
		}
		std::cout << "\n\n" << std::endl;
	}
}