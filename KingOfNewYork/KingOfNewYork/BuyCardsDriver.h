/** This is the header file BuyCardsDriver.h an interface for the BuyCardsDriver class.

	@author Shifat Khan
	@version 1.0 11/03/2018
*/
#include <iostream>
#include "Deck.h"
#include "Card.h"
#include "Player.h"
#include "KnyExceptions.h"
#include "MainGameLoop.h"
#include "AiUtil.h"


namespace BuyCardsDriver {
	void start(Player& player, Deck& d, MainGameLoop::MainGameLoop& mainGameLoop);
	void playerTurn(Player& player, Deck& d, MainGameLoop::MainGameLoop& mainGameLoop);
	void buyCardsInput(Player& player, Deck& d, MainGameLoop::MainGameLoop& mainGameLoop);

	//for AI
	void startForAI(Player& player, Deck& d);
	bool canBuySomething(Player& player, Deck& d);
}