#include "Tile.h"
#include "Reward.h"
#include <vector>
/**
Data Representation of a Building/Unit Tile from the game King of New York
@author Hau Gilles Che
@version 1.0 10/11/2018
*/
Tile::Tile()
{
	this->buildingDurability = -1;
	this->unitDurability = -1;
	this->isUnit = false;
}

Tile::Tile(int buildingDurability, Reward buildingReward)
{
	this->buildingDurability = buildingDurability;
	this->buildingReward = buildingReward;
	this->isUnit = false;
}

Tile::Tile(int buildingDurability, Reward buildingReward, int unitDurability, Reward unitReward, bool isUnit)
{
	this->buildingDurability = buildingDurability;
	this->buildingReward = buildingReward;
	this->unitDurability = unitDurability;
	this->unitReward = unitReward;
	this->isUnit = isUnit;
}

/*
@return durability of the Building element
*/
int Tile::getBuildingDurability() { return this->buildingDurability; }
/*
@return Reward for Destroying the building element
*/
Reward Tile::getBuildingReward() { return this->buildingReward; }
/*
@return Durability of the Unit element
*/
int Tile::getUnitDurability() { return this->unitDurability; }
/*
@return Reward for Destroying the Unit element
*/
Reward Tile::getUnitReward() { return this->unitReward; }
/*
@return Whether the Tile is in unit mode (i.e.: The building element has been destroyed and the Tile has been flipped).
*/
bool Tile::getIsUnit() { return this->isUnit; }

/*
@param buildingDurability Durability of the building element
*/
void Tile::setBuildingDurability(int buildingDurability) { this->buildingDurability = buildingDurability; }
/*
@param buildingReward Reward for destroying the building element
*/
void Tile::setBuildingReward(Reward buildingReward) { this->buildingReward = buildingReward; }
/*
@param unitDurability Durability of the unit element
*/
void Tile::setUnitDurability(int unitDurability) { this->unitDurability = unitDurability; }
/*
@param unitReward Reward for destroying the unit element
*/
void Tile::setUnitReward(Reward unitReward) { this->unitReward = unitReward; }
/*
@param isUnit Whether the Tile is in unit mode (i.e.: The building element has been destroyed and tile has been flipped to show unit element)
*/
void Tile::setIsUnit(bool isUnit) { this->isUnit = isUnit; }

/**
*Displays a Tile's attribute
*/
std::ostream& operator <<(std::ostream& outs, const Tile& aTile) {
	Reward reward;
	int rewardCtr;
	int durability;

	if (!aTile.isUnit)
	{
		durability = aTile.buildingDurability;
		reward = aTile.buildingReward;
	}
	else
	{
		durability = aTile.unitDurability;
		reward = aTile.unitReward;
	}

	outs << "Durability: " << durability << " ; Reward: " << reward.getRewardCount() << " " << reward.getRewardSymbol();
	return outs;
}