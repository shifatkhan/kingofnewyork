#pragma once
#include "Observer.h"
//#include "DiceRollingFacility.h"
#include "DiceRollingFacility.h"

class DiceEffectView : public Observer {
public:
	DiceEffectView();
	DiceEffectView(DiceRollingFacility*);
	~DiceEffectView();
	void setDiceRollingFacility(DiceRollingFacility*);
	void Update(int gameState);
	void display();
	void displayDiceEffects(Symbol);
private:
	DiceRollingFacility* _diceRollingFacility;

};