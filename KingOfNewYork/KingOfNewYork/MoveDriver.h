#ifndef MOVEDRIVER_H
#define MOVEDRIVER_H
#include "Player.h"
#include "MoveManager.h"
#include "MainGameLoop.h"

/**
*	Driver for the Move logic
*	@author Thai-Vu Nguyen 26325440
*/

namespace MoveDriver {
	void moveLoop(Player* player, Map* map, int num_of_players, MainGameLoop::MainGameLoop* mainGameLoop);
	void moveOutLoop(Player* player, Map* map, int num_of_players);
	void displayOptions(vector<MoveOption> options);
	string convertOptionToStringMessage(MoveOption option);
	void processInput(Player* player, Map* map ,int num_of_players, MoveOption option);
	void enterManhattan(Player* player, Map* map, int num_of_players);
	void getUpManhattan(Player* player, Map* map, int num_of_players);
	void enterBurroughOutsideManhattan(Player* player, Map* map, int num_of_players);

}

#endif // !MOVEDRIVER_H
