#ifndef MAPUTIL_H
#define MAPUTIL_H

/**
*	MapUtil file containing a set of utility functions concerning the Map
*	@author Thai-Vu Nguyen 26325440
*	version 1.0 10/4/2018
*/

#include "Map.h"
#include "Region.h"
#include "Zone.h"
#include "Player.h"

namespace MapUtil {

	//Validator function
	bool containsManhattan(Map* map);
	bool validatesManhattanZone(Map* map);
	bool isManhattan(Region* region);
	bool isPartOfManhattan(Zone* zone, Map* map);
	bool isManhattanAvailable(Map* map, int num_of_players);
	bool isRegionTakable(Region* region, int max);
	vector<Zone*> getTakableZonesOutisdeManhattan(Map* map);
	vector<Player*> getAllPlayersInsideRegion(Region* region);
	vector<Player*> getAllPlayersInManhattan(Map* map);
	vector<Player*> getAllPlayersOutsideMahattan(Map* map);
	vector<Region*> getNonManhattanRegions(Map* map);
	Region* getManhattan(Map* map);
	Region* zoneToRegion(Zone* zone, Map* map);
	vector<Zone*> getAllManhattanZones(Map* map);
	vector<Zone*> getNextManhattanZone(Map* map, Zone* zone);
	vector<Zone*> getEntryToManhattan(Map* map);
	vector<Zone*> getLowerManhattanZones(Map* map);
	vector<Zone*> getMidManhattanZones(Map* map);
	vector<Zone*> getUpperManhattanZones(Map* map);
	vector<Zone*> getManhattanZones(Region* manhattan, string type);
};

#endif // !MAPUTIL_H
