/**
*	Header file for the MapLoader class.
*	MapLoader is the class that handles the loading from and saving of Map data into a Map file.
*
*	@author Thai-Vu Nguyen 26325440
*	@version 1 10/14/2018
*/

#ifndef MAPLOADER_H
#define MAPLOADER_H

#include "Map.h"
#include "MapExceptions.h"

enum ParserMode {
	FETCHMODE, FETCH_ZONE, FETCH_REGION
};

enum LoadMode {
	DEFAULT, EXISTING, NEW
};

class MapLoader {
private:
	Map* map;
	std::string filepath;
	LoadMode mode;
	const std::string defaultPath = "Map/Default/kony.map";

public:
	MapLoader();
	MapLoader(LoadMode mode, std::string path);
	~MapLoader();
	void activateMap();
	void saveMap();
	Map* getMap();
	void setFilePath(std::string filepath);
	void setLoadMode(LoadMode mode);
	void addRegion(Region * region);
	void addRegion(std::string name);
	void addRegion(std::string name, int vp, int energy);
	void addZone(Zone* zone);
	void addZone(int region_id, std::vector<int> adj);
	
	
private:
	
	std::vector<std::string> splitedData(std::string dataRow);
	std::vector<std::string> getLines(string path);
	bool validateRegionData(std::vector<std::string> splitData);
	bool validateZoneData(std::vector<std::string> splitData);
	Map* loadMap(std::string path);
	bool isUnsignedInteger(std::string value);
	std::string getBaseName(string path, bool withExtension, char delimiter = '/');
	Zone* createZone(std::vector<std::string> dataRow);
	vector<Zone*> createZones(std::vector<std::string> data);
	Region* createRegion(std::vector<std::string> dataRow);
	vector<Region*> createRegions(std::vector<std::string> data);
	Map* createMap(vector<Zone*> zones, vector<Region*> regions);
};

#endif // !MAPLOARD_H
