/**
*	Header file for the Zone bean container
*
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 10/14/2018
*/

#ifndef  ZONE_H
#define ZONE_H

#include <vector>
//#include "Player.h"
class Zone;
class Player;
using namespace std;
class Zone {
private:
	const int adj_cap = 200;
	const string defaultType = "N/A";
	int id;
	bool is_taken;
	int region_id;
	string type;
	std::vector<int> adj_zone_ids;
	Player* player;
	//Region region;
public:
	//Constructors
	Zone();
	Zone(int id);
	Zone(int id, int region_id);
	Zone(int id, int region_id, std::vector<int> adj_zone_ids);
	//Destrutor
	~Zone();
	//Accessors
	int getId();
	bool isTaken();
	int getRegionId();
	std::vector<int> getAdjZoneIds();
	Player* getPlayer();
	string getType();
	//Region getRegion();
	//Mutators
	void setId(int id);
	void setIsTaken(bool decision);
	void setRegionId(int region_id);
	void setAdjZoneIds(vector<int> adj_zone_ids);
	void setPlayer(Player* player);
	void setType(string type);
	//void setRegion(Region region);
};

#endif // 

