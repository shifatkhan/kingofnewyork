#ifndef  REWARD_H
#define REWARD_H
#include "Symbol.h"
/*
Data Representation of a King of New York reward for destroying a Building/Unit Tile.
@author Hau Gilles Che
@version 1.0 10/10/2018
*/
class Reward
{
public:
	//Constructors
	Reward();
	Reward(Symbol rewardSymbol, int rewardCount);
	
	//Accessors
	int getRewardCount();
	Symbol getRewardSymbol();

	//Mutators
	void setRewardCount(int rewardCount);
	void setRewardSymbol(Symbol rewardSymbol);

private:
	int rewardCount;
	Symbol rewardSymbol;
};
#endif // ! REWARD_H
