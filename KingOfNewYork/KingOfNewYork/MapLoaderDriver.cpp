#include <iostream>
#include <string>
#include <vector>
#include "MapLoaderDriver.h"
#include "Map.h"
#include "MapLoader.h"

/**
*	Driver that tests the loading of maps
*	@author Thai-Vu Nguyen 26325440
*/

using namespace std;

void MapLoaderDriverSpace::TestEmptyMapAndSave() {
	cout << "Test case 1:" << endl;
	cout << "Loading an empty map, adding elements and saving map " << endl;
	MapLoader* maploader = new MapLoader(LoadMode::NEW, "Map/Save/Save1.map");

	try {
		maploader->activateMap();
		maploader->addRegion("Manchester");
		maploader->addRegion("Life");
		maploader->addZone(0, { 1 });
		maploader->addZone(1, { 0 });
		maploader->addZone(0, { 0, 1 });
		maploader->getMap()->connectTheEdges();
		maploader->saveMap();

	}
	catch (exception e) {
		cout << e.what() << endl;
		cout << "Something when wrong\n";
	}
	cout << endl;

}

void MapLoaderDriverSpace::TestExistingMapEditAndSave() {
	cout << "Test case 2:" << endl;
	cout << "Loading an existing Map " << endl;
	cout << "Editing it and saving it to another location";
	string loadspot = "Map/Test2/Renuchan.map";
	string savespot = "Map/Save/Renuchan.map";

	MapLoader l = MapLoader(LoadMode::EXISTING, loadspot);

	try {
		l.activateMap();
		Map* map = l.getMap();
		map->connectTheEdges();
		l.addRegion("Da Test Region", 3, 6);
		l.addZone(3, { 1 });
		l.setFilePath(savespot);
		l.saveMap();

	}
	catch (MapExceptions::InvalidMapDataException &i) {
		cout << "Invalid map (Unexpected)\n";
		cout << i.what() << endl;
	}
	catch (exception &e) {
		cout << e.what() << endl;
		cout << "Invalid MAP (Very unexpected)" << endl;
	}
	cout << endl;
}

void MapLoaderDriverSpace::TestBadDataMaps() {
	cout << "Test case 3: " << endl;
	cout << "Testing the loading of bad files, should throw exceptions when bad data occurs" << endl;
	string loadspot = "Map/BadMap/Bad1.map";
	string loadspot2 = "Map/BadMap/Bad2.map";
	string loadspot3 = "Map/BadMap/Bad3.map";
	string savespot = "Map/Save/Bad.map";
	string paths[] = { loadspot, loadspot2, loadspot3 };
	MapLoader load = MapLoader(LoadMode::EXISTING, loadspot);

	for (string path : paths) {
		try {
			load.setFilePath(path);
			load.activateMap();
			//This piece shouldn't reach
			Map* map = load.getMap();
			map->connectTheEdges();
			load.setFilePath(savespot);
			//No saving occurs [Expected]
			load.saveMap();

		}
		catch (MapExceptions::InvalidMapDataException &i) {
			cout << i.what() << endl;
			cout << "Invalid Map (Expected)\n";
		}
		catch (exception &e) {
			cout << e.what();
			cout << "Invalid map (Others)\n";
		}
	}


	//MapLoader maploader();
}

void MapLoaderDriverSpace::TestNoMapLoadedExceptions() {
	cout << "Test case 4:" << endl;
	cout << "Trying to do Map operations when map is not loaded. Purpose of this test is to catching Exceptions\n\n";
	string loadspot = "Map/Bad Map/Bad1.map";
	MapLoader load = MapLoader(LoadMode::EXISTING, loadspot);

	string tests[3] = { "addRegion","addZone", "saveMap" };

	for (string test : tests) {
		if (test == "addZone") {
			cout << "Trying to Add Zone without a loaded map" << endl;
			try {
				load.addZone(1, {});
			}
			catch (MapExceptions::MapNotLoadedException &i) {
				cout << "Exception caught [Expected]\n";
			}
			catch (MapExceptions::InvalidMapDataException &g) {
				cout << "Bad zone data\n";
			}
			catch (exception &e) {
				cout << "Other exceptions caught [Not so expected]\n";
			}

		}
		else if (test == "addRegion")
		{
			cout << "Trying to Add Region without a loaded map" << endl;
			try {
				load.addRegion("life");
			}
			catch (MapExceptions::MapNotLoadedException &i) {
				cout << "Exception caught [Expected]\n";
			}
			catch (MapExceptions::InvalidMapDataException &g) {
				cout << "Bad region data\n";
			}
			catch (exception &e) {
				cout << "Other exceptions caught [Not so expected]\n";
			}

		}
		else if ("saveMap") {
			cout << "Trying to save map without a loaded map" << endl;
			try {
				load.saveMap();
			}
			catch (MapExceptions::MapNotLoadedException &i) {
				cout << "Exception caught [Expected]\n";
			}
			catch (exception &e) {
				cout << "Other exceptions caught [Not so expected]\n";
			}

		}
	}

}


void MapLoaderDriverSpace::runTests() {
	TestEmptyMapAndSave();
	TestExistingMapEditAndSave();
	TestBadDataMaps();
	TestNoMapLoadedExceptions();

}