#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Card.h"
#include "TileStack.h"
#include "DiceRollingFacility.h"
#include "MainGameLoop.h"
#include "Subject.h"

class Zone;
class Map;
class Region;
class PlayerStrategy;


enum PlayerMode {
	Human, Aggressive_AI, Moderate_AI
};

class Player : public Subject {
private:
	int lp;
	int vp;
	std::string name;
	int energy;
	std::vector<Card> keepCards;
	CardEffect cardEffectState;
	PlayerStrategy* strategy;

	Zone* zone;
	DiceRollingFacility* diceRollingFacility;

	// TODO: maybe change this to a string since it has no effect on the gameplay.
	Card monsterCard;

	// TODO: maybe change this to 2 bools representing whether or not you have the card.
	std::vector<Card> specialCards;

	//Helper functions
	Symbol chooseSymbolToResolve(vector<Symbol> symbolsToResolve, MainGameLoop::MainGameLoop* loop);
	int firstOccurenceOfSymbol(Symbol s,vector<Symbol> symbols);
	void destroyTile(int destructCtr, Region* playerRegion, MainGameLoop::MainGameLoop* loop);
	int buildingToDestroy(int destructCtr, vector<TileStack> buildings);
	int unitToDestroy(int destructCtr, vector<Tile> units);
	void processTileRewards(Reward reward);
	bool canDestroyBuildingsTile(vector<TileStack> buildings, int destructCtr);
	bool canDestroyUnitsTile(vector<Tile> units, int destructCtr);
	void resolveOuch(int ouchCtr, Region* playerRegion,Map* map);

public:
	// Initializes the player to 10 LP, 0 VP, and "" name.
	Player();
	Player(int lp, int vp, std::string name, Card monsterCard, DiceRollingFacility* diceRollingFacility);
	~Player();

	// Accessors
	int getLP();
	int getVP();
	std::string getName();
	int getEnergy();
	std::vector<Card> getKeepCards();
	Card getMonsterCard();
	std::vector<Card> getSpecialCards();
	CardEffect getCardEffectState();

	Zone* getZone();
	DiceRollingFacility* getDiceRollingFacility();

	PlayerStrategy* getPlayerStrategy();

	// Mutators
	void setLP(int lp);
	void setVP(int vp);
	void setName(std::string name);
	void setEnergy(int energy);
	void setKeepCards(std::vector<Card> keepCards);
	void setMonsterCard(Card monsterCard);
	void setSpecialCards(std::vector<Card> specialCards);
	void setDiceRollingFacility(DiceRollingFacility* diceRollingFacility);
	void setPlayerStrategy(PlayerStrategy* strategy);
	void setCardEffectState(CardEffect cardEffect);

	void addKeepCard(Card keepCard);
	void removeKeepCard(std::string cardName);

	void removeSpecialCard(std::string cardName);

	void takeDamage();
	void takeDamage(int damage);

	void rollDice();
	void resolveDice(Map* map, int number_of_player, MainGameLoop::MainGameLoop* loop);
	void move(Zone* zone);
	void die();
	void buyCard(Card cardBought);

	void applyCardEffect(Card* card);

	void displayPlayerStatus();
	int setUpInput(string, string, int, int);
};

#endif// !PLAYER_H