#ifndef TILE_H
#define TILE_H
#include <vector>
#include <string>
#include <iostream>
#include "Reward.h"
/**
Data Representation of a Building/Unit Tile from the game King of New York
@author Hau Gilles Che
@version 1.0 10/11/2018
*/
class Tile
{
public:
	//Constructors
	Tile();
	Tile(int buildingDurability, Reward buildingReward);
	Tile(int buildingDurability, Reward buildingReward,int unitDurability, Reward unitReward, bool isUnit);

	//Accessors
	int getBuildingDurability();
	Reward getBuildingReward();
	int getUnitDurability();
	Reward getUnitReward();
	bool getIsUnit();

	//Mutators
	void setBuildingDurability(int buildingDurability);
	void setBuildingReward(Reward buildingReward);
	void setUnitDurability(int unitDurability);
	void setUnitReward(Reward unitReward);
	void setIsUnit(bool isUnit);

	friend std::ostream& operator <<(std::ostream& outs, const Tile& tile);
private:
	int buildingDurability;
	Reward  buildingReward;
	int unitDurability;
	Reward  unitReward;
	bool isUnit;
};
#endif // !TILE_H
