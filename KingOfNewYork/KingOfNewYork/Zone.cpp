#include "Zone.h"


/*
*	Implemention of the Zone bean container
*	@author Thai-Vu Nguyen
*	@version 1.0 10/14/2018
*/

//Constructors

Zone::Zone() {
	this->id = 0;
	this->is_taken = false;
	this->region_id = -999;
	this->player = nullptr;
	this->adj_zone_ids = vector<int>();
	this->adj_zone_ids.reserve(this->adj_cap);
	this->type = defaultType;
}

Zone::Zone(int id) {
	this->id = id;
	this->is_taken = false;
	this->region_id = -999;
	this->player = nullptr;
	this->adj_zone_ids = vector<int>();
	this->adj_zone_ids.reserve(this->adj_cap);
	this->type = defaultType;

}

Zone::Zone(int id, int region_id) {
	this->id = id;
	this->is_taken = false;
	this->player = nullptr;
	this->region_id = region_id;
	this->adj_zone_ids = vector<int>();
	this->adj_zone_ids.reserve(this->adj_cap);
	this->type = defaultType;
}

Zone::Zone(int id, int region_id, vector<int> adj_zone_ids) {
	this->id = id;
	this->is_taken = false;
	this->region_id = region_id;
	this->player = nullptr;
	this->type = defaultType;
	if (adj_zone_ids.size() > this->adj_cap) {
		this->adj_zone_ids = vector<int>();
		this->adj_zone_ids.reserve(adj_zone_ids.size() * 2);
	}else {
		this->adj_zone_ids = vector<int>();
		this->adj_zone_ids.reserve(this->adj_cap);
	}
	for (int i = 0; i < adj_zone_ids.size(); i++) {
		this->adj_zone_ids.push_back(adj_zone_ids.at(i));
	}

}

//Destructor

Zone::~Zone() {
	//cout << "Destructor zone of id "<< this->id<<endl;
	//delete this;
}

//Accessors

int Zone::getId() { return this->id; }
bool Zone::isTaken() { return this->is_taken; }
int Zone::getRegionId() { return this->region_id; }
std::vector<int> Zone::getAdjZoneIds(){return this->adj_zone_ids;}
Player * Zone::getPlayer()
{
	return this->player;
}
string Zone::getType() { return this->type; }
//Region Zone::getRegion() { return this->region; }
//Mutators

void Zone::setId(int id) { this->id = id; }
void Zone::setIsTaken(bool is_taken) { this->is_taken = is_taken; }
void Zone::setRegionId(int region_id) { this->region_id = region_id; }
void Zone::setType(string type) { this->type = type; }
void Zone::setAdjZoneIds(vector<int> adj_zone_ids) { 
	this->adj_zone_ids.clear();
	if (adj_zone_ids.size() > this->adj_zone_ids.capacity()) {
		this->adj_zone_ids.reserve(adj_zone_ids.size()*2);
	}
	for (int i = 0; i < adj_zone_ids.size(); i++) {
		this->adj_zone_ids.push_back(adj_zone_ids.at(i));
	}
}
void Zone::setPlayer(Player * player)
{
	if (player == NULL || player == nullptr) {
		this->player = nullptr;
		setIsTaken(false);
	}
	else {
		this->player = player;
		setIsTaken(true);
	}
}
//void Zone::setRegion(Region region) { this->region = region; }