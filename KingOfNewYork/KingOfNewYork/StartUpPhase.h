#ifndef  STARTUPPHASE_H
#define STARTUPPHASE_H
#include <vector>
#include "Player.h"
namespace StartUpPhase 
{
	int determineFirstPlayer(vector<Player*> players);
	vector<Player*> generatePlayOrder(vector<Player*> players, int indexOfFirstPlayer);
	void setMonstersInBorough(vector<Player*> players, Map* map);
}
#endif // ! STARTUPPHASE_H

