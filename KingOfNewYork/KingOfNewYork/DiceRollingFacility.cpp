#include <string>
#include <array>
#include "DiceRollingFacility.h"
#include "BlackDie.h"
#include "KnyExceptions.h"


using namespace std;
/**
@author Hau Gilles Che
@version 1.0 10/08/2018
*/

const int numOfDice = 6;

DiceRollingFacility::DiceRollingFacility()
{
	this->rollCtr = 0;
}

/**
Creates a DiceRollingFacility Object
@param rollCtr number of rolls completed by player
@param dice player's dice
*/
DiceRollingFacility::DiceRollingFacility(int rollCtr, array<BlackDie,numOfDice> dice)
{
	this->rollCtr = rollCtr;
	this->symbolsToResolve.reserve(numOfDice);
	for (int i = 0; i < numOfDice; i++)
	{
		this->dice[i] = dice[i];	
	}
}

DiceRollingFacility::~DiceRollingFacility(){}

/**
@return 6 dice
*/
array<BlackDie,6> DiceRollingFacility::getDice(){return this->dice;}

/**
@return the dice to be resolved
*/
vector<Symbol> DiceRollingFacility::getSymbolsToResolve() { return this->symbolsToResolve; }

/*
@return the amount of times the dice have been rolled.
*/
int DiceRollingFacility::getRollCtr(){ return this->rollCtr; }

/**
Generates a string representation of all the dice symbols that have been rolled
@return Player's current Black Die symbols
*/
std::array<Symbol, 6> DiceRollingFacility::getSymbols()
{
	std::array<Symbol, numOfDice> symbols;
	for (int i = 0; i < numOfDice; i++)
	{
		symbols[i] = this->dice[i].getSymbol();
	}
	return symbols;
}

vector<Symbol> DiceRollingFacility::getUniqueSymbols() {
	vector<Symbol> symbols;
	Symbol s = symbolsToResolve[0];
	symbols.push_back(s);

	for (int i = 1; i < 6; i++) {
		if (symbolsToResolve[i] != s) {
			s = symbolsToResolve[i];
			symbols.push_back(s);
		}
	}

	return symbols;
}

void DiceRollingFacility::setDice(array<BlackDie, 6> dice) { this->dice = dice; }
void DiceRollingFacility::setRollCtr(int rollCtr){ this->rollCtr = rollCtr; }
void DiceRollingFacility::setSymbolsToResolve(vector<Symbol> symbolsToResolve) { this->symbolsToResolve = symbolsToResolve; }

/*
Rolls all the dice
*/
array<BlackDie, 6> DiceRollingFacility::rollAllDice()
{
	for (int i = 0; i < dice.size(); i++)
		dice.at(i).setIsKept(false);
	return rollDice();
}

/**
Rolls a Player's dice 
@throws exception if attempts to roll more than 3 times.
*/
array<BlackDie,6> DiceRollingFacility::rollDice()
{
	//Player cannot roll more than 3 times.
	if (rollCtr >= 3)
	{
		throw KingOfNewYorkExceptions::OutOfRollException();
	}
	for(int i = 0; i < numOfDice;i++)
	{
		this->dice[i].roll();
	}

	this->sortDice();
	this->rollCtr = this->rollCtr++;
	Notify(7);
	return this->dice;
}

/**
Choose which dice to keep and which ones to reroll.
@param keep dice to keep 
*/
void DiceRollingFacility::chooseDiceToKeep(array<bool,6> keep)
{
	for (int i = 0; i < 6; i++)
	{
		this->dice[i].setIsKept(keep[i]);
	}
}

/*
Sorts Dice by symbol for better readability
*/
void DiceRollingFacility::sortDice()
{
	Symbol si;
	Symbol sj;
	for (int i = 0; i < 6; i++)
	{
		for (int j = 5; j > i; j--)
		{
			si = this->dice[i].getSymbol();
			sj = this->dice[j].getSymbol();
			if (si > sj)
			{
				BlackDie tempD = this->dice[j];
				this->dice[j] = this->dice[i];
				this->dice[i] = tempD;
			}
		}
	}

	//applying the sortSymbols in the sortDice ensures most up to date list.
	sortSymbols();
}

/*
Generates a list of symbols that needs to be resolved.
*/
void DiceRollingFacility::sortSymbols()
{
	this->symbolsToResolve.clear();

	for (BlackDie d : this->dice)
	{
		this->symbolsToResolve.push_back(d.getSymbol());
	}
}

std::ostream& operator <<(std::ostream& outs, const DiceRollingFacility& diceFacility)
{
	array<BlackDie, 6> dice = diceFacility.dice;
	for (int i = 0; i < 6; i++)
	{
		outs << "\t[" << i << "] " << dice[i] << endl;
	}
	return outs;
}