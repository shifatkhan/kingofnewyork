/** The implementation file for MainView. This basically takes care of displaying
	the state at which the game is at currently.

	Big thanks to Varun's explaination at:
	https://thispointer.com/designing-a-board-game-mastermind-in-c-using-mvc-state-and-observer-design-patterns/

	@author Shifat Khan
	@version 1.1 11/17/2018
*/
#include "MainView.h"

/**	Default constructor that takes in the MainGameLoop to keep track of the
	game's state.
*/
MainView::MainView(MainGameLoop::MainGameLoop* mainGameLoop) {
	this->mainGameLoop = mainGameLoop;
}

/**	Takes care of displaying information about game state.
	Checks the MainGameLoop's game state to determine what
	to display.
*/
void MainView::Update(int gameState) {
	switch (mainGameLoop->getGameState())
	{
	case MainGameLoop::PLAYER_TURN:
		playerTurnView();
		break;

	case MainGameLoop::PLAYER_DEAD:
		playerDeadView();
		break;

	case MainGameLoop::RESOLVE_DICE:
		playerResolveDiceView();
		break;

	case MainGameLoop::MOVING:
		playerMovingView();
		break;

	case MainGameLoop::BUYING:
		playerBuyingView();
		break;

	case MainGameLoop::ENDED:
		gameEndedView();
		break;

	case MainGameLoop::STATS_MENU:
		gameStatsView();
		break;
	case MainGameLoop::DICE_ROLL:
		gameDiceRollView();
		break;
	default:
		std::cout << "\nInvalid game state: " << mainGameLoop->getGameState() << std::endl;
		break;
	}
}

/**	Displays information related to when it's a player's turn.
*/
void MainView::playerTurnView() {
	std::cout << "\n---------------- " << mainGameLoop->getCurrentPlayer()->getName() << "'s turn ----------------" << std::endl;
}

/**	Displays information related to when a player is dead.
*/
void MainView::playerDeadView() {
	std::cout << "\n---------------- " << mainGameLoop->getCurrentPlayer()->getName() << " is dead. ----------------" << std::endl;
}

/**	Displays information related to when a player is resolving its dice
*/
void MainView::playerResolveDiceView() {
	std::cout << "\n---------------- " << mainGameLoop->getCurrentPlayer()->getName() << "'s RESOLVE DICE action. ----------------" << std::endl;
}

/**	Displays information related to when a player is moving.
*/
void MainView::playerMovingView() {
	std::cout << "\n---------------- " << mainGameLoop->getCurrentPlayer()->getName() << "'s MOVE action. ----------------" << std::endl;
}

/**	Displays information related to when a player is buying cards.
*/
void MainView::playerBuyingView() {
	std::cout << "\n---------------- " << mainGameLoop->getCurrentPlayer()->getName() << "'s BUY CARDS action. ----------------" << std::endl;
}

/**	Displays information related to when the game ends.
*/
void MainView::gameEndedView() {
	std::cout << "\n**************** " << mainGameLoop->getWinner()->getName() << " WON THE GAME. ****************" << std::endl;
}

void MainView::gameDiceRollView() {
	std::cout << "\n---------------- " << mainGameLoop->getCurrentPlayer()->getName() << "'s ROLL DICE action. ----------------" << std::endl;
}

/**	Displays information related to the overall game's statistics.
*/
void MainView::gameStatsView() {
	std::cout << "\n\n---------------- GAME STATISTICS ----------------" << std::endl;

	bool exit = false;
	string input;
	int choice;

	// Loop until user wants to exit.
	while (!exit)
	{
		cout << "What do you want to view?" << endl;
		cout << "[1] View players' stats." << endl;
		cout << "[2] View map." << endl;
		cout << "[3] View deck." << endl;
		cout << "[-1] Exit menu." << endl;
		cout << "Choose your option: ";
		cin >> input;
		choice = atol(input.c_str());

		switch (choice)
		{
		case -1:
			exit = true;
			break;

		case 1:
			mainGameLoop->showPlayersStatus();
			break;

		case 2:
			mainGameLoop->getMap()->printMap();
			break;

		case 3:
			mainGameLoop->getDeck().displayPlayDecks();
			break;

		default:
			cout << "Invalid input. Please choose from the selection." << endl;
			break;
		}
	}
}