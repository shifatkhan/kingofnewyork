/** Implementation file for MainGameLoop.h
	This is the main loop of the game where we
	go through each players' turn and determine who won.

	@author Shifat Khan
	@version 2.0 11/15/2018
*/
#include "MainGameLoop.h"
#include "BuyCardsDriver.h"
#include "MoveDriver.h"
#include <stdlib.h>
#include "PlayerActor.h"
#include "Subject.h"
#include "DiceEffectView.h"

namespace MainGameLoop{

	/**	3 param Constructor that takes in the necessary data to start
		the game's loop.
	*/
	MainGameLoop::MainGameLoop(vector<Player*> players, Deck deck, Map* map) {
		this->players = players;
		this->deck = deck;
		this->map = map;
	}

	void MainGameLoop::mainLoop() {
		winner = NULL;
		int round = 1;
		DiceEffectView* diceVew = new DiceEffectView();

		// Loop until someone won the game.
		for (int i = 0; i < players.size(); i++)
		{
			system("CLS");
			if ( players[i]->getLP() <= 0) {
				// Current player is dead. Skip its turn.
				setGameState(PLAYER_DEAD);
			}
			else {
				diceVew->setDiceRollingFacility(players[i]->getDiceRollingFacility());
				//diceVew->setPlayer(players[i]);
				// Set current player for observer.
				currentPlayerIndex = i;

				setGameState(PLAYER_TURN);

				PlayerActor actor = PlayerActor();
				actor.setPlayerStrategy(players[i]->getPlayerStrategy());

				setGameState(DICE_ROLL);
				actor.rollDice(players[i], this);
				//rollDice(players[i]);
				setGameState(RESOLVE_DICE);
				//players[i]->resolveDice(map, players.size());
				actor.resolveDice(players[i], map, players.size(), this);


				// Move.
				if (players[i]->getLP() < 1) {
					setGameState(PLAYER_DEAD);
					players[i]->die();
				}
				else {
					setGameState(MOVING);
					//MoveDriver::moveLoop(players[i], map, players.size(), this);
					actor.move(players[i], map, players.size(), this);
					setGameState(BUYING);
					//BuyCardsDriver::start(*players[i], deck, *this);
					actor.buyCards(players[i], deck, *this);
					// TODO: Remove these two. Used only for Testing purposes.
					showPlayersStatus();
					modifyPlayersData();
				}

				freeDeadPlayersFromZones();

				winner = checkWinner();
				if (winner != NULL) {
					// Somebody won.
					break;
				}

				// Loop back to the 1st player's turn after the last player's turn.
				if (i == players.size() - 1) {
					i = -1;
					round++;
				}

				// Pauses program before immediately jumping to next player's turn.
				system("pause");
			}
		}

		setGameState(ENDED);
		system("pause");
	}

	/**
		Checks if a player won the game. Winner is determined by the
		player that reached 20VP and survived, OR the player is the lone
		survivor.
		@return Player* the winner, or NULL if nobody won yet.
	*/
	Player* MainGameLoop::checkWinner() {
		// Check if a player won by reaching 20 VP.
		for (Player* p : players)
		{
			if (p->getVP() >= 20 && p->getLP() > 0)
			{
				return p;
			}
		}

		// Check if a lone survivor won by counting how many are alive.
		Player* winner = NULL;
		int alive = 0;

		// TODO: break out of loop when alive > 1? Change for better big-O
		for (Player* p : players)
		{
			if (p->getLP() > 0) {
				alive++;
				winner = p;
			}
		}

		if (alive == 1) {
			return winner;
		}

		return NULL;
	}


	void MainGameLoop::showPlayersStatus() {
		for (int i = 0; i < players.size(); i++)
		{
			std::cout << "[" << i + 1 << "]" << endl;
			players[i]->displayPlayerStatus();
		}
	}

	void MainGameLoop::freeDeadPlayersFromZones() {
		for (int i = 0; i < players.size(); i++) {
			if (players[i]->getLP() < 1) {
				if (players[i]->getZone() != NULL && players[i]->getZone() != nullptr)
					players[i]->die();
			}
		}
	}

	void MainGameLoop::modifyPlayersData() {
		string choice = "y";
		int playerNum = 0;
		int statNum = 0;
		string input;

		while (choice == "y" || choice == "Y") {
			std::cout << "TEST - Do you want to change players' data? (Y/N): ";

			if (std::cin >> choice && choice == "y" || choice == "Y") {
				showPlayersStatus();

				std::cout << "Which player do you want to modify? (enter number): ";
				std::cin >> input;
				playerNum = atol(input.c_str());

				if (playerNum > 0 || playerNum <= players.size()) {
					std::cout << "\nChanging player " << players[playerNum - 1]->getName() << endl;
					players[playerNum - 1]->displayPlayerStatus();

					std::cout << "Enter # of what you want to change (LP = 1, VP = 2, Energy = 3): ";
					std::cin >> input;
					statNum = atol(input.c_str());

					int newStat = 0;
					switch (statNum)
					{
					case 1:
						std::cout << "Enter player's new LP: ";
						std::cin >> input;
						newStat = atol(input.c_str());

						if (newStat >= 0 || newStat <= 10)
							players[playerNum - 1]->setLP(newStat);
						break;

					case 2:
						std::cout << "Enter player's new VP: ";
						std::cin >> input;
						newStat = atol(input.c_str());
						if (newStat >= 0)
							players[playerNum - 1]->setVP(newStat);
						break;

					case 3:
						std::cout << "Enter player's new Energy: ";
						std::cin >> input;
						newStat = atol(input.c_str());
						if (newStat >= 0)
							players[playerNum - 1]->setEnergy(newStat);
						break;
					default:
						std::cout << "Changed nothing.\n\n";
						break;
					}
				}
			}
			std::cout << "\n";
		}
	}

	/**
	* Displays the options a player can choose from during reroll phase.
	*/
	void MainGameLoop::displayRollOptions()
	{
		cout << "\nSelect one of the options below:" << endl;
		cout << "[1] Keep all your dice." << endl;
		cout << "[2] Reroll between 1 to 6 die." << endl;
		cout << "[-1] See Game statistics." << endl;
		cout << "Choose your option:";
	}

	/**
	* Displays and accept the player's decision on keeping his dice or rerolling them.
	*@return List of the choices the player made.
	*/
	array<bool,6> MainGameLoop::displayDiceToKeep(array<BlackDie, 6> dice)
	{
		array<bool, 6> toKeepInput;
		bool goodInput;
		string input;
		int inputNum;

		cout << "\nSelecting the dice to keep:"<<endl;
		for (int i = 0; i < 6; i++)
		{
			goodInput = false;
			while (!goodInput)
			{
				cout << "\nDie: [" << i << "] " << dice[i] << endl;
				cout << "Select one of the options below:" << endl;
				cout << "[1] Keep die." << endl;
				cout << "[2] Reroll die." << endl;
				cout << "[-1] See Game statistics." << endl;
				cout << "Choose your option: ";
				cin >> input;
				inputNum = atol(input.c_str());

				switch (inputNum)
				{
				case -1:
					// Access the Game's statistics menu.
					setGameState(STATS_MENU);
					break;
				case 1:
					toKeepInput[i] = true;
					goodInput = true;
					break;
				case 2:
					toKeepInput[i] = false;
					goodInput = true;
					break;
				default:
					cout << "Invalid input. Please choose from the selection." << endl;
					break;
				}
			}
		}
		return toKeepInput;
	}

	// Accessors / Mutators
	vector<Player*> MainGameLoop::getPlayers() {
		return players;
	}
	
	Deck MainGameLoop::getDeck() {
		return deck;
	}

	Map* MainGameLoop::getMap() {
		return map;
	}

	Player* MainGameLoop::getCurrentPlayer() {
		return players[currentPlayerIndex];
	}

	Player* MainGameLoop::getWinner() {
		return winner;
	}

	GameState MainGameLoop::getGameState() const {
		return gameState;
	}

	/**	Change the game's state.
		Whenever state has been changed, notify the observers (gameview)
	*/
	void MainGameLoop::setGameState(GameState gameState) {
		this->gameState = gameState;

		Notify(gameState);
	}
}