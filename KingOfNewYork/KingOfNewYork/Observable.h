#ifndef OBSERVABLE_H
#define OBSERVABLE_H
#include <list>
using namespace std;
class Observer;

class Observable
{
public:
	Observable();
	~Observable();
	virtual void attach(Observer *);
	virtual void detach(Observer *);
	virtual void notify() = 0;
private:
	list<Observer*> *_observers;
};
#endif // OBSERVABLE_H