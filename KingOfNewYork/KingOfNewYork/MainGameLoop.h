#ifndef MAINGAMELOOP_H
#define MAINGAMELOOP_H
/** This is the header file MainGameLoop.h an interface for the MainGameLoop class.

	@author Shifat Khan
	@version 1.0 11/03/2018
*/
#include <iostream>
#include "Deck.h"
#include "Card.h"
#include "Map.h"
#include "Subject.h"
#include "DiceRollingFacility.h"
//#include "Player.h"
class Player;

namespace MainGameLoop {
	enum GameState
	{
		PLAYER_TURN = 0,
		PLAYER_DEAD,
		RESOLVE_DICE,
		MOVING,
		BUYING,
		ENDED,
		STATS_MENU,
		DICE_ROLL
	};

	class MainGameLoop : public Subject {
	public:
		MainGameLoop (vector<Player*> players, Deck deck, Map* map);

		// Accessors / Mutators
		vector<Player*> getPlayers();
		Deck getDeck();
		Map* getMap();
		Player* getCurrentPlayer();
		Player* getWinner();
		GameState getGameState() const;
		void setGameState(GameState gameState);

		void mainLoop();
		Player* checkWinner();
		void showPlayersStatus();
		void freeDeadPlayersFromZones();
		//DEPRECATED?
		//User interaction function
		void displayRollOptions();
		array<bool, 6> displayDiceToKeep(array<BlackDie, 6> dice);
		// For testing purposes only
		void modifyPlayersData();

	private:
		vector<Player*> players;
		Deck deck;
		Map* map;

		Player* winner;
		int currentPlayerIndex;
		GameState gameState;
	};
}
#endif // MAINGAMELOOP_H