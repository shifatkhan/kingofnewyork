#ifndef PLAYERMOVETESTER_H
#define PLAYERMOVETESTER_H
#include "Player.h"
#include "MapLoader.h"
#include <iostream>

namespace PlayerMoveTester {
	void testPlayerMove();
}

#endif // !PLAYERMOVETESTER_H
