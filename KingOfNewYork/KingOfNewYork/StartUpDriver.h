#ifndef STARTUPDRIVER_H
#define STARTUPDRIVER_H
#include <vector>
#include "Player.h"

namespace StartUpDriver
{
	vector<Player*> setUp(vector<Player*> players,Map* map);
	int determineFirstPlayer(vector<Player*> players);
	vector<Player*> generatePlayOrder(vector<Player*> players);
	void testSetMonsterInBorough(vector<Player*> players, Map* map);
	void displayPlayerOrder(vector<Player*> players);
}
#endif // !STARTUPDRIVER_H

