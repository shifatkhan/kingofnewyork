/**
*	Implementation file for the Map class. Serves as a data structure containing Zones and Regions.
*
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 10/14/2018
*/


#include "Map.h"
#include "Player.h"

//struct duo definitions
duo::duo() {
	this->id = -1;
	this->visited = false;
}

duo::duo(int id, bool visited)
{
	this->id = id;
	this->visited = visited;
}


/*
*	Default Constructor
*/
Map::Map() {
	this->map_name = "";
	this->zone_capacity = 300;
	this->region_capacity = 100;
	this->zone_size = 0;
	this->region_size = 0;
	this->zones = new vector<Zone*>(this->zone_capacity);
	this->zones->clear();
	this->zones->reserve(this->zone_capacity);
	this->regions = new vector<Region*>(this->region_capacity);
	this->regions->clear();
	this->regions->reserve(this->region_capacity);
	this->adjacencyZoneList = vector<vector<int>>(this->zone_capacity);
	this->regionZoneList = vector<vector<int>>(this->region_capacity);

	
	//Initializing the inner vectors
	this->resetLists();

}

Map::Map(std::string map_name)
{
	this->map_name = map_name;
	this->zone_capacity = 300;
	this->region_capacity = 100;
	this->zone_size = 0;
	this->region_size = 0;
	this->zones = new vector<Zone*>(this->zone_capacity);
	this->zones->clear();
	this->zones->reserve(this->zone_capacity);
	this->regions = new vector<Region*>(this->region_capacity);
	this->regions->clear();
	this->regions->reserve(this->region_capacity);
	this->adjacencyZoneList = vector<vector<int>>(this->zone_capacity);
	this->regionZoneList = vector<vector<int>>(this->region_capacity);


	//Initializing the inner vectors
	this->resetLists();
}



Map::Map(std::string map_name, int ZONE_CAPACITY, int REGION_CAPACITY)
{
	this->map_name = map_name;
	this->zone_capacity = ZONE_CAPACITY;
	this->region_capacity = REGION_CAPACITY;
	this->zone_size = 0;
	this->region_size = 0;
	this->zones = new vector<Zone*>(this->zone_capacity);
	this->zones->clear();
	this->zones->reserve(this->zone_capacity);
	this->regions = new vector<Region*>(this->region_capacity);
	this->regions->clear();
	this->regions->reserve(this->region_capacity);
	this->adjacencyZoneList = vector<vector<int>>(this->zone_capacity);
	this->regionZoneList = vector<vector<int>>(this->region_capacity);

	//Initializing the inner vectors
	this->resetLists();

}

/*
*	Destructor
*/
Map::~Map() {
	
	for (int i = 0; i < this->regions->size(); i++) {
		if (regions->at(i) != NULL && regions->at(i) != nullptr) {
			delete regions->at(i);
			regions->at(i) = NULL;
		}
		
	}
	regions->clear();
	delete regions;
	regions = NULL;

	zones->clear();
	delete zones;
	zones = NULL;
	
	
}

//Accessors
std::string Map::getMapName() { return this->map_name; }

std::vector<Zone*>* Map::getZones() { return this->zones; }

std::vector<Region*>* Map::getRegions() { return this->regions; }

int Map::getZoneSize() { return this->zone_size; }

int Map::getRegionSize() { return this->region_size; }

Region* Map::getRegionById(int id) {
	for (int i = 0; i < this->regions->size(); i++) {
		if (this->regions->at(i)->getId() == id)
			return this->regions->at(i);
	}
	return nullptr;
}

Zone * Map::getZoneById(int id)
{
	for (int i = 0; i < this->zones->size(); i++)
	{
		if (this->zones->at(i)->getId() == id) {
			return this->zones->at(i);
		}
	}
	return nullptr;
}

void Map::setMapName(string map_name) { this->map_name = map_name; }


/*
*	Add a region in the map.
*	Doesn't connect it yet. Call connecteTheEdges() for this behaviour
*/
void Map::addRegion(Region * region)
{
	if (region == NULL || region == nullptr)
		throw MapExceptions::NullPtrException();
	if (this->region_size >= this->region_capacity)
		throw MapExceptions::RegionOutOfRangeException();
	if (region->getId() >= this->region_capacity)
		throw MapExceptions::RegionOutOfRangeException();
	if (this->regionExist(region->getId()))
		throw MapExceptions::SameIDException();

	this->regions->push_back(region);
	this->region_size++;
}

/*
*	Add a Zone in the map
*	Will not connect the zone in the adjacency list and region list however. Call ConnectTheEdge() function after adding Zone for the connection
*/
void Map::addZone(Zone * zone)
{
	if(zone == NULL || zone == nullptr)
		throw MapExceptions::NullPtrException();
	if (this->zone_size >= this->zone_capacity)
		throw MapExceptions::ZoneOutOfRangeException();
	if (zone->getId() >= this->zone_capacity)
		throw MapExceptions::ZoneOutOfRangeException();

	if (zoneExist(zone->getId())) {
		throw MapExceptions::SameIDException();
	}

	if (regionExist(zone->getRegionId()) == false)
		throw MapExceptions::NoEntityMatchException();

	this->zones->push_back(zone);
	this->zone_size++;
}



void Map::addEdge(int zone_id1, int zone_id2, bool ordered)
{
	//Check if the zones exists
	if (this->zoneExist(zone_id1) == false || this->zoneExist(zone_id2)) {
		throw MapExceptions::ZoneOutOfRangeException();
	}
	if(isZonesConnected(zone_id1, zone_id2) == false)
		this->adjacencyZoneList.at(zone_id1).push_back(zone_id2);
	if (ordered == false) {
		if(isZonesConnected(zone_id2, zone_id1) == false)
			this->adjacencyZoneList.at(zone_id2).push_back(zone_id1);
	}
}

Region* Map::getRegionByZone(Zone* zone) {
	return this->getRegionById(zone->getRegionId());
}



vector<int> Map::getAdjZoneIDs(int zone_id)
{
	if (this->zoneExist(zone_id) == false)
		return vector<int>();
	return this->adjacencyZoneList.at(zone_id);
}

/*
*	Fetches all adjacent Zones to a Zone_id
*/
vector<Zone*> Map::getAdjZones(int zone_id)
{
	vector<Zone*> adj_zones = vector<Zone*>();
	vector<int> adj_id = this->adjacencyZoneList.at(zone_id);
	for (int id : adj_id) {
		adj_zones.push_back(this->zones->at(id));
	}
	return adj_zones;

}

/*
*	Fetches all adjacent Zones to a Zone 
*/
vector<Zone*> Map::getAdjZones(Zone zone)
{
	return this->getAdjZones(zone.getId());
}

/**
* Fetch all the Zone ids belonging to a region
*/
vector<int> Map::getZoneIdsByRegion(Region * region)
{
	return this->getZoneIdsByRegion(region->getId());
}

/**
* Fetch all the Zone ids belonging to a region
*/
vector<int> Map::getZoneIdsByRegion(int region_id)
{
	if (regionExist(region_id) == false) {
		//Return empty vector
		return vector<int>();
	}
	return this->regionZoneList.at(region_id);
}

/*
* Returns a vector of Zones under a region
*/
vector<Zone*> Map::getZonesByRegion(Region* region) {
	return this->getZonesByRegion(region->getId());
}

/*
* Returns a vector of Zones under a region
*/
vector<Zone*> Map::getZonesByRegion(int region_id) {
	vector<Zone*> zones = vector<Zone*>();
	zones.reserve(this->zone_capacity);
	
	vector<int> zone_ids = this->getZoneIdsByRegion(region_id);
	for (int i = 0; i < zone_ids.size(); i++)
	{
		zones.push_back(this->getZoneById(zone_ids.at(i)));
	}

	return zones;
}

/**
*	This function connects all the zones to the Regions and all the adjacent zones in the map.
*	Use this function after adding all needed Regions and Zones
*	Acts as an update function
*	
*/
void Map::connectTheEdges()
{
	
	//Reset the adjacency and the region's zone list
	this->resetLists();
	//Populate the region zone list
	for (int i = 0; i < this->zones->size();i++) {
		if (this->regionExist(this->zones->at(i)->getRegionId())) {
			this->regionZoneList.at(this->zones->at(i)->getRegionId()).push_back(this->zones->at(i)->getId());
		}
	}
	//Step 2: Connect the adjacencies to the zones
	for (int i = 0; i < this->zones->size();i++) {
		this->adjacencyZoneList.at(zones->at(i)->getId()) = vector<int>();
		for (int j = 0; j < this->zones->at(i)->getAdjZoneIds().size(); j++) {
			if (this->zoneExist(this->zones->at(i)->getAdjZoneIds().at(j))) {
				//At corresponding index, push all adjacent zone ids
				this->adjacencyZoneList.at(zones->at(i)->getId()).push_back(this->zones->at(i)->getAdjZoneIds().at(j));
			}
			
		}
	}

	for (int i = 0; i < this->regions->size(); i++) {
		int region_id = this->regions->at(i)->getId();
		this->regions->at(i)->setZones(this->getZonesByRegion(region_id));
	}
	
	
}


/**
*	Check if the first zone is adjacent to the second zone
*	@return true if src_id is has a connection to dest_id
*/
bool Map::isZonesConnected(int src_id, int dest_id)
{
	vector<int> adj = this->adjacencyZoneList.at(src_id);
	for (int i = 0; i < adj.size(); i++) {
		if (adj.at(i) == dest_id)
			return true;
	}
	return false;
}

/*
*	Performs a regional DFS to check if nodes under a region are connected
*/
bool Map::areAllZonesConnected(int region_id)
{
	vector<bool> visited = this->regionalDFS(region_id);
	for (int i = 0; i < visited.size(); i++)
		if (visited.at(i) == false)
			return false;
	return true;
}

/*
* Performs a complete graph DFS and regional DFS for each regions to check if nodes are connected
*/
bool Map::areAllZonesConnected()
{
	//Checking if the entire map is connected
	//cout << endl<< "areAllZonesConnected function" << endl;
	//cout << "whole DFS check " << endl;
	vector<bool> visited = this->DFS();
	for (int i = 0; i < visited.size(); i++) {
		if (visited.at(i) == false) {
			cout << "whole DFS failure\n";
			return false;
		}
	}

	//Checking if all Zones inside each Region are connected
	cout << "whole DFS passed" << endl;
	//cout << "Region DFS check" << endl;
	for (int j = 0; j < this->regions->size(); j++) {
		if (this->areAllZonesConnected(this->regions->at(j)->getId()) == false) {
			cout << "Regional DFS failure for " << this->regions->at(j)->getRegionName() << endl;
			return false;
		}
	}
	cout << "regional DFS passed" << endl;
	cout << "Map is a connected Graph\n" <<endl;
	return true;
}

void Map::printMap() {
	for (int i = 0; i < this->regions->size(); i++) {
		cout << "Region " << this->regions->at(i)->getRegionName() << endl;
		cout << "\tZones: \n";
		vector<Zone*> regionZones = this->regions->at(i)->getZones();
		for (int j = 0; j < regionZones.size(); j++) {
			cout << "\t\tZone: " << regionZones.at(j)->getId() << " Owner: ";
			if (regionZones.at(j)->isTaken())
				cout << regionZones.at(j)->getPlayer()->getName();
			else
				cout << "None";
			cout << " Extra Type : " << regionZones.at(j)->getType();
			cout << "\n";
		}
		this->regions->at(i)->displayTiles();
	}
}

/*
*	Private method performing complete DFS
*/
vector<bool> Map::DFS() {
	
	vector<duo> visited (zone_capacity);
	
	for (int i = 0; i < this->zone_capacity; i++) {
		visited.at(i) = duo(-1, false);
	}

	//Matching the Adjancency list
	for (int i = 0; i < this->zones->size(); i++) {
		visited.at(this->zones->at(i)->getId()).id = this->zones->at(i)->getId();
	}
	bool lone = false;

	for (int i = 0; i < visited.size(); i++) {
		
		if (visited.at(i).id != -1 && visited.at(i).visited == false) {
			if (this->zones->size() <= 1)
				lone = true;
			//We don't visit the source Vertex
			//If the map is connected, the source Vertex should be
			//visited by another Vertex.
			this->DFSHelper(i, visited, lone);
			lone = false;
		}
			
	}

	vector<bool> visitedVertex;
	visitedVertex.reserve(this->zone_capacity);

	//If the map of n vertex is connected, vector of n bool should all be true.
	for (int i = 0; i < zones->size(); i++) {
		visitedVertex.push_back(visited.at(this->zones->at(i)->getId()).visited);
	}

	return visitedVertex;
}

/*
*	Private method performing DFS per region
*/
vector<bool> Map::regionalDFS(int region_id)
{
	vector<int> zone_ids = this->getZoneIdsByRegion(region_id);
	vector<duo> visited(this->zone_capacity);
	for (int i = 0; i < this->zone_capacity; i++) {
		visited.at(i) = duo(-1, false);
	}
	for (int i = 0; i < zone_ids.size(); i++) {
		visited.at(zone_ids.at(i)).id = zone_ids.at(i);
	}

	//If region only one vertex, visit the source vertex since one Zone under a region is still a connected Region.
	//Else, don't visit the source vertex immediately. 
	bool lone = false;
	for (int i = 0; i < visited.size(); i++) {
		if (visited.at(i).id != -1) {
			if (this->regionZoneList.at(region_id).size() <= 1)
				lone = true;
			this->RegionalDFSHelper(region_id, visited.at(i).id, visited, lone);
			lone = false;
		}
	}
	vector<bool> visitedVertex;
	visitedVertex.reserve(zone_ids.size());

	//If the region of n vertex is connected, vector of n bool should all be true.
	for (int i = 0; i < zone_ids.size();i++) {
		visitedVertex.push_back(visited.at(zone_ids.at(i)).visited);
	}
	return visitedVertex;
}


/*
*	Recursive helper method to perform a DFS on the map
*/
void Map::DFSHelper(int currIndex, vector<duo> &visited, bool visit) {
	if (visit == true) {
		visited.at(currIndex).visited = true;
		//cout << "visiting Zone id " << currIndex << endl;
		
	}
	for (int i = 0; i < this->adjacencyZoneList.at(currIndex).size(); i++) {
		int next_index = this->adjacencyZoneList.at(currIndex).at(i);
		if(visited.at(next_index).visited == false)
			this->DFSHelper(next_index, visited, true);
	}
}

/*
*	Recursive helper method to perform a regional DFS on the map
*/
void Map::RegionalDFSHelper(int region_id, int currIndex, vector<duo> &visited, bool visit) {
	if (visit == true) {
		if (visited.at(currIndex).visited == false) {
			visited.at(currIndex).visited = true;
			//cout << "visiting Zone id " << currIndex << endl;
		}
		
	}
	for (int i = 0; i < this->adjacencyZoneList.at(currIndex).size(); i++) {
		int next_index = this->adjacencyZoneList.at(currIndex).at(i);
		if (visited.at(next_index).visited == false && this->isZonePartOfRegion(next_index, region_id))
			this->RegionalDFSHelper(region_id, next_index, visited, true);
	}
}

/*
* Cleans the adjancency and regional zone id vectors
*/
void Map::resetLists()
{
	//Initializing the inner vectors
	for (int i = 0; i < this->zone_capacity; i++) {
		this->adjacencyZoneList.at(i).clear();
		this->adjacencyZoneList.at(i).reserve(this->zone_capacity);

	}

	for (int i = 0; i < this->region_capacity; i++) {
		this->regionZoneList.at(i).clear();
		this->regionZoneList.at(i).reserve(this->zone_capacity);
	}
}

/**
*	Checks the existance of a zone id
*	@return true if zone exists
*/
bool Map::zoneExist(int id)
{
	for (int i = 0; i < this->zones->size(); i++ ) {
		if (this->zones->at(i)->getId() == id)
			return true;
	}
	return false;
}

/*
*	Checks the existance of a region id
*	@return true if region exists
*/
bool Map::regionExist(int id)
{
	for (int i = 0; i < this->regions->size(); i++) {
		if (this->regions->at(i)->getId() == id)
			return true;
	}
	return false;
}

bool Map::isZonePartOfRegion(int zone_id, int region_id) {
	vector<int> region_zones = this->getZoneIdsByRegion(region_id);
	if (region_zones.size() == 0) {
		return false;
	}

	for (int i = 0; i < region_zones.size(); i++) {
		if (zone_id == region_zones.at(i))
			return true;
	}

	return false;
}