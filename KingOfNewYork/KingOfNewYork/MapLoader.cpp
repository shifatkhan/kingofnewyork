#include "MapLoader.h"
#include<sstream>
#include<iostream>
#include<fstream>
using namespace std;

/*
*	Implementation of the MapLoader class. Involved with loading and saving Map
*	
*	@author Thai-Vu Nguyen
*	@version 1.0 10/14/2018
*/


MapLoader::MapLoader()
{
	this->mode = LoadMode::DEFAULT;
	this->filepath = this->defaultPath;
}

/*
* MapLoader constructor
*/
MapLoader::MapLoader(LoadMode mode, std::string path)
{
	this->filepath = path;
	this->mode = mode;
}

MapLoader::~MapLoader()
{
	//delete map;
	//map = NULL;
}

/*
*	Requires this function to be called to load the map
*	If LoadMode is set to New, an empty map is loaded.
*	@exception InvalidMapDataException when file contains bad data
*/
void MapLoader::activateMap()
{
	if (this->mode == LoadMode::NEW) {
		this->map = new Map();
		map->setMapName(this->getBaseName(this->filepath,false));
	}
	else if (this->mode == LoadMode::DEFAULT) {
		this->map = loadMap(this->defaultPath);
	}
	else {
		this->map = loadMap(this->filepath);
	}
}

/*
*	Saves the Map data into a map file.
*/
void MapLoader::saveMap()
{
	if (this->map == NULL || this->map == nullptr)
		throw MapExceptions::MapNotLoadedException();

	ofstream file(this->filepath);
	if (file.is_open()) {
		//Save the regions
		file << "[Regions]" << endl;
		for (int i = 0; i < this->map->getRegionSize(); i++) {
			Region* region = this->map->getRegions()->at(i);
			file << region->getId() << "," << region->getRegionName() << "," << region->getVpBonus() << "," << region->getEnergyBonus() << endl;
		}
		file << endl;
		//Save the zones
		file << "[Zones]" << endl;
		for (int j = 0; j < this->map->getZoneSize(); j++) {
			Zone* zone = this->map->getZones()->at(j);
			file << zone->getId() << "," << zone->getRegionId() << "," << zone->getType();
			for (int adj : map->getAdjZoneIDs(zone->getId())) {
				file << "," << adj;
			}
			file << endl;
		}
	}

	file.close();
}

/*
* Returns all the lines of a file as a vector of string
*/
vector<string> MapLoader::getLines(string path) {
	
	vector<string> lines = vector<string>();
	
	ifstream filestream;
	filestream.open(path);
	string data;
	while (std::getline(filestream, data)) {
		if (data.length() >= 0)
			lines.push_back(data);
	}
	filestream.close();

	return lines;
	
}

/*
*	Checks that the RAW data for a single Region is valid
*/
bool MapLoader::validateRegionData(std::vector<std::string> splitData)
{
	// Expected Region data: int id, string name, int vp_bonus, int energy_bonus
	if (splitData.size() < 2 || splitData.size() > 4) {
		return false;
	}

	for (int i = 0; i < splitData.size(); i++) {
		//Check for empty values
		if (splitData.at(i).empty())
			return false;
		if (i == 0 || i == 2 || i == 3) {
			if (this->isUnsignedInteger(splitData.at(i)) == false)
				return false;
		}
		else if (i == 1) {
			if (splitData.at(i).empty())
				return false;
		}
		
	}
	return true;
}

/*
*	Checks that the RAW data for a signle Zone is valid
*/
bool MapLoader::validateZoneData(std::vector<std::string> splitData)
{
	if (splitData.size() < 3) {
		return false;
	}
	for (int i = 0; i < splitData.size(); i++) {
		if (splitData.at(i).empty())
			return false;
		if (i != 2) {
			if (this->isUnsignedInteger(splitData.at(i)) == false)
				return false;
		}
		else {
			if (this->isUnsignedInteger(splitData.at(i)))
				return false;
		}
	}
	return true;
}


/*
* Split the row of data into a vector
*/
vector<string> MapLoader::splitedData(string row) {
	vector<string> splited;
	string element;
	std::stringstream st(row);
	while (std::getline(st, element, ',')) {
		splited.push_back(element);
	}
	return splited;
}

/*
* Loads and create a Map object from a Map file
*/
Map * MapLoader::loadMap(std::string path)
{
	vector<std::string> lines = this->getLines(path);
	vector<string> regionLines;
	regionLines.reserve(lines.size());
	vector<string> zoneLines;
	zoneLines.reserve(lines.size());
	ParserMode mode = ParserMode::FETCHMODE;
	for (std::string line : lines) {
		if (!line.empty()) {
			
			//Searching for the [Zones] or [Regions] indicator
			if (line.find("[Zones]") != string::npos) {
				mode = ParserMode::FETCH_ZONE;
			}
			else if (line.find("[Regions]") != string::npos) {
				mode = ParserMode::FETCH_REGION;
			}
			else {
				if (mode == ParserMode::FETCH_REGION) {
					regionLines.push_back(line);
				}
				else if (mode == ParserMode::FETCH_ZONE) {
					zoneLines.push_back(line);
				}
			}
			
		}
		
	}

	vector<Region*> regions = this->createRegions(regionLines);
	vector<Zone*> zones = this->createZones(zoneLines);

	return createMap(zones, regions);
}

/*
*	Check if the string is an unsigned integer
*/
bool MapLoader::isUnsignedInteger(std::string value)
{
	for (int i = 0; i < value.length(); i++) {
		if (!isdigit(value.at(i)))
			return false;
	}

	return true;
}

/*
* Returns the basename of a file from its path
*/
std::string MapLoader::getBaseName(string path, bool withExtension, char delimiter)
{
	std::size_t del_p = path.find_last_of(delimiter);
	int length = path.length();
	int baseLength = length - del_p - 1;

	if (del_p != string::npos) {
		string base = path.substr(del_p + 1, baseLength);
		if (withExtension == false)
		{
			size_t dot_p = base.find_last_of('.');
			if (dot_p != string::npos)
				return base.substr(0, dot_p);
		}
		else {
			return base;
		}

	}
	return "";

}


//Accessor
Map * MapLoader::getMap()
{
	return this->map;
}

//Mutators
void MapLoader::setFilePath(std::string filepath)
{
	this->filepath = filepath;
}

void MapLoader::setLoadMode(LoadMode mode)
{
	this->mode = mode;
}

/*
* Adds a region and connects the edges
*/
void MapLoader::addRegion(Region * region)
{
	if (this->map == NULL || this->map == nullptr)
		throw MapExceptions::MapNotLoadedException();

	this->map->addRegion(region);
	this->map->connectTheEdges();
}

void MapLoader::addRegion(string name)
{

	this->addRegion(name, 0, 0);
}

void MapLoader::addRegion(std::string name, int vp, int energy)
{
	if (this->map == NULL || this->map == nullptr)
		throw MapExceptions::MapNotLoadedException();

	if (name.empty())
		throw MapExceptions::InvalidMapDataException();

	int highest_id = 0;;
	for (int i = 0; i < this->map->getRegionSize(); i++) {
		int temp_id = this->map->getRegions()->at(i)->getId();
		if (i == 0)
			highest_id = temp_id;
		else {
			if (highest_id < temp_id)
				highest_id = temp_id;
		}
	}
	if (map->getRegionSize() > 0)
		highest_id++;
	Region* region = new Region(highest_id, name, vp, energy);
	this->addRegion(region);
}

/*
* Adds a Zone and connects the edges
*/
void MapLoader::addZone(Zone * zone)
{
	if (this->map == NULL || this->map == nullptr)
		throw MapExceptions::MapNotLoadedException();

	this->map->addZone(zone);
	this->map->connectTheEdges();
}

void MapLoader::addZone(int region_id, vector<int> adj)
{
	if (this->map == NULL || this->map == nullptr)
		throw MapExceptions::MapNotLoadedException();

	int highest_id = 0;
	for (int i = 0; i < this->map->getZoneSize(); i++) {
		int temp_id = this->map->getZones()->at(i)->getId();
		if (i == 0) {
			highest_id = temp_id;
		}
		else {
			if (highest_id < temp_id)
				highest_id = temp_id;
		}
	}
	if(map->getZoneSize() >0)
		highest_id++;

	Zone* zone;
	if (adj.size() != 0)
		zone = new Zone(highest_id, region_id, adj);
	else
		zone = new Zone(highest_id, region_id);
	

	this->addZone(zone);
	
}

/*
* convert the raw data into a Zone
* Use this function after validation
*/
Zone * MapLoader::createZone(vector<std::string> dataRow)
{
	int id;
	int region_id;
	vector<int> adj;
	string type;
	adj.reserve(dataRow.size());

	for (int i = 0; i < dataRow.size();i++) {
		if (i == 0)
			id = stoi(dataRow.at(i));
		else if (i == 1)
			region_id = stoi(dataRow.at(i));
		else if (i == 2)
			type = dataRow.at(i);
		else
			adj.push_back(stoi(dataRow.at(i)));
	}

	Zone* zone = new Zone(id, region_id, adj);
	zone->setType(type);
	return zone;
}

/*
*	Generates a vector of Zones based on the raw data
*	
*	@exception InvalidMapDataException if any row of data are invalid
*/
vector<Zone*> MapLoader::createZones(vector<string> data)
{
	vector<Zone*> zones;
	zones.reserve(data.size());
	for (string row : data) {
		vector<string> split = this->splitedData(row);
		if (this->validateZoneData(split) == false)
			throw MapExceptions::InvalidMapDataException();
		zones.push_back(this->createZone(split));
	}

	return zones;

}

/**
*	Convert raw data into a Region instance
*	Use after validation
*/
Region * MapLoader::createRegion(std::vector<std::string> dataRow)
{
	int id;
	string name;
	int vp;
	int ep;

	for (int i = 0; i < dataRow.size(); i++) {
		if (i == 0)
			id = stoi(dataRow.at(i));
		else if (i == 1)
			name = dataRow.at(i);
		else if (i == 2)
			vp = stoi(dataRow.at(i));
		else if (i == 3)
			ep = stoi(dataRow.at(i));
	}

	return new Region(id, name, vp, ep);
}

/*
*	Generates a vector of Regions based on the raw data
*
*	@exception InvalidMapDataException if any row of data are invalid
*/
vector<Region*> MapLoader::createRegions(std::vector<std::string> data)
{
	vector<Region*> regions = vector<Region*>();
	regions.reserve(data.size());
	for (string row : data) {
		vector<string> split = this->splitedData(row);
		if (this->validateRegionData(split) == false)
			throw MapExceptions::InvalidMapDataException();
		regions.push_back(this->createRegion(split));
	}
	return regions;
}

/*
*	Generates a Map from the regions and zones
*/
Map * MapLoader::createMap(vector<Zone*> zones, vector<Region*> regions)
{
	string base = this->getBaseName(this->filepath, false);
	Map* map = new Map(base, zones.size() * 2, regions.size()* 2);
	for (Region* region : regions) {
		map->addRegion(region);
	}

	for (Zone* zone : zones) {
		map->addZone(zone);
	}
	map->connectTheEdges();

	return map;
}

