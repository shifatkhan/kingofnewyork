#include "Reward.h"
/*
Data Representation of a King of New York reward for destroying a Building/Unit Tile.
@author Hau Gilles Che
@version 1.0 10/10/2018
*/
Reward::Reward()
{
	this->rewardCount = 0;
}

Reward::Reward(Symbol rewardSymbol, int rewardCount)
{
	this->rewardCount = rewardCount;
	this->rewardSymbol = rewardSymbol;
}
/*
@return the amount of rewards.
*/
int Reward::getRewardCount() { return this->rewardCount; }

/*
@return Reward symbol.
*/
Symbol Reward::getRewardSymbol() { return this->rewardSymbol; }

/*
@param rewardCount value to set the rewards amount to.
*/
void Reward::setRewardCount(int rewardCount) { this->rewardCount = rewardCount; }

/*
@param rewardSymbol value to set the Reward symbol as.
*/
void Reward::setRewardSymbol(Symbol rewardSymbol) { this->rewardSymbol = rewardSymbol; }