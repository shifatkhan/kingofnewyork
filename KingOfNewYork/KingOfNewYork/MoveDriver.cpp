#include "MoveDriver.h"
#include "MapUtil.h"
#include "IOHelper.h"

/*
*	Implemention for the Driver of the move logic
*	Handles IO, gives options to move for the Player.
*	@author Thai-Vu Nguyen 26325440
*/


/*
*	Handles the loop for the Player's movement
*	Requires a handle of the player, map and the number of players
*/
void MoveDriver::moveLoop(Player * player, Map* map, int num_of_players, MainGameLoop::MainGameLoop* mainGameLoop)
{
	MoveManager manager(map);
	vector<MoveOption> options;
	options.reserve(6);
	
	cout << "You, " << player->getName() << ", are a " << player->getMonsterCard().getName() << " and you live ";
	if (player->getZone() != NULL)
		cout << "in Zone " << player->getZone()->getId() << " of " << MapUtil::zoneToRegion(player->getZone(), map)->getRegionName() << endl;
	else
		cout << "nowhere\n";
	//Get possible Actions
	options = manager.possibleMoves(player, num_of_players);
	if (options.size() == 0)
		options.push_back(MoveOption::DoNothing);
	//Choose potential option
	displayOptions(options);

	if (mainGameLoop != NULL) {
		// Display the option of accessing the Stats menu if the game started.
		cout << "[-1] See Game statistics." << endl;
	}

	string message = "Please choose your action (Type between the values inside the squared brackets): ";
	int input = player->setUpInput(message, "Wrong input, Try again\n", -1, options.size());
	input--;

	// Loop added to access the Game's stat menu.
	// Keep looping until user chose a MOVE option.
	while (input == -2 || input == -1) {
		// -1 is when a user inputs "0", which shouldn't do anything. Only reason it is here is because IOHelper takes in a range as input.
		if (input == -2 && mainGameLoop != NULL) {
			// Access the Game's statistics menu.
			mainGameLoop->setGameState(MainGameLoop::STATS_MENU);
		}
		else {
			cout << "Wrong input, Try again\n";
		}

		// After accessing the Stats menu, show options to user again.
		displayOptions(options);
		
		if (mainGameLoop != NULL) 
			cout << "[-1] See Game statistics." << endl;
		input = player->setUpInput(message, "Wrong input, Try again\n", -1, options.size());
		input--;
	}

	//Process input
	processInput(player, map, num_of_players, options.at(input));
}

/**
*	The loop for the Player's exit out of Manhattan
*	Gives the Player the choice between leaving or staying in Manhattan
*/
void MoveDriver::moveOutLoop(Player * player, Map * map, int num_of_players)
{
	
	vector<MoveOption> options = {MoveOption::DoNothing, MoveOption::EnterNonManhattan};
	options.reserve(6);

	cout << "You, " << player->getName() << ", are a " << player->getMonsterCard().getName() << " and you have taken taken damage ";
	if (player->getZone() != NULL)
		cout << "inside Zone " << player->getZone()->getId() << " of " << MapUtil::zoneToRegion(player->getZone(), map)->getRegionName() << endl;
	displayOptions(options);
	string message = "Please choose your action (Type between the values inside the squared brackets): ";
	int input = IOHelper::getValidIntOutput(message, "Wrong input, Try again\n", 1, options.size());
	input--;

	//Process input
	processInput(player, map, num_of_players, options.at(input));

}

/**
*	Print function
*/
void MoveDriver::displayOptions(vector<MoveOption> options)
{
	cout << "\nHere are your Move options\n";
	for (int i = 0; i < options.size(); i++) {
		cout << "[" << i + 1 << "] " << convertOptionToStringMessage(options.at(i)) << endl;
	}
}

/*
*	Translate the move enum into some readable text
*/
string MoveDriver::convertOptionToStringMessage(MoveOption option)
{
	string moveMessage = "";
	switch (option)
	{
	case MoveOption::DoNothing:
		moveMessage = "Stay in your burrough";
		break;
	case MoveOption::EnterManhattan:
		moveMessage = "Go to Manhattan";
		break;
	case MoveOption::EnterNonManhattan:
		moveMessage = "Enter a burrough outside Manhattan";
		break;
	case MoveOption::MoveUpManhattan:
		moveMessage = "Move up Manhattan";
		break;
	default:
		break;
	}
	return moveMessage;
}

/*
*	Handles the Option chosen by the Player
*	Will trigger an action to move based on the option taken
*/
void MoveDriver::processInput(Player * player, Map * map, int num_of_players, MoveOption option)
{
	switch (option) {
	case DoNothing:
		cout << "You chose to stay here\n";
		break;
	case EnterManhattan:
		cout << "You chose to enter Manhattan\n";
		enterManhattan(player, map, num_of_players);
		break;
	case EnterNonManhattan:
		cout << "You chose to enter a burrough outside of Manhattan\n";
		enterBurroughOutsideManhattan(player, map, num_of_players);
		break;
	case MoveUpManhattan:
		cout << "You chose to move up manhattan\n";
		getUpManhattan(player, map, num_of_players);
		break;
	default:
		break;
	}
}

/*
*	Handles the movement to the entry point of Manhattan
*/
void MoveDriver::enterManhattan(Player * player, Map * map, int num_of_players)
{
	vector<Zone*> zones = MapUtil::getEntryToManhattan(map);
	if (zones.size() == 0) {
		cout << "No Zones to enter, stay here\n";
		return;
	}

	cout << "\nHere are the Zones you may enter \n";
	for (int i = 0; i < zones.size(); i++) {
		cout << "["<< i + 1 << "] "<<"Zone " << zones.at(i)->getId() << " of Region " 
			<< zones.at(i)->getType() << " " << MapUtil::zoneToRegion(zones.at(i), map)->getRegionName() << endl;
	}
	int input = IOHelper::getValidIntOutput("Choose a Zone based on the value inside the bracket: ", "Wrong input, Try again\n", 1, zones.size());
	input--;
	Zone* zone = zones.at(input);
	player->move(zone);
	int vp =player->getVP();
	player->setVP(vp + 1);
	cout << "\nYou get to earn one Victory point for entering Manhattan. Your VP: " << player->getVP();
	cout << endl;
	map->printMap();
	cout << endl;
}

/*
*	Handles the movement of Player to move up Manhattan
*/
void MoveDriver::getUpManhattan(Player * player, Map * map, int num_of_players)
{
	vector<Zone*> zones = MapUtil::getNextManhattanZone(map, player->getZone());
	if (zones.size() == 0) {
		cout << "No Zones to enter, stay here\n";
		return;
	}
	cout << "\nHere are the Zones you may enter \n";
	for (int i = 0; i < zones.size(); i++) {
		cout << "[" << i + 1 << "] Zone " << zones.at(i)->getId() << " of Region "
			<< zones.at(i)->getType() << " " << MapUtil::zoneToRegion(zones.at(i), map)->getRegionName() << endl;
	}

	int input = IOHelper::getValidIntOutput("Choose a Zone based on the value inside the bracket: ", "Wrong input, Try again\n", 1, zones.size());
	input--;
	Zone* zone = zones.at(input);
	player->move(zone);
	cout << endl;
	map->printMap();
	cout << endl;
}

/*
*	Handles the movement of Player to move to an available burrough outside Manhattan
*/
void MoveDriver::enterBurroughOutsideManhattan(Player * player, Map * map, int num_of_players)
{
	vector<Zone*> zones = MapUtil::getTakableZonesOutisdeManhattan(map);
	if (zones.size() == 0) {
		cout << "No Zones to enter, stay here\n";
		return;
	}
	cout << "\nHere are the Zones you may enter \n";
	for (int i = 0; i < zones.size(); i++) {
		cout << "[" << i + 1 << "] Zone " << zones.at(i)->getId() << " of Region "
			<< MapUtil::zoneToRegion(zones.at(i), map)->getRegionName() << endl;
	}

	int input = player->setUpInput("Choose a Zone based on the value inside the bracket: ", "Wrong input, Try again\n", 1, zones.size());
	input--;
	Zone* zone = zones.at(input);
	player->move(zone);
	cout << endl;
	map->printMap();
	cout << endl;
}

