#include <string>
#include "Region.h"

using namespace std;

/*
*	Implementation of the Region bean container
*	@author Thai-Vu Nguyen
*	@version 10/14/2018
*/

//Constructors

Region::Region() {
	this->id = 0;
	this->region_name = "";
	this->energy_bonus = 0;
	this->vp_bonus = 0;
	this->zones = vector<Zone*>();
	this->tileStacks = vector<TileStack>();
	this->tileStacks.reserve(3);
}

Region::Region(int id, string region_name) {
	this->id = id;
	this->region_name = region_name;
	this->energy_bonus = 0;
	this->vp_bonus = 0;
	this->zones = vector<Zone*>();
	this->tileStacks = vector<TileStack>();
	this->tileStacks.reserve(3);
}

Region::Region(int id, string region_name, int vp_bonus, int energy_bonus) {
	this->id = id;
	this->region_name = region_name;
	this->energy_bonus = energy_bonus;
	this->vp_bonus = vp_bonus;
	this->zones = vector<Zone*>();
	this->tileStacks = vector<TileStack>();
	this->tileStacks.reserve(3);
}

//Destructor
Region::~Region() {
	for (Zone* zone : zones) {
		delete zone;
	}
	zones.clear();
}

//Accessors

int Region::getId() { return this->id; }
string Region::getRegionName() { return this->region_name; }
int Region::getEnergyBonus() { return this->energy_bonus; }
int Region::getVpBonus() { return this->vp_bonus; }
vector<Zone*> Region::getZones(){ return this->zones; }
vector<TileStack> Region::getTileStacks() { return this->tileStacks; }
vector<Tile> Region::getUnits() { return this->units; }
//Mutators

void Region::setId(int id) { this->id = id; }
void Region::setRegionName(string region_name) { this->region_name = region_name; }
void Region::setEnergyBonus(int energy_bonus) { this->energy_bonus = energy_bonus; }
void Region::setVpBonus(int vp_bonus) { this->vp_bonus = vp_bonus; }
void Region::setZones(vector<Zone*> zones) { this->zones = zones; }
void Region::setTileStack(vector<TileStack> tileStacks) { this->tileStacks = tileStacks; }
void Region::setUnits(vector<Tile> units) { this->units = units; }

/*
Adds a unit into the region.
@param unit The unit to be added.
*/
void Region::addUnit(Tile unit)
{
	this->units.push_back(unit);
}

int Region::getNumOfUnits() { return units.size(); }

void Region::displayTiles() {
	cout << "Buildings in " << this->getRegionName() << ":" << endl;
	if (this->tileStacks.empty())
	{
		cout << "\tNo Building present in this borough." << endl;
	}
	else
	{
		for (int i = 0; i < this->tileStacks.size(); i++)
		{
			if (tileStacks.at(i).getTiles().size() > 0)
			{
				cout << "\t[" << i << "] " << tileStacks[i].getTopTile() << endl;
			}
		}
	}
	
	cout << "Units in " << this->getRegionName() << ":" << endl;
	if (this->units.empty())
	{
		cout << "\tNo Unit present in this borough." << endl;
	}
	else
	{
		for (int i = 0; i < this->units.size(); i++)
		{
			cout << "\t[" << i << "] " << this->units[i] << endl;
		}
	}
	
}