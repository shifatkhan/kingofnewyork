#ifndef MAPLOADERDRIVER_H
#define MAPLOADERDRIVER_H

namespace MapLoaderDriverSpace {

	void TestEmptyMapAndSave();
	void TestExistingMapEditAndSave();
	void TestBadDataMaps();
	void TestNoMapLoadedExceptions();

	void runTests();
}

#endif // !MAPLOADERDRIVER_H
