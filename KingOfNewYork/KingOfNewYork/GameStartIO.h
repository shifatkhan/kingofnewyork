#ifndef GAMESTARTIO_H
#define GAMESTARTIO_H

#include <stdio.h>
#include "Deck.h"
#include "MapLoader.h"
#include "MapUtil.h"

/**
*	Handles the Game Start of the Game
*	Loads the pieces of the Game
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 11/4/2018
*/

class GameStartIO {
private:
	MapLoader mloader;
	vector<Player*> players;
	Deck deck;
	Zone* entryToManhattan;


public:
	GameStartIO();
	void start();
	MapLoader getMapLoader();
	Map* getMap();
	vector<Player*> getPlayers();
	Deck getDeck();
	void displayAll();

private:
	void loadMap();
	string selectPath();
	int selectPlayerNumbers();
	void listFilesInDir(const char *subDir, vector<string> &files);
	bool isPathValid(string path);
	void loadDeck();
	Player* createPlayer(int playerIndex);
	void createPlayers(int num);
	void displayMonsterCards(vector<Card> monsterCards);
	vector<TileStack> loadStacks();

	PlayerMode choosePlayerMode();
	void setPlayerStrategy(Player* player, PlayerMode mode);
	void generateTitleScreen();
};

#endif // !GAMESTARTIO_H
