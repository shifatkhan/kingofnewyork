#include "MapUtil.h"

/**
*	MapUtil file containing a set of utility functions concerning the Map
*	@author Thai-Vu Nguyen 26325440
*	version 1.0 10/4/2018
*/

//Converts string to Uppercase
string toUpperCase(string val) {
	transform(val.begin(), val.end(), val.begin(), toupper);
	return val;
}


//Validation to check if Map contains a region with Manhattan on its name
bool MapUtil::containsManhattan(Map* map) {
	vector<Region*>* regions = map->getRegions();
	
	//Map should contain 1 manhattan regions and one at least non-manhattan
	if (regions->size() < 2) {
		return false;
	}
	string manhattan = "Manhattan";


		bool found = false;
		for (int i = 0; i < regions->size() && found == false; i++) {
			string region_name = regions->at(i)->getRegionName();
			transform(region_name.begin(), region_name.end(), region_name.begin(), toupper);
			transform(manhattan.begin(), manhattan.end(), manhattan.begin(), toupper);
			if (manhattan == region_name) {
				found = true;
			}
		}
		if (found == false)
			return false;
	
	
	return true;
}

//Validates the Zones of Manhattan
//Assumes Manhattan region has already been validated
bool MapUtil::validatesManhattanZone(Map * map)
{
	vector<Zone*> lower = getLowerManhattanZones(map);
	vector<Zone*> mid = getMidManhattanZones(map);
	vector<Zone*> upper = getUpperManhattanZones(map);

	//Checking the size of each Manhattan sub-parts
	if (lower.size() != 2)
		return false;
	if (mid.size() < 2)
		return false;
	if (upper.size() < 2)
		return false;

	//Checking if all entry points of Manhattan links to all Zones in Mid Manhattan
	for (int i = 0; i < lower.size(); i++)
	{
		bool connected = false;
		
		for (int j = 0; j < mid.size(); j++) {
			connected = map->isZonesConnected(lower.at(i)->getId(), mid.at(j)->getId());
		}
		if (connected == false)
			return false;
	}

	//Checking if all zones inside Mid manhattan links to all Zones in Upper Manhattan
	for (int i = 0; i < mid.size(); i++)
	{
		bool connected = false;

		for (int j = 0; j < upper.size(); j++) {
			connected = map->isZonesConnected(mid.at(i)->getId(), upper.at(j)->getId());
		}
		if (connected == false)
			return false;
	}

	return true;
}

//Check if the Region is Manhattan
bool MapUtil::isManhattan(Region* region) {
	string name = toUpperCase( region->getRegionName());
	string manhattan = toUpperCase ("Manhattan");
	return name == manhattan;
}

//Checks if the Zone is part of Manhattan
bool MapUtil::isPartOfManhattan(Zone* zone, Map* map) {
	Region* region = map->getRegionByZone(zone);
	return MapUtil::isManhattan(region);
}

//Returns true if Manhattan is available to be taken
bool MapUtil::isManhattanAvailable(Map* map, int num_of_players)
{
	int max = 1;
	if (num_of_players >= 5)
		max = 2;
	return getAllPlayersInManhattan(map).size() < max;
}

//Check if the Burrough is takable
bool MapUtil::isRegionTakable(Region * region, int max)
{
	if (getAllPlayersInsideRegion(region).size() < max)
		return true;
	return false;
}

//Returns all Zones outside of manhattan and inside Regions that are takeable
vector<Zone*> MapUtil::getTakableZonesOutisdeManhattan(Map * map)
{
	vector<Zone*> zones;
	zones.reserve(map->getZoneSize());
	vector<Region*> regions = getNonManhattanRegions(map);
	for (Region* region : regions) {
		if (isRegionTakable(region,2)) {
			for (int i = 0; i < region->getZones().size(); i++)
			{
				if (region->getZones().at(i)->isTaken() == false)
					zones.push_back(region->getZones().at(i));
			}
		}

	}

	return zones;
}

//Returns all the Players inside a region
vector<Player*> MapUtil::getAllPlayersInsideRegion(Region * region)
{
	vector<Player*> players;
	players.reserve(6);
	vector<Zone*> zones = region->getZones();
	for (int i = 0; i < zones.size(); i++)
	{
		if (zones.at(i)->isTaken() && zones.at(i)->getPlayer() != NULL)
			players.push_back(zones.at(i)->getPlayer());
	}
	return players;
}

//Returns all the Players inside Manhattan
vector<Player*> MapUtil::getAllPlayersInManhattan(Map* map) {
	vector<Zone*>* zones = map->getZones();
	vector<Player*> playersInManhattan;
	playersInManhattan.reserve(6);
	for (int i = 0; i < zones->size(); i++) {
		if (zones->at(i)->isTaken() && zones->at(i)->getPlayer() != NULL && isPartOfManhattan(zones->at(i), map)) {
			playersInManhattan.push_back(zones->at(i)->getPlayer());
		}
	}

	return playersInManhattan;

}

//Returns all the Players outside Manhattan
vector<Player*> MapUtil::getAllPlayersOutsideMahattan(Map* map) {
	vector<Zone*>* zones = map->getZones();
	vector<Player*> playersOutsideManhattan;
	playersOutsideManhattan.reserve(6);
	for (int i = 0; i < zones->size(); i++) {
		if (zones->at(i)->isTaken() && zones->at(i)->getPlayer() != NULL && isPartOfManhattan(zones->at(i), map) == false) {
			playersOutsideManhattan.push_back(zones->at(i)->getPlayer());
		}
	}
	return playersOutsideManhattan;
}

//Returns all the Burroughs outside Manhattan
vector<Region*> MapUtil::getNonManhattanRegions(Map * map)
{
	vector<Region*> regions;
	regions.reserve(map->getRegions()->size());
	for (int i = 0; i < map->getRegionSize(); i++) {
		if (isManhattan(map->getRegions()->at(i)) == false)
			regions.push_back(map->getRegions()->at(i));
	}
	return regions;
}

//Get a handle on Manhattan
Region * MapUtil::getManhattan(Map * map)
{
	vector<Region*>* regions = map->getRegions();
	for (int i = 0; i < regions->size(); i++) {
		if (isManhattan(regions->at(i)))
			return regions->at(i);
	}
	return nullptr;
}

//Deals with connecting Zone to its region
Region * MapUtil::zoneToRegion(Zone * zone, Map * map)
{
	return map->getRegionByZone(zone);
}

//Deals with getting every zones inside manhattan
vector<Zone*> MapUtil::getAllManhattanZones(Map * map)
{
	Region* manhattan = getManhattan(map);

	if (manhattan != NULL && manhattan != nullptr)
		return manhattan->getZones();
	else
		return vector<Zone*>();
}

//Retrieve the available Zones upstair to the current level of Manhattan
vector<Zone*> MapUtil::getNextManhattanZone(Map * map, Zone * zone)
{
	if (isPartOfManhattan(zone, map) == false)
		return vector<Zone*>();
	vector<Zone*> next_zones;
	if (zone->getType() == "Upper")
		return vector<Zone*>();
	else if (zone->getType() == "Mid")
		next_zones = getUpperManhattanZones(map);
	else if (zone->getType() == "Lower")
		next_zones = getMidManhattanZones(map);
	vector<Zone*> filter;
	filter.reserve(next_zones.size());
	for (int i = 0; i < next_zones.size(); i++) {
		if (next_zones.at(i)->isTaken() == false)
			filter.push_back(next_zones.at(i));
	}
	return filter;
}

//Retrieve the available entry to Lower Manhattan
vector<Zone*> MapUtil::getEntryToManhattan(Map * map)
{
	vector<Zone*> lower = getLowerManhattanZones(map);
	vector<Zone*> filter;
	filter.reserve(lower.size());
	for (int i = 0; i < lower.size(); i++)
		if (lower.at(i)->isTaken() == false)
			filter.push_back(lower.at(i));
	return filter;
}

//Retrieve all Zones inside lower Manhattan
vector<Zone*> MapUtil::getLowerManhattanZones(Map * map)
{
	Region* manhattan = getManhattan(map);
	if (manhattan == NULL || manhattan == nullptr)
		return vector<Zone*>();
	return getManhattanZones(manhattan, "Lower");
}

//Retrieve all Zones inside Mid Manhattan
vector<Zone*> MapUtil::getMidManhattanZones(Map * map)
{
	Region* manhattan = getManhattan(map);
	if (manhattan == NULL || manhattan == nullptr)
		return vector<Zone*>();
	return getManhattanZones(manhattan, "Mid");
}

//Retrieve all Zones inside Upper Manhattan
vector<Zone*> MapUtil::getUpperManhattanZones(Map * map)
{
	Region* manhattan = getManhattan(map);
	if (manhattan == NULL || manhattan == nullptr)
		return vector<Zone*>();
	return getManhattanZones(manhattan, "Upper");
}

vector<Zone*> MapUtil::getManhattanZones(Region * manhattan, string type)
{
	vector<Zone*> zones = manhattan->getZones();
	vector<Zone*> zonesOfType;
	zonesOfType.reserve(zones.size());
	
	for (int i = 0; i < zones.size(); i++) {
		string comparison = toUpperCase(zones.at(i)->getType());
		type = toUpperCase(type);
		if (type == comparison)
			zonesOfType.push_back(zones.at(i));
	}
	return zonesOfType;
}

