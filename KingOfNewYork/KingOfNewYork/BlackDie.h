#ifndef BLACKDIE_H
#define BLACKDIE_H
#include <string>
#include <array>
#include <iostream>
#include "Symbol.h"
using namespace std;
/*
Data representation of a Black Die in the board game King of New York
@author Hau Gilles Che	
@version 1.0 10/07/2018
*/
class BlackDie{
public:
	//Constructor
	BlackDie();
	BlackDie(Symbol symbol,int seed, int totalRolls=0, bool isKept=false);
	//Destructor
	~BlackDie(){};

	//Accessors
	bool getIsKept();
	Symbol getSymbol();
	array<int, 6> getRollStats();
	int getTotalRolls();

	//Mutators
	void setIsKept(bool keep);
	void setSymbol(Symbol symbol);
	void setRollStats(array<int, 6> rollStats);
	void setTotalRolls(int totalRolls);

	void roll();
	string rollPercentages();

	friend std::ostream& operator <<(std::ostream& outs, const BlackDie& die);

private:
	//Private Members
	bool isKept;
	Symbol symbol;
	int seed;
	array<int,6> rollStats;
	int totalRolls;
	
	void incrementRolls();

	void generateValue();
	std::string generateSymbolString(Symbol symbol);

	void setSeed();
	Symbol validateSymbol(Symbol s);
};
#endif // !BLACKDIE_H