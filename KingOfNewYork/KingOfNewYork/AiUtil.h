#ifndef AIUTIL_H
#define AIUTIL_H

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <Windows.h>

namespace AiUtil {
	int random(int min, int max);
	void loadingEffect();
}

#endif // !AIUTIL_H
