#include "StartUpPhase.h"
#include "Symbol.h"
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include "MoveDriver.h"
namespace StartUpPhase 
{
	int determineFirstPlayer(vector<Player*> players)
	{
		array<BlackDie, 8> dice;
		int attackCtr = 0;
		int currAttackCtr = 0;
		int topAttackIndex = 0;

		for (int i = 0; i < players.size();i++)
		{
			std::cout<< players.at(i)->getName() << " is rolling the dice";
			for (int i = 0; i < 3; i++)
			{
				Sleep(500);
				std::cout << ".";
			}
			std::cout << std::endl;
			
			for (BlackDie d : dice)
			{
				d.roll();
				if (d.getSymbol() == ATTACK)
					currAttackCtr++;
				std::cout << d.getSymbol() << std::endl;
			}
			std::cout << "Player " << players.at(i)->getName() << " Rolled: " << currAttackCtr << " Attacks" << std::endl;

			if (currAttackCtr > attackCtr)
			{
				topAttackIndex = i;
				attackCtr = currAttackCtr;
			}
			currAttackCtr = 0;
		}

		std::cout << "Player " << players.at(topAttackIndex)->getName() << " wins. " << std::endl;
		return topAttackIndex;
	}

	vector<Player*> generatePlayOrder(vector<Player*> players, int indexOfFirstPlayer)
	{
		Player* firstP = players[indexOfFirstPlayer];

		players.erase((players.begin() + indexOfFirstPlayer));
		players.insert(players.begin(), firstP);
		return players;
	}

	void setMonstersInBorough(vector<Player*> players, Map* map)
	{
		int numOfPlayers = players.size();
		for (Player* p : players)
		{
			MoveDriver::moveLoop(p, map, numOfPlayers, NULL);
		}
	}
};