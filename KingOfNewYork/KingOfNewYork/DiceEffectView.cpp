#include "DiceEffectView.h"
#include "Symbol.h"
using namespace std;

DiceEffectView::DiceEffectView() {
	_diceRollingFacility = NULL;
};

DiceEffectView::DiceEffectView(DiceRollingFacility* diceRollingFacility) {
	_diceRollingFacility = diceRollingFacility;
	_diceRollingFacility->Attach(this);
}

DiceEffectView::~DiceEffectView() {
	_diceRollingFacility->Detach(this);
}

void DiceEffectView::setDiceRollingFacility(DiceRollingFacility* diceRollingFacility) {
	if (_diceRollingFacility != NULL) {
		_diceRollingFacility->Detach(this);
	}
	_diceRollingFacility = diceRollingFacility;
	_diceRollingFacility->Attach(this);
}

void DiceEffectView:: Update(int gameState) {
	display();
}

void DiceEffectView::display() {
	int roll = _diceRollingFacility->getRollCtr();
	if (roll == 0) {
		cout << "No die have been rolled." << endl;
	}
	else {
		cout << "You rolled your dice " << roll << " time(s)." << endl;
		cout << "You can reroll your dice " << 3 - roll << " time(s)." << endl;
		cout << "Current roll:" << endl;
		cout << *_diceRollingFacility << endl << endl;

		for (Symbol s : _diceRollingFacility->getUniqueSymbols()) {
			displayDiceEffects(s);
		}
	}
}

void DiceEffectView::displayDiceEffects(Symbol symbol) {
	cout << symbol;
	switch (symbol)
	{
	case ENERGY:
		cout << ": You earn 1 Energy cube for each Energy you roll." << endl;
		break;
	case ATTACK:
		cout << ": You inflict one damage for each Attack you roll." << endl;
		cout << "\t -If you are in Manhattan, each Attach you roll inflicts damage to all Monsters outside Manhattan." << endl;
		cout << "\t -If you are not in Manhattan, each Attack you roll inflicts damage to all Monsters in Manhattan." << endl;
		break;
	case DESTRUCTION:
		cout << ": You can use Destruction to destroy Buildings or eliminate Units." << endl;
		break;
	case HEAL:
		cout << ": Each Heal you rolled allows you to heal 1 Life Point that you've lost, unless you are in Manhattan." << endl;
		break;
	case CELEBRITY:
		cout << ": If you roll fewer than 3 Celebrity nothing happens." << endl;
		cout << "\tIf you roll triple Celebrity or more, you take the Superstar card.\n \tYou immediately earn 1 Victory Point + 1 Victory Point per Celebrity beyond the first three." << endl;
		break;
	case OUCH:
		cout << ": Units will attack. Each unit deals 1 damage. The spread of the attack varies depending on the amount of Ouch! rolled." << endl;
		cout << "\t1 Ouch!: The Units in your borough attack you." << endl;
		cout << "\t2 Ouch!: The Units in your borough attack all the Monsters in your borough (including you)." << endl;
		cout << "\t3 Ouch!: You trigger a counterattack by the entire army! All Units in the entire city attack." << endl;
		cout << "\t\tEach monster is attack by all Units in his borough." << endl;
		break;
	default:
		break;
	}
	cout << endl;
}