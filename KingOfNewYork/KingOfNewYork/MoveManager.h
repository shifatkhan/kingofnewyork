#ifndef MOVEMANAGER_H
#define MOVEMANAGER_H

#include "Map.h"

/**
*	Logic for the Move
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 11/4/2018
*/

enum MoveOption {
	EnterManhattan, EnterNonManhattan, DoNothing, MoveUpManhattan
};

class MoveManager {
private:
	Map* map;
public:
	MoveManager(Map* map);
	~MoveManager();
	vector<MoveOption> possibleMoves(Player* player, int num_of_players);

};

#endif // !MOVEMANAGER_H
