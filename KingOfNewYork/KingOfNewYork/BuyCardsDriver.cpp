/** Implementation file for BuyCardsDriver.h
	This is basically a game loop to buy cards.
	It takes a reference to the main game loop to change
	the game loop's state.

	@author Shifat Khan
	@version 1.0 11/03/2018
*/
#include "BuyCardsDriver.h"

namespace BuyCardsDriver {

	/**	Checks if the player has enough energy to begin with.
	*/
	void start(Player& player, Deck& d, MainGameLoop::MainGameLoop& mainGameLoop) {
		std::cout << player.getName() << " is buying cards" << std::endl;

		if (player.getEnergy() == 0) {
			std::cout << player.getName() << " has 0 Energy cubes. Can't buy any cards.";
		}
		else {
			playerTurn(player, d, mainGameLoop);
		}
	}

	/**	Gets the player's choice on whether he/she wants to discard, buy, or do nothing.
		Player can also decide to access the Game stats menu.
	*/
	void playerTurn(Player& player, Deck& d, MainGameLoop::MainGameLoop& mainGameLoop) {
		int choice = 1;
		string input;
		while (choice == -1 || choice == 1 || choice == 2) {
			d.displayPlayDecks();
			std::cout << "Energy cubes: " << player.getEnergy() << std::endl;
			std::cout << "What do you want to do?" << std::endl;
			std::cout << "\t[1]: Discard cards for 2 Energy.\n";
			std::cout << "\t[2]: Buy a card.\n";
			std::cout << "\t[3]: Do nothing.\n";
			std::cout << "\t[-1] See Game statistics.\n";

			// Loop for good input.
			while (true)
			{
				// Initialize choice to a bad input.
				choice = 9999;

				std::cout << "Enter the number of the choice: ";
				std::cin >> input;
				choice = atol(input.c_str());
				if (choice == -1 || (choice >= 1 && choice <= 3)) {
					break;
				}
				else {
					std::cout << choice << " is not a valid option!" << std::endl;
				}
			}
			std::cout << "\n";

			switch (choice)
			{
			case -1:
				// Access the Game's statistics menu.
				mainGameLoop.setGameState(MainGameLoop::STATS_MENU);
				break;

			case 1:
				// Discard the flipped cards to reveal three new ones.
				if (player.getEnergy() < 2) {
					std::cout << "Not enough Energy to discard!\n\n";
				}
				else {
					try {
						d.flipTopThreeCards();
						player.setEnergy(player.getEnergy() - 2);
					}
					catch (KingOfNewYorkExceptions::EmptyDeckException ede) {
						std::cout << "No more cards to get!" << std::endl;
					}
				}
				break;
				
			case 2:
				// Buy one of the flipped cards.
				buyCardsInput(player, d, mainGameLoop);
				break;
			default:
				std::cout << "Doing nothing.\n\n";
				break;
			}
		}
	}

	/**	This executes the action of buying a card.
	*/
	void buyCardsInput(Player& player, Deck& d, MainGameLoop::MainGameLoop& mainGameLoop) {
		// Display the top three cards.
		d.displayPlayDecks();

		std::cout << player.getName() << ": BUYING CARDS" << std::endl;
		std::cout << "Energy cubes: " << player.getEnergy() << std::endl;

		int cardNumber;
		string input;

		// Loop for good input.
		while (true)
		{
			std::cout << "[-1] See Game statistics.\n";
			std::cout << "Enter the number of the card you wish to buy (enter 9 to cancel): ";

			std::cin >> input;
			cardNumber = atol(input.c_str());
			if ((cardNumber >= 1 && cardNumber <= d.getFlippedThreeCards().size()) || cardNumber == 9 || cardNumber == -1) {
				break;
			}
			else {
				std::cout << cardNumber << " is not a valid option!\n" << std::endl;
				
				// Display the top three cards.
				d.displayPlayDecks();
			}
		}
		std::cout << "\n";

		// Card number can never go up to 9, since there can only be 3 cards flipped.
		// So 9 represents the "Do nothing" action.
		if (cardNumber != 9) {
			if (cardNumber == -1) {
				// Access the Game's statistics menu.
				mainGameLoop.setGameState(MainGameLoop::STATS_MENU);
			}
			else {
				// Buy card.
				try {
					Card buying = d.getFlippedCardAtIndex(cardNumber - 1);

					if (player.getEnergy() >= buying.getCost()) {
						buying = d.takeFlippedCardAtIndex(cardNumber - 1);
						player.buyCard(buying);
						std::cout << buying.getName() << " card bought!\n";
						player.displayPlayerStatus();
					}
					else {
						std::cout << "Not enough Energy to buy this card!\n\n";
					}
				}
				catch (KingOfNewYorkExceptions::EmptyDeckException ede) {
					std::cout << "No more cards to buy!" << std::endl;
				}
				catch (KingOfNewYorkExceptions::DeckIndexOutOfRangeException die) {
					std::cout << "Not a valid index: " << cardNumber << std::endl;
				}
			}
		}
	}

	/*
	*	Automated buy for the AI
	*/
	void startForAI(Player & player, Deck & d)
	{
		std::cout << player.getName() << " is buying cards" << std::endl;

		if (player.getEnergy() == 0) {
			std::cout << player.getName() << " has 0 Energy cubes. Can't buy any cards.\n";
		}
		else {
			d.displayDecks();
			AiUtil::loadingEffect();
			try {
				if (canBuySomething(player, d)) {
					//cout << "\nActually, never mind, " << player.getName() << " is a cheapstake and wants to keep his energy.\n";
					bool found = false;
					for(int i = 0; i < d.getFlippedThreeCards().size() && found == false; i++)
						if (player.getEnergy() >= d.getFlippedThreeCards().at(i).getCost())
						{
							found = true;
							Card buy = d.takeFlippedCardAtIndex(i);
							player.buyCard(buy);
							cout << player.getName() << " bought " << buy.getName() << endl;
						}
				}
				else {
					cout << "\n" << player.getName() << " cannot afford to buy any card";
				}

			}
			catch (KingOfNewYorkExceptions::EmptyDeckException ede) {
				std::cout << "No more cards to buy!" << std::endl;
			}
			
			
		}
	}

	bool canBuySomething(Player & player, Deck & d)
	{
		vector<Card> cards = d.getFlippedThreeCards();
		for (int i = 0; i < cards.size(); i++)
			if (player.getEnergy() >= cards.at(i).getCost())
				return true;
		return false;
	}
}