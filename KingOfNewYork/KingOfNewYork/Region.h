/**
*	Header file for the Region bean container
*/


#ifndef REGION_H
#define REGION_H

#include<string>
#include<vector>

#include "Zone.h"
#include "TileStack.h"

class Zone;

class Region {
private:
	int id;
	std::string region_name;
	int energy_bonus;
	int vp_bonus;
	std::vector<Zone*> zones;
	std::vector<TileStack> tileStacks;
	std::vector<Tile> units;
public:
	//Contructor
	Region();
	Region(int id, std::string name);
	Region(int id, std::string name, int vp_bonus, int energy_bonus);
	
	~Region();
	//Accessors
	int getId();
	std::string getRegionName();
	int getEnergyBonus();
	int getVpBonus();
	std::vector<Zone*> getZones();
	std::vector<TileStack> getTileStacks();
	std::vector<Tile> getUnits();
	//Mutators
	void setId(int id);
	void setRegionName(std::string region_name);
	void setEnergyBonus(int energy_bonus);
	void setVpBonus(int vp_bonus);
	void setZones(std::vector<Zone*> zones);
	void setTileStack(std::vector<TileStack> titleStacks);
	void setUnits(std::vector<Tile> units);

	//Extra
	void displayTiles();
	void addUnit(Tile unit);
	int getNumOfUnits();
};

#endif // !REGION_H