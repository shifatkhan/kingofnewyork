#include "MoveManager.h"
#include "MapUtil.h"

/*
*	Implementation for the back-end logic behind Move
*	@author Thai-Vu Nguyen
*	@version 1.0 11/4/2018
*/

MoveManager::MoveManager(Map * map)
{
	this->map = map;
}

MoveManager::~MoveManager()
{
	//Setting the map to NULL since Map needs to be destroyed at end of program
	this->map = NULL;
}

/*
*	Back-end logic behind the Player's move
*	Returns a list of possible options that a player can make regarding his movement on the map
*/
vector<MoveOption> MoveManager::possibleMoves(Player* player, int num_of_players) {
	vector<MoveOption> options;
	options.reserve(6);
	//If Player is not on the map, force him to enter a zone outside Manhattan
	if (player->getZone() == NULL || player->getZone() == nullptr)
	{
		options.push_back(MoveOption::EnterNonManhattan);
		return options;
	}

	//If Manhattan is empty, the player is forced to enter manhattan
	if (MapUtil::getAllPlayersInManhattan(map).size() == 0) {
		options.push_back(MoveOption::EnterManhattan);
		return options;
	}


	if (MapUtil::isPartOfManhattan(player->getZone(), map)) {
		//If Player is in manhattan, move up unless he's already on the Upper zone
		if (player->getZone()->getType() == "Upper") {
			options.push_back(MoveOption::DoNothing);
		}
		else {
			options.push_back(MoveOption::MoveUpManhattan);
		}
	}else {
		if (MapUtil::isManhattanAvailable(map, num_of_players))
			options.push_back(MoveOption::EnterManhattan);
		options.push_back(MoveOption::DoNothing);
		options.push_back(MoveOption::EnterNonManhattan);
	}
	return options;
}