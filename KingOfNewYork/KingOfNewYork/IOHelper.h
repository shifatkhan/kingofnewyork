#ifndef IOHelper
#include <string>

/**
*	Helper functions for handling IO input
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 11/4/2018
*/

namespace IOHelper {
	int getValidIntOutput(std::string message, std::string error, int min, int max);
};

#endif // !IOHelper
