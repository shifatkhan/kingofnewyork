#include <string>
#include "BlackDie.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "KnyExceptions.h"
using namespace std;
/**
@author Hau Gilles Che
@version 1.0 10/07/2018
*/
BlackDie::BlackDie()
{
	this->isKept = false;
	this->seed = ((int)time(0))*rand();
	this->totalRolls = 0;
	this->rollStats.fill(0);
}

/*
@param symbolVal int value associated with a symbol
@param isKept whether the die should be kept or not. If kept, the die will not be rerolled
*/
BlackDie::BlackDie(Symbol symbol,int seed, int totalRolls, bool isKept)
{
	this->isKept = isKept;
	this->symbol = validateSymbol(symbol);
	this->totalRolls = totalRolls;
	this->seed = seed;
}

bool BlackDie::getIsKept(){ return this->isKept; }
Symbol BlackDie::getSymbol() { return this->symbol; }
array<int,6> BlackDie::getRollStats() { return this->rollStats; }
int BlackDie::getTotalRolls() { return this->totalRolls; }

void BlackDie::setIsKept(bool keep){ this->isKept = keep; }

void BlackDie::setSymbol(Symbol symbol)
{ 
	this->symbol = validateSymbol(symbol); 
}

void BlackDie::setRollStats(array<int, 6> rollStats)
{
	this->rollStats = rollStats;
}

void BlackDie::setTotalRolls(int totalRolls)
{
	this->totalRolls = totalRolls;
}

/*
Generates value of a symbol
*/
void BlackDie::generateValue()
{
	this->setSeed();
	srand(this->seed);
	Symbol s = static_cast<Symbol>((rand() % 6) + 1);
	this->symbol = validateSymbol(s);
}

/*
Increments the rolls counters used for roll stats
*/
void BlackDie::incrementRolls()
{
	int symbolIndex = this->symbol - 1;
	this->totalRolls = this->totalRolls + 1;
	//decrement index value since symbols are in range 1-6 and array is in range 0-5
	this->rollStats[symbolIndex] = this->rollStats[symbolIndex]++;
}

/*
Generate a new die roll.
@return symbol that has been rolled.
*/
void BlackDie::roll(){
	if (!this->isKept)
	{
		this->generateValue();
		this->incrementRolls();
	}
}

/*
Generates the percentages of all symbol rolled.
@return comma-separated string of rolled percentages.
*/
string BlackDie::rollPercentages()
{
	string percentages = "Total Roll Count: ";
	percentages.append(std::to_string(this->totalRolls));
	percentages.append("; ");
	double symbolPercent = 0;
	int stringSize = 0;
	string symbolStats = "";
	Symbol symbol;
	for (int i = 0; i < 6; i++)
	{
		symbolPercent = ((double)((double)this->rollStats[i]/ (double)this->totalRolls)) * 100;
		symbol = validateSymbol(static_cast<Symbol>(i + 1));
		symbolStats.append(generateSymbolString(symbol));
		symbolStats.append(": ");
		stringSize = (std::to_string(symbolPercent)).length();
		symbolStats.append((std::to_string(symbolPercent)).substr(0,stringSize-4));
		symbolStats.append("%; ");
		percentages.append(symbolStats);
		symbolStats.clear();
	}
	return  percentages;
}

/*
Generate a seed for rand function. 
*/
void BlackDie::setSeed() { this->seed = ((int)time(0))*rand();}

/*
Generates the a die Symbol.
@param symbol Symbol to be converted.
@return Name of the symbol.
*/
string BlackDie::generateSymbolString(Symbol symbol) {
	string symbolString = "";
	switch (symbol) {
	case ENERGY:
		symbolString = "Energy";
		break;
	case ATTACK:
		symbolString = "Attack";
		break;
	case DESTRUCTION:
		symbolString = "Destruction";
		break;
	case HEAL:
		symbolString = "Heal";
		break;
	case CELEBRITY:
		symbolString = "Celebrity";
		break;
	case OUCH:
		symbolString = "Ouch!";
		break;
	default:
		throw KingOfNewYorkExceptions::DieValueOutOfRangeException();
		break;
	}

	return symbolString;
}

Symbol BlackDie::validateSymbol(Symbol s)
{
	if (s < 1 || s > 6)
		throw KingOfNewYorkExceptions::DieValueOutOfRangeException();
	return s;
}

std::ostream& operator <<(std::ostream& outs, const BlackDie& die)
{
	outs << die.symbol;
	return outs;
}
