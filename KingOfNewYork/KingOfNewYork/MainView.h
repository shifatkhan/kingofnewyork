#pragma once
/** The header file for MainView.

	@author Shifat Khan
	@version 1.0 11/16/2018
*/
#ifndef MAINVIEW_H
#define MAINVIEW_H

#include "Observer.h"
#include "MainGameLoop.h"
#include "Player.h"

class MainView : public Observer{
public:
	MainView(MainGameLoop::MainGameLoop* mainGameLoop);
	virtual ~MainView() {}
	void Update(int gameState);

private:
	MainGameLoop::MainGameLoop* mainGameLoop;

	void playerTurnView();
	void playerDeadView();
	void playerResolveDiceView();
	void playerMovingView();
	void playerBuyingView();
	void gameEndedView();
	void gameStatsView();
	void gameDiceRollView();
};

#endif