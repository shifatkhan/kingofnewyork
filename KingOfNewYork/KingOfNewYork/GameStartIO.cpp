#include "GameStartIO.h"
#include <Windows.h>
#include "IOHelper.h"
#include "DiceRollingFacility.h"
#include "TileSetUpDriver.h"
#include "PlayerStrategy.h"
using namespace std;



/**
*	Handles the Game Start of the Game
*	Loads the pieces of the Game
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 11/4/2018
*/

//Constructor
GameStartIO::GameStartIO(){
	players = vector<Player*>();
	players.reserve(6);
	entryToManhattan = nullptr;
}

/*
*	Function to load all the game components. Handles IO.
*/
void GameStartIO::start()
{
	generateTitleScreen();
	this->loadMap();
	loadDeck();
	int playerNum = this->selectPlayerNumbers();
	createPlayers(playerNum);
	vector<TileStack> stacks = loadStacks();
	for (int i = 0; i < mloader.getMap()->getRegionSize(); i++) {
		vector<TileStack> tempStacks;
		tempStacks.reserve(3);
		for (int j = 0; j < 3; j++) {
			TileStack tempStack = stacks.back();
			tempStacks.push_back(tempStack);
			stacks.pop_back();
		}
		mloader.getMap()->getRegions()->at(i)->setTileStack(tempStacks);
	}

	this->displayAll();
}

MapLoader GameStartIO::getMapLoader()
{
	return this->mloader;
}

Map* GameStartIO::getMap() {
	return mloader.getMap();
}

vector<Player*> GameStartIO::getPlayers()
{
	return this->players;
}

Deck GameStartIO::getDeck()
{
	return this->deck;
}

void GameStartIO::displayAll() {
	this->deck.displayPlayDecks();

	cout << "\nList of players\n";
	for (int i = 0; i < players.size(); i++) {
		players.at(i)->displayPlayerStatus();
	}

	this->mloader.getMap()->printMap();

}

void GameStartIO::loadMap() {
	bool loaded = false;
	do {
		string path = selectPath();
		if (this->isPathValid(path)) {
			mloader.setLoadMode(LoadMode::EXISTING);
			mloader.setFilePath(path);
			try {
				mloader.activateMap();
				if (mloader.getMap()->areAllZonesConnected() 
					&& MapUtil::containsManhattan(this->getMap())
					&& MapUtil::validatesManhattanZone(this->getMap())
					&&  mloader.getMap()->getRegionSize() <=5 )
					loaded = true;
				else {
					cout << "This map failed one of the following conditions\n";
					cout << "\tUnconnected Map\n";
					cout << "\tDoes not contain Manhattan\n";
					cout << "\tToo many regions. We don't have enough tiles for this map configuration\n";
					cout << "\tEach zones of Manhattan subsection isn't connected to all zones of the next Manhattan subsection\n";
					delete (mloader.getMap());
				}
			}
			catch (MapExceptions::InvalidMapDataException &imdata) {
				cout << "Map is not valid\n";
			}
			catch (exception &e) {
				cout << "Map is not valid\n";
			}

		}
		else {
			cout << "Path not valid\n";
		}

	} while (loaded == false);
}

string GameStartIO::selectPath()
{
	vector<string> files;
	this->listFilesInDir("Map", files);

	for (int i = 0; i < files.size(); i++)
		cout << files.at(i) << endl;

	string path;
	cout << "Please choose a Map file: ";
	cin >> path;
	cout << endl;
	return path;

}

int GameStartIO::selectPlayerNumbers()
{
	int num = 0;
	string value;
	char char_arr[10];
	bool selected = false;

	do {
		cout << "Choose amount of players: ";
		cin >> value;
		cout << endl;


		num = atol(value.c_str());
		if (num >= 1 && num <= 6) {
			selected = true;
		}

	} while (selected == false);
	return num;
}

/**
*	Load to a vector the list of paths to a file inside a directory

*	Special thanks to NTDLS https://stackoverflow.com/questions/2314542/listing-directory-contents-using-c-and-windows
*/

void GameStartIO::listFilesInDir(const char *subDir, vector<string> &files)
{
	WIN32_FIND_DATA findFile;
	HANDLE handleFind = NULL;

	char subPath[1000];


	sprintf_s(subPath, "%s/*.*", subDir);
	handleFind = FindFirstFile(subPath, &findFile);

	if (handleFind == INVALID_HANDLE_VALUE){
		cout << "Path not found" << endl;
		return;
	}

	do
	{
		if (strcmp(findFile.cFileName, ".") != 0
			&& strcmp(findFile.cFileName, "..") != 0){
			sprintf_s(subPath, "%s/%s", subDir, findFile.cFileName);
			if (findFile.dwFileAttributes &FILE_ATTRIBUTE_DIRECTORY){
				listFilesInDir(subPath, files);
			}
			else {
				string file = subPath;
				files.push_back(file);
			}
		}
	} while (FindNextFile(handleFind, &findFile));

	FindClose(handleFind);

}

bool GameStartIO::isPathValid(string path) {
	std::ifstream stream(path);
	return (bool)stream;
}

/**
*	Loads the Deck in the game
*/
void GameStartIO::loadDeck() {
	deck.setupDeck();
	deck.displayPlayDecks();
}

/**
*	Handles the IO for creation of Player object
*/
Player* GameStartIO::createPlayer(int playerIndex) {
	
	string name;
	Player* player = new Player();
	cout << "Hello Player "<< playerIndex <<"\n";
	cout << "Enter name: ";
	cin >> name;
	cout << endl;
	PlayerMode mode = this->choosePlayerMode();
	this->setPlayerStrategy(player, mode);
	DiceRollingFacility* drf = new DiceRollingFacility;
	player->setDiceRollingFacility(drf);
	player->setName(name);
	this->displayMonsterCards(deck.getMonsterCards());
	int monsterNum = player->setUpInput("Choose a monster (Type a value that is inside a squared bracket): ", "Try Again\n", 1, deck.getMonsterCards().size());
	monsterNum--;
	Card card = deck.getMonsterCards().at(monsterNum);
	player->setMonsterCard(card);
	deck.removeMonsterCard(card.getName());

	return player;
}

/*
*	Creates num amount of Players
*/
void GameStartIO::createPlayers(int num) {
	vector<Card> monstercards = deck.getMonsterCards();

	for (int i = 0; i < num; i++) {
		players.push_back(createPlayer(i + 1));
	}

	cout << "List of Players: \n";
	for (int i = 0; i < players.size(); i++) {
		cout << "\t" << players.at(i)->getName()<< " is a " << players.at(i)->getMonsterCard().getName() << endl;
	}

	
}


void GameStartIO::displayMonsterCards(vector<Card> monsterCards) {
	cout << "List of Monster cards to choose\n\n";
	for (int i = 0; i < monsterCards.size(); i++) {
		cout << "[" << i + 1 << "] - " << monsterCards.at(i).getName() << endl;
	}
}

vector<TileStack> GameStartIO::loadStacks() {
	vector<Tile> tiles =TileSetUpDriver::generateTiles();
	TileStack shuffledTileStack = TileSetUpDriver::shuffleTiles(tiles);
	return TileSetUpDriver::createTileStacksForGame(shuffledTileStack, 15, 3);
}

PlayerMode GameStartIO::choosePlayerMode()
{
	vector<PlayerMode> modesToChoose = {PlayerMode::Human, PlayerMode::Aggressive_AI, PlayerMode::Moderate_AI};

	cout << "Choose the mode for the player\n";
	for (int i = 0; i < modesToChoose.size(); i++) {
		PlayerMode currMode = modesToChoose.at(i);
		if (currMode == Human) {
			cout << "[" << i + 1 << "] - Human Player\n";
		}
		else if(currMode == Aggressive_AI) {
			cout << "[" << i + 1<< "] - Agressive AI\n";
		}
		else if (currMode == Moderate_AI) {
			cout << "[" << i + 1 << "] - Moderate AI\n";
		}
	}
	int input = IOHelper::getValidIntOutput("Please choose a player mode: ", "Try again\n", 1, 3);
	input--;
	return modesToChoose.at(input);
}

/*
*	Sets the appropriate PlayerStrategy instance to the Player
*/
void GameStartIO::setPlayerStrategy(Player* player, PlayerMode mode)
{
	switch (mode)
	{
	case Human:
		player->setPlayerStrategy(new PlayerStrategy());
		break;
	case Aggressive_AI:
		player->setPlayerStrategy(new AgressiveAIStrategy());
		break;
	case Moderate_AI:
		player->setPlayerStrategy(new ModerateAIStrategy());
		break;
	default:
		player->setPlayerStrategy(new PlayerStrategy());
		break;
	}

	
}

void GameStartIO::generateTitleScreen()
{
	string r1 = " _  ___                ____   __   _   _                __     __        _";
	string r2 = "| |/ (_)              / __ \\ / _| | \\ | |               \\ \\   / /       | |";
	string r3 = "| ' / _ _ __   __ _  | |  | | |_  |  \\| | _____      __  \\ \\_/ /__  _ __| | _";
	string r4 = "|  < | | '_ \\ / _` | | |  | |  _| | . ` |/ _ \\ \\ /\\ / /   \\   / _ \\| '__| |/ /";
	string r5 = "| . \\| | | | | (_| | | |__| | |   | |\\  |  __/\\ V  V /     | | (_) | |  |   < ";
	string r6 = "|_|\\_\\_|_| |_|\\__, |  \\____/|_|   |_| \\_|\\___| \\_/\\_/      |_|\\___/|_|  |_|\\_\\";
	string r7 = "               __/ |                                                           ";
	string r8 = "              |___/                                                          ";

	cout << r1 << endl << r2 << endl << r3 << endl << r4 << endl << r5 << endl << r6 << endl << r7 << endl << r8 << endl;
}
