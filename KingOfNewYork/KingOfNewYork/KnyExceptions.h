#ifndef KNYEXCEPTIONS_H
#define KNYEXCEPTIONS_H
#include <iostream>
#include <exception>

namespace KingOfNewYorkExceptions
{
	struct EmptyTileStackException : public std::exception
	{
		const char * what() const throw() { return "Error - There is no more Tiles here."; };
	};
	struct DieValueOutOfRangeException : public std::exception
	{
		const char * what() const throw() { return "Error - Black Die value must be between 1 and 6."; };
	};

	struct InvalidDieSymbolException : public std::exception
	{
		const char * what() const throw() { return "Error - Invalid Black Die symbol."; };
	};

	struct OutOfRollException : public std::exception
	{
		const char * what() const throw() { return "Error - User is out of rolls."; };
	};

	struct EmptyDeckException : public std::exception
	{
		const char * what() const throw() { return "Error - Deck is empty."; };
	};

	struct DeckIndexOutOfRangeException : public std::exception
	{
		const char * what() const throw() { return "Error - Trying to access deck's out of bounds index."; };
	};

	struct CardNotImplementedException : public std::exception
	{
		const char * what() const throw() { return "Error - This card was not implemented."; };
	};
};

#endif //!KNYECEPTIONS_H
