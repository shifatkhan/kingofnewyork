#ifndef TILESETUPDRIVER_H
#define TILESETUPDRIVER_H
#include <vector>
#include "Tile.h"
#include "TileStack.h"
/*
Driver for tests and set up of Building/Unit Tiles
@author Hau Gilles Che
@version 1.0 10/10/2018
*/
namespace TileSetUpDriver
{
	//Trigger Function for all tests
	void testAll();
	//Test Functions
	TileStack shuffleTiles(std::vector<Tile> tiles);
	void setUpTilesForGame(TileStack tiles, int numOfStacks, int numOfTilesPerStack);
	std::vector<TileStack> createTileStacksForGame(TileStack tiles, int numOfStacks, int numOfTilesPerStack);

	//Helper Functions
	void displayTileStacks(std::vector<TileStack> tileStacks);
	std::vector<Tile> generateTiles();
	std::vector<Tile> generateDurability1Building(int numToCreate);
	std::vector<Tile> generateDurability2Building(int numTallToCreate,int numHospitalToCreate,int numPowerPlantToCreate);
	std::vector<Tile> generateDurability3Building(int numTallToCreate, int numHospitalToCreate, int numPowerPlantToCreate);
	std::vector<Tile> insertTiles(std::vector<Tile> tiles, int numToInsert, Tile tileToInsert);
}
#endif // !TILESETUPDRIVER_H

