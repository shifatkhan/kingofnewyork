#include "PlayerStrategy.h"
#include "MoveDriver.h"
#include "BuyCardsDriver.h"
#include "RollDiceDriver.h"
#include "AiMoveDriver.h"
#include "AiUtil.h"

//Default constructor
PlayerStrategy::PlayerStrategy()
{

}

PlayerStrategy::~PlayerStrategy()
{
}

/**
*	Calls for the MoveDriver's Loop that contains human interaction
*/
void PlayerStrategy::move(Player * player, Map * map, int number_of_players, MainGameLoop::MainGameLoop* loop)
{
	MoveDriver::moveLoop(player, map, number_of_players, loop);
}

/**
*	Calls for the MoveDriver's moveOut driver
*/
void PlayerStrategy::moveOutManhattan(Player * player, Map * map, int number_of_players)
{
	MoveDriver::moveOutLoop(player, map, number_of_players);
}

void PlayerStrategy::buyCards(Player* player, Deck & deck, MainGameLoop::MainGameLoop & loop) {
	BuyCardsDriver::start(*player, deck, loop);
}

void AIStrategy::buyCards(Player* player, Deck& deck, MainGameLoop::MainGameLoop & loop) {
	BuyCardsDriver::startForAI(*player, deck);
}

void PlayerStrategy::rollDice(Player* player, MainGameLoop::MainGameLoop* loop) {
	RollDiceDriver::rollDice(player, loop);
}


void PlayerStrategy::resolveDice(Player * player, Map * map, int num_of_players, MainGameLoop::MainGameLoop * loop)
{
	player->resolveDice(map, num_of_players, loop);
}

int PlayerStrategy::chooseTileToDestroy(bool canDestroyBuildings, bool canDestroyUnits)
{
	string input;
	int numInput;
	cin >> input;

	numInput = atol(input.c_str());
	return numInput;
}

int PlayerStrategy::chooseBuildingToDestroy(int destructCtr, vector<TileStack> buildings)
{
	string input;

	cin >> input;
	
	return  atol(input.c_str());
}

int PlayerStrategy::chooseUnitToDestroy(int destructCtr, vector<Tile> units)
{
	string input;
	
	cin >> input;

	return atol(input.c_str());
}

int PlayerStrategy::chooseSymbolToResolve(vector<Symbol> symbolsToResolve)
{
	
	string input;
	int numInput;

	cin >> input;
	numInput = atol(input.c_str());

	return numInput;
}

int PlayerStrategy::setUpInput(int min, int max) {
	string input;
	cin >> input;
	return atol(input.c_str());
}

AIStrategy::AIStrategy(){}

AIStrategy::~AIStrategy() {}


void AIStrategy::resolveDice(Player * player, Map * map, int num_of_players, MainGameLoop::MainGameLoop* loop)
{
	player->resolveDice(map, num_of_players, loop);
}

void AIStrategy::move(Player * player, Map * map, int number_of_players, MainGameLoop::MainGameLoop* loop)
{
	vector<MoveOption> options = {MoveOption::DoNothing};
	AiMoveDriver::move(player, options, map, number_of_players);
}

/**
* Basic AI decisional logic for building destruction, simply destroy the first destructable building in sight.
*/
int AIStrategy::chooseBuildingToDestroy(int destructCtr, vector<TileStack> buildings)
{
	int destroyIndex = -1;
	int i = 0;
	do
	{
		if (buildings[i].getTopTile().getBuildingDurability() <= destructCtr)
		{
			destroyIndex = i;
		}
		i++;
	} while ((destroyIndex < 0) || i <buildings.size());
	

	//Makes it look like the AI is thinking/prevents the screen from just cascading 
	AiUtil::loadingEffect();
	cout << destroyIndex << endl;
	
	return destroyIndex;
}

/**
* Basic AI decisional logic for unit destruction, simply destroy the first destructable unit in sight.
*/
int AIStrategy::chooseUnitToDestroy(int destructCtr, vector<Tile> units)
{
	int destroyIndex = -1;
	int i = 0;
	do
	{
		if (units[i].getUnitDurability() <= destructCtr)
		{
			destroyIndex = i;
		}
		i++;
	}while (destroyIndex < 0);

	//Makes it look like the AI is thinking/prevents the screen from just cascading 
	AiUtil::loadingEffect();
	cout << destroyIndex << endl;

	return destroyIndex;
}

/**
* Basic AI symbol resolving decisional logic. Just to make it slightly less stupid, ensures that the AI would resolve
* Ouch first if any was rolled so if any Heal was rolled, he can heal.
*/
int AIStrategy::chooseSymbolToResolve(vector<Symbol> symbolsToResolve)
{
	int symbolVal = symbolsToResolve[0];
	for (Symbol s : symbolsToResolve)
	{
		if (s == OUCH)
		{
			symbolVal = OUCH;
		}
	}
	//Makes it look like the AI is thinking/prevents the screen from just cascading 
	AiUtil::loadingEffect();
	cout << symbolVal << endl;
	return symbolVal;
}

int AIStrategy::setUpInput(int min, int max) {
	if (min < 0) {
		min = 1;
	}
	int value = AiUtil::random(min, max);
	cout << value << endl;
	AiUtil::loadingEffect();
	return value;
}

AgressiveAIStrategy::AgressiveAIStrategy(){}

AgressiveAIStrategy::~AgressiveAIStrategy(){}



void AgressiveAIStrategy::rollDice(Player * player, MainGameLoop::MainGameLoop* loop)
{
	vector<Symbol> keep = {Symbol::ATTACK, Symbol::DESTRUCTION};
	RollDiceDriver::rollDiceForAI(player, keep);
}

void AgressiveAIStrategy::moveOutManhattan(Player * player, Map * map, int number_of_players)
{
	AiMoveDriver::moveOut(player, true, map, number_of_players);
}

/**
* Aggressive AI focused on desctruction. Will attempt to destroy buildings whenever he can. 
*/
int AgressiveAIStrategy::chooseTileToDestroy(bool canDestroyBuildings, bool canDestroyUnits)
{
	int choice;
	if (canDestroyBuildings)
	{
		choice = 1;
	}
	else
	{
		choice = 2;
	}

	//Makes it look like the AI is thinking/prevents the screen from just cascading 
	AiUtil::loadingEffect();
	cout << choice << endl;
	return choice;
}

ModerateAIStrategy::ModerateAIStrategy()
{
}

ModerateAIStrategy::~ModerateAIStrategy()
{
}

void ModerateAIStrategy::rollDice(Player * player, MainGameLoop::MainGameLoop* loop)
{
	vector<Symbol> keep = { Symbol::ATTACK, Symbol::HEAL, Symbol::ENERGY };
	RollDiceDriver::rollDiceForAI(player, keep);
}

void ModerateAIStrategy::moveOutManhattan(Player * player, Map * map, int number_of_players)
{
	AiMoveDriver::moveOut(player, false, map, number_of_players);
}

/**
* Moderate AI tile destruction decisional logic. Since moderate tries to balance his health, he will destroy units if he can to prevent damage.
*/
int ModerateAIStrategy::chooseTileToDestroy(bool canDestroyBuildings, bool canDestroyUnits)
{
	int choice;
	if (canDestroyUnits)
	{
		choice = 2;
	}
	else
	{
		choice = 1;
	}

	//Makes it look like the AI is thinking/prevents the screen from just cascading 
	AiUtil::loadingEffect();
	cout << choice << endl;
	return choice;
}

