/*
*	Serves as the main interface for Player's actions
*	@author Thai-Vu Nguyen
*	@version 1.0 11/18/2018
*/

#include "PlayerActor.h"

void PlayerActor::setPlayerStrategy(PlayerStrategy* strategy)
{
	this->strategy = strategy;
}

void PlayerActor::rollDice(Player * player, MainGameLoop::MainGameLoop * loop)
{
	this->strategy->rollDice(player, loop);
}

void PlayerActor::resolveDice(Player* player, Map* map, int num_of_player, MainGameLoop::MainGameLoop * loop) {

	this->strategy->resolveDice(player, map, num_of_player, loop);
}

void PlayerActor::move(Player * player, Map * map, int num_of_player, MainGameLoop::MainGameLoop* loop)
{
	this->strategy->move(player, map, num_of_player, loop);
}

void PlayerActor::buyCards(Player * player, Deck & deck, MainGameLoop::MainGameLoop& loop)
{
	this->strategy->buyCards(player, deck, loop);
}


