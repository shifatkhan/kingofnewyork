#include "AiMoveDriver.h"

/**
*	Implementation file for the AiMoveDriver
*	Handles automated Move logic for the AI
*	@author Thai-Vu Nguyen
*/

/*
*	Move logic for the AI
*	@param Player
*	@param vector of MoveOption to prioritize
*	@param number of players in the game
*/
void AiMoveDriver::move(Player * player, vector<MoveOption> priorities, Map * map, int num_of_player)
{
	cout << "\nThe move phase for " << player->getName() << "\n\n";
	MoveManager manager(map);
	vector<MoveOption> options;
	options.reserve(6);

	options = manager.possibleMoves(player, num_of_player);
	
	//There's somehow no move avaible, just add the Option to do nothing
	if (options.size() == 0)
		options.push_back(MoveOption::DoNothing);

	
	MoveOption option;
	if (options.size() == 1) {
		//Only option available
		option = options.at(0);
	}
	else {
		//Filter the available moves with the options in priority
		vector<MoveOption> filtered = filterOptions(options, priorities);
		
		int index = 0;
		if (filtered.size() > 0) {
			//Pick a preferable move
			index = AiUtil::random(0, filtered.size() - 1);
			option = filtered.at(index);
		}
		else {
			//Pick random options
			index = AiUtil::random(0, options.size() - 1);
			option = options.at(index);
		}
		
	}
	processInput(player, map, num_of_player, option);

}

/*
*	Logic handling moving out of manhattan for an AI
*/
void AiMoveDriver::moveOut(Player * player, bool stay, Map * map, int num_of_players)
{
	cout << player->getName() << " took damage inside zone "<< player->getZone()->getId()  
		<< " of " << player->getZone()->getType() << " " << MapUtil::zoneToRegion(player->getZone(), map)->getRegionName() << endl;
	if (stay) {
		cout << player->getName() << " is stubborn and will stay in his burrough of Manhattan "<< endl;
	}
	else {
		cout << player->getName() << " will move out of Manhattan " << endl;
		vector<Zone*> zones = MapUtil::getTakableZonesOutisdeManhattan(map);
		displayAvailableZones(zones, map);
		if (zones.size() == 0) {
			cout << player->getName() << " will stay in his burrough after all.\n";
		}
		else {
			int index = AiUtil::random(0, zones.size() - 1);
			Zone* zoneChosen = zones.at(index);
			cout << player->getName() << " will move to Zone " << zoneChosen->getId() << " of " << MapUtil::zoneToRegion(zoneChosen, map)->getRegionName() << endl;
			player->move(zoneChosen);
		}
	}
}

void AiMoveDriver::displayAvailableZones(vector<Zone*> zones, Map * map)
{
	cout << "Here are the zones available to take \n";
	if (zones.size() == 0) {
		cout << "No zones are available\n";
	}
	else {
		for (int i = 0; i < zones.size(); i++) {
			cout << "[" << i + 1 << "] - Zone " << zones.at(i)->getId() << " of " << MapUtil::zoneToRegion(zones.at(i), map)->getRegionName() << endl;
		}
	}
	
}

/*
*	Filters available options with desired options from AI
*	@param available options
*	@param desired options
*/
vector<MoveOption> AiMoveDriver::filterOptions(vector<MoveOption> options, vector<MoveOption> priorities)
{
	vector<MoveOption> filtered;
	filtered.reserve(options.size());

	for (int i = 0; i < options.size(); i++) {
		if (isInPriority(options.at(i), priorities))
			filtered.push_back(options.at(i));
	}
		
	return filtered;
}

/*
*	Checks if the MoveOption is in the vector of MoveOption
*/
bool AiMoveDriver::isInPriority(MoveOption option, vector<MoveOption> priorities)
{
	for (int i = 0; i < priorities.size(); i++) {
		if (option == priorities.at(i))
			return true;
	}
	return false;
}

void AiMoveDriver::processInput(Player * player, Map * map, int num_of_players, MoveOption option)
{
	AiUtil::loadingEffect();
	switch (option) {
	case DoNothing:
		cout << player->getName() << " will remain here in "<< MapUtil::zoneToRegion(player->getZone(), map)->getRegionName() <<".\n";
		break;
	case MoveUpManhattan:
		getUpManhattan(player, map, num_of_players);
		break;
	case EnterManhattan:
		enterManhattan(player, map, num_of_players);
		break;
	case EnterNonManhattan:
		getUpManhattan(player, map, num_of_players);
		break;
	default:
		break;
	}
}

/*
*	Function to have the AI enter Manhattan
*/
void AiMoveDriver::enterManhattan(Player * player, Map * map, int num_of_players)
{
	vector<Zone*> zones = MapUtil::getEntryToManhattan(map);
	cout << player->getName() << " is entering Manhattan\n";

	displayAvailableZones(zones, map);

	AiUtil::loadingEffect();
	Zone* zone = chooseRandomZones(zones);
	player->move(zone);
	cout << player->getName() << " moved to Zone " << zone->getId() << " of " << zone->getType() << " " << MapUtil::zoneToRegion(zone, map)->getRegionName() << endl;
	int vp =player->getVP();
	vp++;
	player->setVP(vp);
	//map->printMap();
}

/*
*	Function to have the AI move up manhattan
*/
void AiMoveDriver::getUpManhattan(Player * player, Map * map, int num_of_players)
{
	vector<Zone*> zones = MapUtil::getNextManhattanZone(map, player->getZone());
	cout << player->getName() << " is his making his way up Manhattan \n";
	AiUtil::loadingEffect();
	Zone* zone = chooseRandomZones(zones);
	player->move(zone);
	cout << player->getName() << " moved to Zone " << zone->getId() << " of " << zone->getType() << " " << MapUtil::zoneToRegion(zone, map)->getRegionName() << endl;
	//map->printMap();
}

void AiMoveDriver::enterBurroughOutsideManhattan(Player * player, Map * map, int num_of_players)
{
	vector<Zone*> zones = MapUtil::getTakableZonesOutisdeManhattan(map);
	cout << player->getName() << " is making his move\n";
	AiUtil::loadingEffect();
	Zone* zone = chooseRandomZones(zones);
	player->move(zone);
	cout << player->getName() << " moved to Zone " << zone->getId() << " of " << MapUtil::zoneToRegion(zone, map)->getRegionName() << endl;
	//map->printMap();
}

Zone * AiMoveDriver::chooseRandomZones(vector<Zone*> zones)
{
	int index = AiUtil::random(0, zones.size() - 1);
	return zones.at(index);
}
