#include "RollDiceDriver.h"

/*
*	Handles dice roll phase of a Player's turn
*	
*	@author Hau Gilles Che
*	@author Thai-Vu Nguyen
*/


namespace RollDiceDriver {

	/**
		*Dice reroll phase of a game turn.
		*@param the current player.
		*/
	void rollDice(Player* player, MainGameLoop::MainGameLoop* loop)
	{
		DiceRollingFacility* playerDice = new DiceRollingFacility;
		playerDice = player->getDiceRollingFacility();
		string keepAll = "";
		string userInput;
		int numUserInput;
		int rollCtr = 0;
		array<string, 6> toKeepInput;
		array<bool, 6> keep;
		bool goodInput;
		bool reroll = true;
		array<BlackDie, 6> dice;

		while (reroll && (rollCtr < 3))
		{
			goodInput = false;
			playerDice->rollDice();
			rollCtr = playerDice->getRollCtr();

			if (rollCtr < 3)
			{
				while (!goodInput)
				{
					displayRollOptions();
					cin >> userInput;
					numUserInput = atol(userInput.c_str());

					switch (numUserInput)
					{
					case -1:
						// Access the Game's statistics menu.
						loop->setGameState(MainGameLoop::STATS_MENU);
						break;
					case 1:
						keep = { true,true,true,true,true,true };
						goodInput = true;
						reroll = false;
						break;
					case 2:
						keep = displayDiceToKeep(playerDice->getDice());
						goodInput = true;
						break;
					default:
						cout << "Invalid input. Please choose from the options provided." << endl;
						break;
					}
					// Double check if valid input was provided.
					if (goodInput)
						playerDice->chooseDiceToKeep(keep);
				}
			}
		}
	}

	/**
		* Displays the options a player can choose from during reroll phase.
		*/
	void displayRollOptions()
	{
		cout << "\nSelect one of the options below:" << endl;
		cout << "[1] Keep all your dice." << endl;
		cout << "[2] Reroll between 1 to 6 die." << endl;
		cout << "[-1] See Game statistics." << endl;
		cout << "Choose your option:";
	}

	/**
	* Displays and accept the player's decision on keeping his dice or rerolling them.
	*@return List of the choices the player made.
	*/
	array<bool, 6> displayDiceToKeep(array<BlackDie, 6> dice)
	{
		array<bool, 6> toKeepInput;
		bool goodInput;
		string input;
		int inputNum;

		cout << "\nSelecting the dice to keep:" << endl;
		for (int i = 0; i < 6; i++)
		{
			goodInput = false;
			while (!goodInput)
			{
				cout << "\nDie: [" << i << "] " << dice[i] << endl;
				cout << "Select one of the options below:" << endl;
				cout << "[1] Keep die." << endl;
				cout << "[2] Reroll die." << endl;
				cout << "Choose your option: ";
				cin >> input;
				inputNum = atol(input.c_str());

				switch (inputNum)
				{
				case 1:
					toKeepInput[i] = true;
					goodInput = true;
					break;
				case 2:
					toKeepInput[i] = false;
					goodInput = true;
					break;
				default:
					cout << "Invalid input. Please choose from the selection." << endl;
					break;
				}
			}
		}
		return toKeepInput;
	}

	/**
	*	Automated Dice roll and reroll phase
	*	@param Player the current player
	*	@param vector<Symbo> a vector of dice symbols to keep
	*/
	void rollDiceForAI(Player * player, vector<Symbol> toKeep)
	{
		string name = player->getName();
		DiceRollingFacility* drf = new DiceRollingFacility;
		drf = player->getDiceRollingFacility();
		array<BlackDie, 6> dice;
		int rollctr = 0;
		bool reroll = true;
		do {
			int keepctr = 0;
			if (rollctr == 0) {
				drf->rollAllDice();
			}
			else {
				drf->rollDice();
			}
			
			rollctr = drf->getRollCtr();
			dice = drf->getDice();

			AiUtil::loadingEffect();

			if (rollctr < 3) {
				cout << name << " will keep:\n ";
				for (int i = 0; i < dice.size(); i++) {
					if (keepDice(dice.at(i), toKeep)) {
						dice.at(i).setIsKept(true);
						keepctr++;
						cout << "\t" << dice.at(i) << endl;
					}
				}
				if (keepctr == 0)
					cout << " no dice\n";

				if (keepctr == 6) {
					cout << name << " is satisfied with all his dice. He will keep of his dice\n";
					reroll = false;
				}
			}
			
			drf->setDice(dice);

			AiUtil::loadingEffect();

			

		} while (reroll && rollctr < 3);
		drf->setRollCtr(0);
	}

	/**
	*	check if the die's symbol is in the list of symbols to keep
	*	@return true if die should be kept
	*/
	bool keepDice(BlackDie die, vector<Symbol> keep)
	{
		for (int i = 0; i < keep.size(); i++) {
			if (die.getSymbol() == keep.at(i))
				return true;
		}
		return false;
	}
}