#ifndef GAMESTARTDRIVER_H
#define GAMESTARTDRIVER_H

#include "GameStartIO.h"

/**
*	Driver for the Game Start
*	Loads the pieces of the Game
*	@author Thai-Vu Nguyen 26325440
*	@version 1.0 11/4/2018
*/

namespace GameStartDriver {
	GameStartIO run();
}

#endif // !GAMESTARTDRIVER_H
