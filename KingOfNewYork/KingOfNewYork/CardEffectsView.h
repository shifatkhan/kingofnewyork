#ifndef CARDEFFECTSVIEW_H
#define CARDEFFECTSVIEW_H

#include "Observer.h"
#include "Player.h"

class CardEffectsView : public Observer {
public:
	CardEffectsView(Player* player);
	~CardEffectsView() {};
	void Update(int gameState);

private:
	Player* player;

	inline void egoTripView();
	inline void nextStageView();
	inline void PowerSubstationView();
	inline void brooklynBridgeView();
	inline void subterraneanCableView();
};

#endif // !CARDEFFECTSVIEW_H
