#ifndef MAPEXCEPTIONS_H
#define MAPEXCEPTIONS_H

#include <iostream>
#include <exception>

/*
*	Set of Custom Exceptions used for Map and MapLoader
*	@author Thai-Vu Nguyuen
*/

namespace MapExceptions {
	struct ZoneOutOfRangeException : public std::exception {
		const char *what() const throw() { return "Error: the Zone ID is out of the valid expected range\n"; }
	};

	struct RegionOutOfRangeException : public std::exception {
		const char *what() const throw() { return "Error: The Region ID is out the valid expected range\n"; }
	};

	struct InvalidMapDataException : public std::exception {
		const char *what() const throw() { return "Error: The Map Data is erroneous\n"; }
	};

	struct MapNotLoadedException :public std::exception {
		const char *what() const throw() { return "Error: The Map is not loaded\n"; }
	};

	struct SameIDException : public std::exception {
		const char *what() const throw() {return "Error: Trying to add an Entity with the same ID\n"; }
	};

	struct NullPtrException : public std::exception {
		const char *what() const throw() { return "Error: Trying to add a Pointer Entity that is null\n"; }
	};

	struct NoEntityMatchException : public std::exception {
		const char *what() const throw() { return "Error: This entity does not exist in the Map"; }
	};
}

#endif // ! MAPEXCEPTIONS_H
