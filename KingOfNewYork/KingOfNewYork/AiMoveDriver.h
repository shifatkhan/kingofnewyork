#ifndef AIMOVEDRIVER_H
#define AIMOVEDRIVER_H

#include "Player.h"
#include "Map.h"
#include "MoveManager.h"
#include "MapUtil.h"
#include "AiUtil.h"

namespace AiMoveDriver {
	void move(Player* player, vector<MoveOption> priorities, Map* map, int num_of_player);
	void moveOut(Player* player, bool stay, Map* map, int num_of_players);
	void displayAvailableZones(vector<Zone*> zones,  Map* map);
	vector<MoveOption> filterOptions(vector<MoveOption> options, vector<MoveOption> priorities);
	bool isInPriority(MoveOption option, vector<MoveOption> priorities);
	void processInput(Player* player, Map* map, int num_of_players, MoveOption option);
	void enterManhattan(Player* player, Map* map, int num_of_players);
	void getUpManhattan(Player* player, Map* map, int num_of_players);
	void enterBurroughOutsideManhattan(Player* player, Map* map, int num_of_players);
	Zone* chooseRandomZones(vector<Zone*> zones);
}

#endif // !AIMOVEDRIVER_H
