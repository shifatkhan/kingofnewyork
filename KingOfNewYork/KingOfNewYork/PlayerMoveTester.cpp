#include "PlayerMoveTester.h"

void PlayerMoveTester::testPlayerMove() {
	MapLoader load = MapLoader(LoadMode::EXISTING, "Map/Test2/Renuchan.map");
	Player* player = new Player();
	player->setName("Renuchan");
	
	try {
		load.activateMap();
		load.getMap()->connectTheEdges();
	}
	catch (exception &e) {
		cout << "Something went wrong with the map";
		return;
	}

	int decision = -1;
	cout << "\nTest of Player Move \n";
	cout << "Welcome " << player->getName();

	Map* map = load.getMap();

	vector<Zone*>* zones = map->getZones();
	

	while (true) {
		int decision = -1;
		cout << "\nHere are the zones and their ownership\n";
		for (int i = 0; i < zones->size(); i++) {
			cout <<"Zone ID" << zones->at(i)->getId() << " owner: ";
			if (zones->at(i)->getPlayer() != NULL && zones->at(i)->getPlayer() != nullptr)
				cout << zones->at(i)->getPlayer()->getName();
			else
				cout << "no one";
			cout << "\n";
		}


		cout << "\nMove to one of theses these Zones :\n";
		for (int i = 0; i < zones->size(); i++) {
			cout << " - [" << zones->at(i)->getId() <<"] - ";
		}

		cout << "Your Zone to take (press other digit to quit): ";
		cin >> decision;
		cout << "\n\n";
		if (decision >= zones->size() || decision < 0)
			return;
		else {
			player->move(zones->at(decision));
		}
	}

	


}