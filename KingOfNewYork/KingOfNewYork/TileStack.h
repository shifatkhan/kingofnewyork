#ifndef TILESTACK_H
#define TILESTACK_H
#include <vector>
#include "Tile.h"
/*
Data Structure Representing a stack of King of New York building/unit Tiles.
@author Hau Gilles Che
@version 1.0 10/10/2018
*/
class TileStack
{
public:
	//Constructors
	TileStack();
	TileStack(std::vector<Tile> tiles);
	
	//Accessors
	std::vector<Tile> getTiles();
	Tile getTopTile();
	//Mutators
	void setTiles(std::vector<Tile> tiles);

	//Actions
	Tile removeTopTile();
	void shuffle();
	
private:
	std::vector<Tile> tiles;
};
#endif // !TILESTACK_H
