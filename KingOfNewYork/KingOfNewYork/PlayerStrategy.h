#ifndef PLAYERSTRATEGY_H
#define PLAYERSTRATEGY_H

#include "Player.h"
#include "Deck.h"

class PlayerStrategy {
public:
	PlayerStrategy();
	//Decisional logic for choosing a building to destroy
	virtual ~PlayerStrategy();
	virtual void rollDice(Player* player, MainGameLoop::MainGameLoop * loop);
	virtual void resolveDice(Player* player, Map* map, int num_of_players, MainGameLoop::MainGameLoop * loop);
	virtual void move(Player* player, Map* map, int number_of_players, MainGameLoop::MainGameLoop* loop);
	virtual void moveOutManhattan(Player* player, Map* map, int number_of_players);
	virtual void buyCards(Player* player, Deck& deck, MainGameLoop::MainGameLoop & loop);
	virtual int chooseBuildingToDestroy(int destructCtr, vector<TileStack> buildings);
	//Decisional logic for choosing what symbol to resolve
	virtual int chooseSymbolToResolve(vector<Symbol> symbolsToResolve);
	//Decisional logic for choosing a type of tile to be destroyed
	virtual int chooseTileToDestroy(bool canDestroyBuildings, bool canDestroyUnits);
	//Decisional logic for choosing a unit to destroy
	virtual int chooseUnitToDestroy(int destructCtr, vector<Tile> units);
	virtual int setUpInput(int, int);
	
};

class AIStrategy: public virtual PlayerStrategy {
public:
	AIStrategy();
	virtual ~AIStrategy();
	
	//Automated Resolve
	virtual void resolveDice(Player* player, Map* map, int num_of_players, MainGameLoop::MainGameLoop* loop);
	//Automated Move: we'll do nothing outside of manhattan
	//Will move inside manhattan and move up
	virtual void move(Player* player, Map* map, int number_of_players, MainGameLoop::MainGameLoop* loop);
	//Automated Move: AI will not buy anything for simplicity sake for now
	virtual void buyCards(Player* player, Deck& deck, MainGameLoop::MainGameLoop & loop);
	virtual int chooseBuildingToDestroy(int destructCtr, vector<TileStack> buildings);
	virtual int chooseUnitToDestroy(int destructCtr, vector<Tile> units);
	virtual int chooseSymbolToResolve(vector<Symbol> symbolsToResolve);
	virtual int setUpInput(int, int);
};

class AgressiveAIStrategy : public virtual AIStrategy {
public:
	AgressiveAIStrategy();
	~AgressiveAIStrategy();
	//Will keep all attack die and destruction die
	virtual void rollDice(Player* player, MainGameLoop::MainGameLoop* loop);
	
	//Stays in Manhattan because he's agressive
	virtual void moveOutManhattan(Player* player, Map* map, int number_of_players);
	int chooseTileToDestroy(bool canDestroyBuildings, bool canDestroyUnits);
};

class ModerateAIStrategy : public virtual AIStrategy {
public:
	ModerateAIStrategy();
	~ModerateAIStrategy();
	//Will keep health, keep some attack die when out of manhattan
	virtual void rollDice(Player* player, MainGameLoop::MainGameLoop* loop);

	//Will get out of manhattan for safety
	virtual void moveOutManhattan(Player* player, Map* map, int number_of_players);
	int chooseTileToDestroy(bool canDestroyBuildings, bool canDestroyUnits);
};

#endif // !PLAYERSTRATEGY_H
