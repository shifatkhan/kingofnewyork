#include <sstream>
#include <iostream>
#include "Map.h"
#include "Region.h"
#include "Zone.h"
#include "MapDriver.h"

using namespace std;


	void MapDriverSpace::testWholeDFS() {
		cout << "\nTest case: Testing out the Full DFS of a simulated map" << endl;

		Region* regionA = new Region(0, "A");
		Region* regionB = new Region(1, "B");
		Region* regionC = new Region(2, "C");
		Region* regionD = new Region(3, "D");

		Zone* zone0 = new Zone(0, 0, {1});
		Zone* zone1 = new Zone(1, 1, {2,0});
		Zone* zone2 = new Zone(2, 1, {3,1});
		Zone* zone3 = new Zone(3, 2, {4});
		Zone* zone4 = new Zone(4, 3, {5});
		Zone* zone5 = new Zone(5, 3, {6});
		Zone* zone6 = new Zone(6, 3, {4});


		Map map("Test1", 10, 5);

		map.addRegion(regionA);
		map.addRegion(regionB);
		map.addRegion(regionC);
		map.addRegion(regionD);

		map.addZone(zone0);
		map.addZone(zone1);
		map.addZone(zone2);
		map.addZone(zone3);
		map.addZone(zone4);
		map.addZone(zone5);
		map.addZone(zone6);

		map.connectTheEdges();

		if (map.areAllZonesConnected()) {
			cout << "Zones are connected (Expected)" << endl;
		}
		else {
			cout << "Zones are not connected (Not Expected)" << endl;
		}
	}

	void MapDriverSpace::testRegionalDFS() {
		cout << "\nTest case: Testing out the Regional DFS of a simulated Map" << endl;

		Region* region1 = new Region(0, "A");
		Region* region2 = new Region(1, "B");


		Zone* zone0 = new Zone(0, 1);
		Zone* zone1 = new Zone(1, 0);
		Zone* zone2 = new Zone(2, 1);
		Zone* zone3 = new Zone(3, 0);
		Zone* zone4 = new Zone(4, 0);

		zone0->setAdjZoneIds({ 2 });
		zone1->setAdjZoneIds({ 3,4 });
		zone2->setAdjZoneIds({ 0,1,4 });
		zone3->setAdjZoneIds({ 1 });
		zone4->setAdjZoneIds({ 2 });


		Map map("Test1", 10, 5);

		map.addRegion(region1);
		map.addRegion(region2);

		map.addZone(zone0);
		map.addZone(zone1);
		map.addZone(zone2);
		map.addZone(zone3);
		map.addZone(zone4);

		map.connectTheEdges();


		vector<Zone*> zones = map.getZonesByRegion(region1->getId());



		cout << "Testing region of id  " << region1->getId() << endl;

		cout << "Printing out all zones of Region id " << region1->getId() << " before DFS" << endl;
		for (int i = 0; i < zones.size(); i++) {
			cout << "Zone id " << zones.at(i)->getId() << endl;
		}

		cout << "starting DFS " << endl;

		if (map.areAllZonesConnected(region1->getId())) {
			cout << "Zones are connected (Expected)" << endl;
		}
		else {
			cout << "Zones are not connected (Not Expected)" << endl;
		}

		cout << endl << "Testing region of id  " << region2->getId() << endl;
		if (map.areAllZonesConnected(region2->getId())) {
			cout << "Zones are connected (Expected)" << endl;
		}
		else {
			cout << "Zones are not connected (Not Expected)" << endl;
		}


	}

	void MapDriverSpace::testNotConnectedDFS()
	{
		cout << "\nTest case: Not connected Region." << endl;
		cout << "The map should be able to detect that its vertex under a region is not connected" << endl;

		Region* regionA = new Region(0,"Region A");
		Region* regionB = new Region(1, "Region B");
		Region* regionC = new Region(2, "Region C");

		Zone* zone0 = new Zone(0, 0);
		Zone* zone1 = new Zone(1, 0, { 0 ,2 ,3});
		Zone* zone2 = new Zone(2, 1, { 1 });
		Zone* zone3 = new Zone(3, 2, { 1 });

		Map map = Map("Life", 5, 5);
		map.addRegion(regionA);
		map.addRegion(regionC);
		map.addRegion(regionB);

		map.addZone(zone0);
		map.addZone(zone3);
		map.addZone(zone2);
		map.addZone(zone1);
		map.connectTheEdges();


		cout << "Printing the Regions and their zones";

		for (int i = 0; i < map.getRegionSize(); i++) {
			cout << map.getRegions()->at(i)->getRegionName() << " has the Zones " << endl;
			vector<Zone*> zones = map.getZonesByRegion(map.getRegions()->at(i)->getId());
			for (int j = 0; j < zones.size(); j++)
				cout << "	 Zone ID:" << zones.at(j)->getId()<<endl;

		}
		cout << endl;


		//In this case, the total area of the map is connected
		//But Region A isn't a connected submap

		if (map.areAllZonesConnected()) {
			cout << "Zones are connected (Not Expected)" << endl;
		}
		else {
			cout << "Zones are not connected (Expected)" << endl;
		}
	}

	void MapDriverSpace::testExceptions()
	{
		cout << "\nTest Case: Triggering the exceptions \n";
		cout << "Making sure the error triggers works as expected \n";

		Map map = Map("Bad Map", 1, 1);

		Region* regionA = new Region(0, "A");

		Zone* zoneA = new Zone(0, 0);

		map.addRegion(regionA);
		map.addZone(zoneA);

		string tests[] = { "NULL1","NULL2", "Zone", "Region" };
		
		for (string test : tests) {
			if (test == "NULL1") {
				try {
					cout << "Attempting to pass a NULL region \n";
					map.addRegion(NULL);
					cout << "Test Failed\n";
				}
				catch (MapExceptions::NullPtrException &n) {
					cout << n.what();
					cout<< "Exception triggered [Expected]\n";
				}
				catch (exception &e) {

				}
			}
			else if (test == "NULL2") {
				try {
					cout << "Attempting to pass a NULL Zone \n";
					map.addZone(NULL);
					cout << "Test Failed\n";
				}
				catch (MapExceptions::NullPtrException &n) {
					cout << n.what();
					cout << "Exception triggered [Expected]\n";
				}
				catch (exception &e) {
					cout << e.what();
					cout << "Other exceptions[Less than Expected]\n";
				}
			}
			else if (test == "Zone") {
				try {
					cout << "Trying to add exceeding amount of Zones\n";
					map.addZone(new Zone(1,0));
					cout << "Test Failed\n";
				}
				catch (MapExceptions::ZoneOutOfRangeException &z) {
					cout << z.what();
					cout << "Exception triggered [Expected]\n";
				}
				catch (exception &e) {
					cout << e.what();
					cout << "Other exceptions[Less than Expected]\n";
				}
			}
			else if (test == "Region") {
				try {
					cout << "Trying to add exceeding amount of Regions\n";
					map.addRegion(new Region(1,"Bad"));

					cout << "Test Failed\n";
				}
				catch (MapExceptions::RegionOutOfRangeException &r) {
					cout << r.what();
					cout << "Exception triggered [Expected]\n";
				}catch (exception &e) {
					cout << e.what();
					cout << "Other exceptions[Less than Expected]\n";
				}
			}
		}

	}

	void MapDriverSpace::runTests() {
		testRegionalDFS();
		testWholeDFS();
		testNotConnectedDFS();
		testExceptions();
	}


