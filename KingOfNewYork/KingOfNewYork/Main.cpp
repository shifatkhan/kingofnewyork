#include <iostream>
#include <string>
#include "DiceRollingFacility.h"
#include "BlackDie.h"
#include "MapDriver.h"
#include "MapLoaderDriver.h"
#include <cstdlib>
#include <ctime>
#include "TileSetUpDriver.h"
#include "PlayerMoveTester.h"
#include "GameStartDriver.h"
#include "MoveDriver.h"
#include "MainGameLoop.h"
#include "StartUpDriver.h"
#include "MainView.h"
#include "CardEffectsView.h"

int main()
{
	GameStartIO io = GameStartDriver::run();
	vector<Player*> players = io.getPlayers();

	vector<CardEffectsView> cardEffectViews;
	cardEffectViews.reserve(players.size());

	for (int i = 0; i < players.size(); i++)
	{
		CardEffectsView temp(players.at(i));
		cardEffectViews.push_back(temp);
		players.at(i)->Attach(&cardEffectViews.at(i));
	}

	Deck deck = io.getDeck();
	Map* map = io.getMap();
	players = StartUpDriver::setUp(players, map);

	MainGameLoop::MainGameLoop gameLoop(players, deck, map);

	MainView mainView(&gameLoop);
	gameLoop.Attach(&mainView);

	gameLoop.mainLoop();

	delete io.getMap();
	for (int i = 0; i < io.getPlayers().size(); i++)
		delete io.getPlayers().at(i);
	return 0;
}