#ifndef ROLLDICEDRIVER_H
#define ROLLDICEDRIVER_H
#include "Symbol.h"
#include "BlackDie.h"
#include "Player.h"
#include "AiUtil.h"
#include "MainGameLoop.h"

namespace RollDiceDriver {
	//Human interaction functions
	void rollDice(Player* player, MainGameLoop::MainGameLoop* loop);
	void displayRollOptions();
	array<bool, 6> displayDiceToKeep(array<BlackDie, 6> dice);

	void rollDiceForAI(Player* player, vector<Symbol> toKeep);
	bool keepDice(BlackDie die, vector<Symbol> keep);
}


#endif // !ROLLDICEDRIVER_H
